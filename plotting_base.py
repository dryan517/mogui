import scipy as sp
from scipy.interpolate import InterpolatedUnivariateSpline
import matplotlib.pyplot as plt
from copy import copy

# Don't use self. in bulk of __init__, just have a list of self.var=var at the end of init
# using sp.multiply on 2d matrices is apparently a waste, and also really confusing when you string many of them. 


class cheaselog():

  def __init__(self, path):

    f = open(path,'r')
    lines=f.readlines()
    f.close()

    for line in lines:
      if 'GEXP' in line:
        gexp = eval(line.split()[3])
      if 'Q AT 95% FLUX SURFACE' in line:
        q95 = eval(line.split()[-1])
      if 'PRESSURE PEAKING FACTOR' in line:
        ppf = eval(line.split()[-1])
      if 'B0 [T]' in line:
       b0exp = eval(line.split()[0])

    li = eval(lines[-14].split()[0])
    q0 = eval(lines[-13].split()[0])
    qa = eval(lines[-12].split()[0])
    i0exp = eval(lines[-19].split()[-1])
    r0exp = eval(lines[-25].split()[0])

    for index,line in enumerate(lines):
      if ' CS  - MESH' in line:
        s_index_start = index+1
      if ' BETA-POLOIDAL(CS)' in line:
        s_index_end = index-1
      if ' CSM - MESH' in line:
        sm_index_start = index+1
      if ' PSI(CSM)' in line:
        sm_index_end = index-1
      if ' P (CSM)' in line:
        p_index_start = index+1
      if ' DP/DPSI(CSM)' in line: 
        p_index_end = index-1
      if ' J-PARALLEL (<j.B>Eq.43)' in line:
        j_index_start = index+1
      if ' J-PAR - J-BS(0)' in line:
        j_index_end = index-1

    cs_lines = lines[s_index_start:s_index_end]
    csm_lines = lines[sm_index_start:sm_index_end]
    p_lines = lines[p_index_start:p_index_end]
    j_lines = lines[j_index_start:j_index_end]

    cs_line = ''
    csm_line = ''
    p_line = ''
    j_line = ''
    for line in cs_lines:
      cs_line = cs_line + line.split('\n')[0]
    self.cs = sp.array([eval(x) for x in cs_line.split()])

    for line in csm_lines:
      csm_line = csm_line + line.split('\n')[0]
    self.csm = sp.array([eval(x) for x in csm_line.split()])

    for line in p_lines:
      p_line = p_line + line.split('\n')[0]
    self.p = sp.array([eval(x) for x in p_line.split()])

    for line in j_lines:
      j_line = j_line + line.split('\n')[0]
    self.j = sp.array([eval(x) for x in j_line.split()])


    self.scalars = [b0exp, r0exp, i0exp, q0, q95, qa, gexp, li]

class moglog():

  def __init__(self, path):

    f=open(path,'r')
    lines=f.readlines()
    f.close()

    for line in lines[:40]:
      if 'B0 (T)    :' in line:
        b0 = eval(line.split()[-1])
      if 'R0 (m)    :' in line:
        r0 = eval(line.split()[-1])
      if 'ntor      :' in line:
        ntor = eval(line.split()[-1])
      if 'vT0 krad/s:' in line:
        vtor0 = eval(line.split()[-1])
      if 'ne0       :' in line:
        ne0 = eval(line.split()[-1])
      if 'Te0       :' in line:
        te0 = eval(line.split()[-1])
      if 'Ti0       :' in line:
        ti0 = eval(line.split()[-1])
      if 'asp       :' in line:
        asp = eval(line.split()[-1])
      if 'q0        :' in line:
        q0 = eval(line.split()[-1])
      if 'q95       :' in line:
        q95 = eval(line.split()[-1])
      if 'qa        :' in line:
        qa = eval(line.split()[-1])
      if 'betaN     :' in line:
        betan = eval(line.split()[-1])
      if 'I (MA)    :' in line:
        i0 = eval(line.split()[-1])
      if 'ETA       :' in line:
        eta_mars = eval(line.split()[-1])
      if 'ROTE      :' in line:
        rote_mars = eval(line.split()[-1])
      if 'Upper coil currents' in line:
        currents_u = sp.array([eval(x) for x in line.split(':')[1].split('\n')[0].split(',')])/1e3
      if 'Lower coil currents' in line:
        currents_l = sp.array([eval(x) for x in line.split(':')[1].split('\n')[0].split(',')])/1e3

    for index, line in enumerate(lines[:40]):
      if '    2    2\n' in line:
        start_ind = index
        nRZ = eval(line.split()[0])

    R = sp.array([eval(x.split()[0])  for x in lines[start_ind+1:start_ind+nRZ+1]])*r0
    Z = sp.array([eval(x.split()[-1]) for x in lines[start_ind+1:start_ind+nRZ+1]])*r0

    self.scalars = [b0,r0,ntor,vtor0,ne0,te0,ti0,asp,q0,q95,qa,betan,i0,eta_mars,rote_mars]
    self.boundR = R
    self.boundZ = Z 
    self.currents_u = currents_u
    self.currents_l = currents_l


class profs1d():

  def __init__(self, path, n=2):
    profeq = sp.loadtxt(path)

    self.s = profeq[:,0] # radial coordinate, sqrt(psi_p)
    self.q = profeq[:,1] # safety factor
    self.j = profeq[:,2] # current density
    self.p = profeq[:,3] # pressure
    self.n = profeq[:,4] # density
    self.vPhi = profeq[:,5] # toroidal rotation
    self.res = profeq[:,6] # resistivity
    self.gamma = profeq[:,7] # ??????
    self.visc = profeq[:,8] # viscosity 
    self.Ti = profeq[:,9] # ion temp
    self.Te = profeq[:,10] # electron temp
    dpsi = profeq[:,11] # d Psi / d s
    dpsi[0]=dpsi[1]
    self.dpsi = dpsi
    self.F = profeq[:,12] # ??????
    self.omega_i = profeq[:,13] # ion diamagnetic freq
    self.omega_e = profeq[:,14] # electron diamagnetic freq
    self.CSV = profeq[:,15] # ??????

    # get rational surfaces
    q_max = self.q.max()
    q_min = self.q.min()
    self.m_rat = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
    self.q_rat = self.m_rat/float(n)
    self.s_rat = InterpolatedUnivariateSpline(self.q, self.s)(self.q_rat)


class profs2d():
  
  def __init__(self, profs1d, rz, r0exp, out_dir=None):
    q=profs1d.q
    j=profs1d.j
    p=profs1d.p
    vPhi=profs1d.vPhi

    q2d = sp.zeros(rz.R[:rz.Ns_plas,:].shape)
    j2d = sp.zeros(rz.R[:rz.Ns_plas,:].shape)
    p2d = sp.zeros(rz.R[:rz.Ns_plas,:].shape)
    vPhi2d = sp.zeros(rz.R[:rz.Ns_plas,:].shape)
    for i in range(rz.Ns_plas):
      q2d[i,:] = q[i]
      j2d[i,:] = j[i]
      p2d[i,:] = p[i]
      vPhi2d[i,:] = vPhi[i]

    self.q2d = q2d
    self.j2d=j2d
    self.p2d=p2d
    self.vPhi2d=vPhi2d

  def plot(self, out_dir, rz, r0exp):
    if out_dir=='None':
      print('out dir not specified')
    else:
      plt.figure(figsize=(8,8))
      plt.subplot(221)
      plt.title('q')
      plt.contourf(rz.R[:rz.Ns_plas,:]*r0exp,rz.Z[:rz.Ns_plas,:]*r0exp,self.q2d, 120)
      plt.colorbar()
      plt.subplot(222)
      plt.title('j')
      plt.contourf(rz.R[:rz.Ns_plas,:]*r0exp,rz.Z[:rz.Ns_plas,:]*r0exp,self.j2d, 120)
      plt.colorbar()
      plt.subplot(223)
      plt.title('p')
      plt.contourf(rz.R[:rz.Ns_plas,:]*r0exp,rz.Z[:rz.Ns_plas,:]*r0exp,self.p2d, 120)
      plt.colorbar()
      plt.subplot(224)
      plt.title('vPhi')
      plt.contourf(rz.R[:rz.Ns_plas,:]*r0exp,rz.Z[:rz.Ns_plas,:]*r0exp,self.vPhi2d, 120)
      plt.colorbar()

      plt.tight_layout()
      plt.savefig(out_dir+'/equil2d.png')
      #plt.show()



class rzcoords():

  def __init__(self, path, nchi):
    rmzm = sp.loadtxt(path)
    Nm0 = int(rmzm[0,0]) # Num. poloidal harmonics for equilibrium quantities (not necessarily same as for perturbation quantities, but should be).
    Ns_plas = int(rmzm[0,1]) # Num. radial points in plasma
    Ns_vac = int(rmzm[0,2]) # Num. radial points in vacuum
    Ns = Ns_plas + Ns_vac
    R0EXP = rmzm[0,3]
    B0EXP = rmzm[1,3]
    s = rmzm[1:Ns+1, 0]
    RM = rmzm[Ns+1:,0] + 1j*rmzm[Ns+1:,1]
    ZM = rmzm[Ns+1:,2] + 1j*rmzm[Ns+1:,3]
    RM = RM.reshape((Nm0, Ns))
    RM = sp.transpose(RM)
    ZM = ZM.reshape((Nm0, Ns))
    ZM = sp.transpose(ZM)
    RM[:,1:] = 2*RM[:,1:]
    ZM[:,1:] = 2*ZM[:,1:]

    m = sp.arange(0,Nm0,1)
    chi = sp.linspace(-sp.pi, sp.pi,nchi)
    expmchi = sp.exp(sp.tensordot(m,chi,0)*1j)
    R = sp.dot(RM[:,:Nm0],expmchi)
    Z = sp.dot(ZM[:,:Nm0],expmchi)
    
    self.R = sp.array(R.real) # R coordinates
    self.Z = sp.array(Z.real) # Z coordinates
    self.Nchi = nchi
    self.Nm0 = Nm0
    self.Ns_plas = Ns_plas  # number is s points in plasma
    self.Ns_vac = Ns_vac    # number of s points in vacuum
    self.Ns = Ns            # total number of s points
    self.R0EXP = R0EXP      # normalisation length
    self.B0EXP = B0EXP      # normalisation magnetic field
    self.m = m              # equilibrium poloidal harmonics
    self.chi=chi            # poloidal angle coordinate
    self.s=s                # radial coordinate = sqrt(psi_pol)


class jacobian():

  # Decide what stuff is needed later, and add a self. in front of it.
  def __init__(self, rz):
    if not isinstance(rz, rzcoords):
      print("Must pass in coordinate system of type plotting_base.rzcoords")
      return

    self.Ns = rz.Ns # Used in jacobian.plot(), so pass in from rzcoords

    self.dRds = sp.copy(rz.R) 
    self.dZds = sp.copy(rz.R)
    self.dRdchi = sp.copy(rz.R)
    self.dZdchi = sp.copy(rz.R)
    self.jacobian = sp.copy(rz.R)

  # this is for the vacuum region. these are overwritten for the plasma region
  # just having a number to denote the boundary index might be simpler in the future.
  # Vac_start variable should be all that's needed. II is way too complicated
    II_start = int(rz.Ns_plas)-1; II_end = len(rz.R[:,0])
    II2_start = int(rz.Ns_plas); II2_end = len(rz.R[:,0])

    s0 = sp.copy(rz.s[II_start:II_end]); R0 = sp.copy(rz.R[II_start:II_end, :])
    chi0=sp.squeeze(sp.copy(sp.array(rz.chi))); Z0 = sp.copy(rz.Z[II_start:II_end, :])

    hs = 0.5*(s0[1:] - s0[:-1]).min(); hs = min(hs,  2e-5)
    hchi = 0.5*(chi0[1:] - chi0[:-1]).min(); hchi = min(hchi,  1e-4)
    s1 = s0-hs; s2 = s0+hs
    chi1 = chi0 - hchi;  chi2 = chi0 + hchi

  # compute dR/ds using R(s,chi)
    R1 = sp.zeros(sp.shape(R0))
    R2 = sp.zeros(sp.shape(R0))
    for i in range(rz.Nchi):
      R1[:,i] = InterpolatedUnivariateSpline(s0,R0[:,i], bbox=[s1[0], s0[-1]])(s1)
      R2[:,i] = InterpolatedUnivariateSpline(s0,R0[:,i], bbox=[s0[0], s2[-1]])(s2)
    self.dRds[II_start:II_end,:] = (R2 - R1)/(2*hs)

  # compute dZ/ds using Z(s,chi) 
    Z1 = sp.zeros(sp.shape(Z0))
    Z2 = sp.zeros(sp.shape(Z0))
    for i in range(rz.Nchi):
      Z1[:,i] = InterpolatedUnivariateSpline(s0,Z0[:,i], bbox=[s1[0], s0[-1]])(s1)
      Z2[:,i] = InterpolatedUnivariateSpline(s0,Z0[:,i], bbox=[s0[0], s2[-1]])(s2)
    self.dZds[II_start:II_end,:] = (Z2 - Z1)/(2*hs)

  #  compute dR/dchi using R(s,chi) 
    R1 = sp.zeros(sp.shape(R0))
    R2 = sp.zeros(sp.shape(R0))
    for i in range(int(rz.Ns_vac)+1):
      R1[i,:] = InterpolatedUnivariateSpline(chi0,R0[i,:], bbox=[chi1[0], chi0[-1]])(chi1)
      R2[i,:] = InterpolatedUnivariateSpline(chi0,R0[i,:], bbox=[chi0[0], chi2[-1]])(chi2) 
      self.dRdchi[i+II_start,:] = (R2[i,:] - R1[i,:])/(2*hchi) 

  # compute dZ/dchi using Z(s,chi) 
    Z1 = sp.zeros(sp.shape(Z0))
    Z2 = sp.zeros(sp.shape(Z0))
    for i in range(int(rz.Ns_vac)+1):
      Z1[i,:] = InterpolatedUnivariateSpline(chi0,Z0[i,:], bbox=[chi1[0], chi0[-1]])(chi1)
      Z2[i,:] = InterpolatedUnivariateSpline(chi0,Z0[i,:], bbox=[chi0[0], chi2[-1]])(chi2)
    self.dZdchi[II_start:II_end,:] = (Z2 - Z1)/(2*hchi)

  # Now do same calculations for plasma region
    II_start=0; II_end = rz.Ns_plas;

    s0 = sp.copy(rz.s[II_start:II_end]); R0 = sp.copy(rz.R[II_start:II_end, :])
    chi0=sp.squeeze(sp.copy(sp.array(rz.chi))); Z0 = sp.copy(rz.Z[II_start:II_end, :])

    hs = 0.5*(s0[1:] - s0[:-1]).min(); hs = min(hs,  2e-5)
    hchi = 0.5*(chi0[1:] - chi0[:-1]).min(); hchi = min(hchi,  1e-4)
    s1 = s0-hs; s2 = s0+hs
    chi1 = chi0 - hchi;  chi2 = chi0 + hchi

  # compute dR/ds using R(s,chi) 
    R1 = sp.zeros(sp.shape(R0))
    R2 = sp.zeros(sp.shape(R0))
    for i in range(rz.Nchi):
      R1[:,i] = InterpolatedUnivariateSpline(s0,R0[:,i], bbox=[s1[0], s0[-1]])(s1)
      R2[:,i] = InterpolatedUnivariateSpline(s0,R0[:,i], bbox=[s0[0], s2[-1]])(s2)
    self.dRds[II_start:II_end,:] = (R2 - R1)/(2*hs)

  # compute dZ/ds using Z(s,chi) 
    Z1 = sp.zeros(sp.shape(Z0))
    Z2 = sp.zeros(sp.shape(Z0))
    for i in range(rz.Nchi):
      Z1[:,i] = InterpolatedUnivariateSpline(s0,Z0[:,i], bbox=[s1[0], s0[-1]])(s1)
      Z2[:,i] = InterpolatedUnivariateSpline(s0,Z0[:,i], bbox=[s0[0], s2[-1]])(s2)
      self.dZds[:II_end,i] = (Z2[:,i] - Z1[:,i])/(2*hs)

  #  compute dR/dchi using R(s,chi)
    R1 = sp.zeros(sp.shape(R0))
    R2 = sp.zeros(sp.shape(R0))
    for i in range(int(rz.Ns_plas)):
      R1[i,:] = InterpolatedUnivariateSpline(chi0,R0[i,:], bbox=[chi1[0], chi0[-1]])(chi1)
      R2[i,:] = InterpolatedUnivariateSpline(chi0,R0[i,:], bbox=[chi0[0], chi2[-1]])(chi2)
    self.dRdchi[II_start:II_end,:] = (R2 - R1)/(2*hchi)

  # compute dZ/dchi using Z(s,chi) 
    Z1 = sp.zeros(sp.shape(Z0))
    Z2 = sp.zeros(sp.shape(Z0))
    for i in range(int(rz.Ns_plas)):
      Z1[i,:] = InterpolatedUnivariateSpline(chi0,Z0[i,:], bbox=[chi1[0], chi0[-1]])(chi1)
      Z2[i,:] = InterpolatedUnivariateSpline(chi0,Z0[i,:], bbox=[chi0[0], chi2[-1]])(chi2)
    self.dZdchi[II_start:II_end,:] = (Z2 - Z1)/(2*hchi)

    G11 = sp.square(self.dRds) + sp.square(self.dZds)
    G12 = sp.multiply(self.dRds, self.dRdchi) + sp.multiply(self.dZds, self.dZdchi)
    G22 = sp.square(self.dRdchi) + sp.square(self.dZdchi)
    G22[0,:]=G22[1,:]
    G33 = sp.square(rz.R)

    # Metrics elements
    self.G11 = G11
    self.G12 = G12
    self.G22 = G22
    self.G33 = G33

    self.jacobian = (-self.dRdchi*self.dZds + self.dRds*self.dZdchi)*rz.R
    self.jacobian[0,:] = self.jacobian[1,:]



class bplasma():

  def __init__(self, path, rz, jc):
  
    self.path = path
    bplasma = sp.loadtxt(self.path)
    Nm1 = int(bplasma[0,0]) # Number of perturbation poloidal harmonics (should be same as equilibrium harmonics)
    self.bm1 = bplasma[Nm1+1:, 0] + 1j*bplasma[Nm1+1:, 1]
    self.bm2 = bplasma[Nm1+1:, 2] + 1j*bplasma[Nm1+1:, 3]
    self.bm3 = bplasma[Nm1+1:, 4] + 1j*bplasma[Nm1+1:, 5]

    self.bm1 = self.bm1.reshape((Nm1, rz.Ns))
    self.bm2 = self.bm2.reshape((Nm1, rz.Ns))
    self.bm3 = self.bm3.reshape((Nm1, rz.Ns))

    # bm2 and bm3 are defined at half int points. 
    # 3 ways to recompute at int points, see MacReadBPLASMA.m, lines 109-124
    # For now, simplest implemented. Assume spline_B23==2.
    for i in range(len(self.bm2[:,1])):
      self.bm2[i, 1:] = self.bm2[i, :-1]
      self.bm3[i, 1:] = self.bm3[i, :-1]

    m = sp.array(bplasma[1:Nm1+1,0])

    expmchi = sp.exp(sp.tensordot(m,rz.chi,0)*1j)

    self.b1 = sp.dot(self.bm1.T,expmchi)
    self.b2 = sp.dot(self.bm2.T,expmchi)
    self.b3 = sp.dot(self.bm3.T,expmchi)

    self.bn = self.b1/sp.sqrt(jc.G22*jc.G33)

    self.m=m 

    """ 
    def calcBrBzBphiBabs(self): #either pass in jc, or make it self.jc
      # method should probably also actually return these? could get confusing otherwise. 
      # this sort of structure is not good practise. 
    """
    self.Br = sp.divide(sp.multiply(self.b1, jc.dRds) + sp.multiply(self.b2, jc.dRdchi), jc.jacobian)
    self.Bz = sp.divide(sp.multiply(self.b1, jc.dZds) + sp.multiply(self.b2,jc.dZdchi), jc.jacobian)
    self.Bphi = sp.divide(sp.multiply(self.b3, rz.R), jc.jacobian)

    self.Br[0,:] = self.Br[1,:]
    self.Bz[0,:] = self.Bz[1,:]
    self.Bphi[0:2,:] = self.Bphi[3,:]

    self.AbsB = sp.sqrt(sp.square(sp.absolute(self.Br)) + sp.square(sp.absolute(self.Bz)) + sp.square(sp.absolute(self.Bphi)))

    self.Nm1=Nm1

    self.rz = rz
    self.jc = jc
    
  def add(self, bplasma, deltaphi_rad):
    # return a new bplasma object. untested
    bplasma_total = copy(self)

    bplasma_total.bm1 = self.bm1 + bplasma.bm1*sp.exp(-1j*deltaphi_rad)
    bplasma_total.bm2 = self.bm2 + bplasma.bm2*sp.exp(-1j*deltaphi_rad)
    bplasma_total.bm3 = self.bm3 + bplasma.bm3*sp.exp(-1j*deltaphi_rad)

    bplasma_total.b1 = self.b1 + bplasma.b1*sp.exp(-1j*deltaphi_rad)
    bplasma_total.b2 = self.b2 + bplasma.b2*sp.exp(-1j*deltaphi_rad)
    bplasma_total.b3 = self.b3 + bplasma.b3*sp.exp(-1j*deltaphi_rad)

    bplasma_total.bn = self.bn + bplasma.bn*sp.exp(-1j*deltaphi_rad)
    
    bplasma_total.Br = self.Br + bplasma.Br*sp.exp(-1j*deltaphi_rad)
    bplasma_total.Bz = self.Bz + bplasma.Bz*sp.exp(-1j*deltaphi_rad)
    bplasma_total.Bphi = self.Bphi + bplasma.Bphi*sp.exp(-1j*deltaphi_rad)

    return bplasma_total



class xplasma():

  def __init__(self, path, rz, jc):
    
    self.path = path

    xplas = sp.loadtxt(self.path)

    Nm1 = int(xplas[0,0]) # Number of poloidal harmonics 
    m = sp.rint(xplas[1:Nm1+1, 0])

    dpsids = xplas[Nm1+1:Nm1+1+rz.Ns_plas,0]

    T = xplas[Nm1+1:Nm1+1+rz.Ns_plas,3]

    xm1 = xplas[Nm1+1+rz.Ns_plas:, 0] + 1j*xplas[Nm1+1+rz.Ns_plas:, 1]
    xm2 = xplas[Nm1+1+rz.Ns_plas:, 2] + 1j*xplas[Nm1+1+rz.Ns_plas:, 3]
    xm3 = xplas[Nm1+1+rz.Ns_plas:, 4] + 1j*xplas[Nm1+1+rz.Ns_plas:, 5]

    xm1 = xm1.reshape((Nm1, rz.Ns_plas))
    xm2 = xm2.reshape((Nm1, rz.Ns_plas))
    xm3 = xm3.reshape((Nm1, rz.Ns_plas))


    # xm2 and xm3 are defined at half-points (s_half), recompute at integer points (s)
    s_half = (rz.s[0:rz.Ns_plas-1]+rz.s[1:rz.Ns_plas])*0.5
    # should probably do it this way in bplasma too, rather than the easy way 
    xm2_new = sp.copy(xm2)
    for i in range(int(Nm1)):
      xm2_new[i,1:-1]=InterpolatedUnivariateSpline(s_half,xm2.real[i,0:-1])(rz.s[1:rz.Ns_plas-1]) + 1j*InterpolatedUnivariateSpline(s_half,xm2.imag[i,0:-1])(rz.s[1:rz.Ns_plas-1]) 
    xm2_new[:,0]=xm2_new[:,1]
    xm2_new[:,-1]=xm2_new[:,-2]
    xm2 = xm2_new
    xm3_new = sp.copy(xm3)
    for i in range(int(Nm1)):
      xm3_new[i,1:-1]=InterpolatedUnivariateSpline(s_half,xm3.real[i,0:-1])(rz.s[1:rz.Ns_plas-1]) + 1j*InterpolatedUnivariateSpline(s_half,xm3.imag[i,0:-1])(rz.s[1:rz.Ns_plas-1]) 
    xm3_new[:,0]=xm3_new[:,1]
    xm3_new[:,-1]=xm3_new[:,-2]
    xm3 = xm3_new

    # change central points of xm2 (no idea why, see ln 67 of MacReadVPLASMA)
    xm2[:,0]=xm2[:,2]
    xm2[:,1]=xm2[:,2]

    expmchi = sp.exp(sp.tensordot(m,rz.chi,0)*1j)
    x1a = sp.dot(xm1.T,expmchi)
    x2a = sp.dot(xm2.T,expmchi)
    x3a = sp.dot(xm3.T,expmchi)

    chione=sp.ones(rz.Nchi)

    x1 = x1a

    Bchi = sp.divide(sp.tensordot(dpsids,chione,0), jc.jacobian[:rz.Ns_plas,:])
    Bphi = sp.divide(sp.tensordot(T,chione,0), sp.square(rz.R[:rz.Ns_plas,:]))
    G11 = sp.square(jc.dRds[0:rz.Ns_plas,:]) + sp.square(jc.dZds[0:rz.Ns_plas,:])
    G12 = sp.multiply(jc.dRds[0:rz.Ns_plas,:], jc.dRdchi[0:rz.Ns_plas,:]) + sp.multiply(jc.dZds[0:rz.Ns_plas,:], jc.dZdchi[0:rz.Ns_plas,:])
    G22 = sp.square(jc.dRdchi[0:rz.Ns_plas,:]) + sp.square(jc.dZdchi[0:rz.Ns_plas,:])
    G22[0,:]=G22[1,:]
    G33 = sp.square(rz.R[0:rz.Ns_plas,:])
    B2 = sp.multiply(sp.square(Bchi),G22) + sp.multiply(sp.square(Bphi),G33)
    x2 = sp.divide(sp.multiply(sp.multiply(-x1,G12),sp.square(Bchi)),B2) + sp.divide(sp.multiply(sp.multiply(x2a,Bphi),G33),B2) + sp.multiply(x3a,Bchi)
    x3 = sp.divide(sp.multiply(sp.multiply(sp.multiply(-x1,G12),Bchi), Bphi), B2) + sp.divide(sp.multiply(sp.multiply(-x2a, Bchi), G22), B2) + sp.multiply(x3a,Bphi)
    xn = sp.divide(sp.multiply(x1a,jc.jacobian[:rz.Ns_plas,:]),sp.sqrt(sp.multiply(G33, G22)))

    # this bit might not be necessary. not sure.
    xn_999=sp.where(rz.s>0.999)[0][0]
#    for i in range(xn_999+1,int(rz.Ns_plas)+1):
#      xn[i-1,:] = xn[xn_999-1,:]

    self.xm1 = xm1

    self.m=m
    self.x1=x1
    self.x2=x2
    self.x3=x3
    self.xn=xn


def plotequil1d(profs1d, cheaselog, moglog, out_dir):

    Zeff=1.0
    lnL=15.0
    e=1.60217657E-19
    epsilon0 = 8.85417E-12 #[C^2/(N m^2)]
    pi=3.14159265359
    me=9.10938291E-31   #[kg]
    mi = 1.67261e-27    #[kg]
    kB=1.3806488E-23 #[J/K]
    mu0 = pi*4e-7;  #[Wb/(A m)]
    Z  = 2; # atomic mass: D = 2, Z for D+T is 2.5
    eV_to_Kelvin = e/kB

    b0exp,r0exp,ntor,vtor0,ne0,te0,ti0,asp,q0,q95,qa,betan,i0exp,eta_mars,rote_mars = moglog.scalars

    v_A = b0exp/sp.sqrt(mu0*mi*Z*ne0) # [m/s] Alfven speed
    eta_norm = (mu0*r0exp*v_A)
    currNorm = b0exp/(mu0*r0exp)
    pressNorm = (b0exp**2)/mu0
    omega_A = v_A/r0exp

    Te_Kelvin= profs1d.Te*te0*eV_to_Kelvin
    etaP = (pi*Zeff*(e**2)*(me**0.5)*lnL)/(((4*pi*epsilon0)**2)*(kB*Te_Kelvin)**1.5)

    plt.figure(figsize=(18,8))
    plt.subplot(241)
    plt.plot(profs1d.s,profs1d.q)
    plt.ylabel('Safety factor q')
    plt.xlim([0,1])
    plt.subplot(242)
    plt.plot(cheaselog.cs,cheaselog.j*currNorm/1e6)
    plt.ylabel('Parallel Current Density, $MA/m^2$')
    plt.ylim(ymin=0)
    plt.xlim([0,1])
    plt.subplot(243)
    plt.plot(cheaselog.cs,cheaselog.p*pressNorm/1e3)
    plt.ylabel('Plasma Pressure, $kPa$')
    plt.ylim(ymin=0)
    plt.xlim([0,1])
    plt.subplot(244)
    plt.semilogy(profs1d.s,profs1d.res*eta_norm)
    plt.semilogy(profs1d.s,etaP)
    plt.ylabel('Resistivity (Ohm m)')
    plt.xlim([0,1])
    plt.subplot(245)
    plt.plot(profs1d.s,profs1d.vPhi*omega_A/1e3) 
    plt.ylabel('Bulk plasma rotation (krad/s)')
    plt.plot([0,1.0],[0.0,0.0],'--k')
    plt.xlim([0,1])
    plt.subplot(246)
    plt.plot(profs1d.s,profs1d.Ti*ti0, label='Ti')
    plt.plot(profs1d.s,profs1d.Te*te0, label='Te')
    plt.ylabel('Ion/Electron temp (eV)')
    plt.xlim([0,1])
    plt.legend()
    plt.plot([1.0],[100],'+k')
    plt.subplot(247)
    plt.plot(profs1d.s,profs1d.n*ne0)
    plt.ylabel('Electron density /m^3')
    plt.xlim([0,1])
    plt.ylim(ymin=0)
    plt.subplot(248)
    plt.plot(profs1d.s,profs1d.omega_i*omega_A/1e3, label='ion diamagnetic')
    plt.plot(profs1d.s,profs1d.omega_e*omega_A/1e3, label='elect diamagnetic')
    plt.ylabel('Diamagnetic freqs (krad/s)')
    plt.xlim([0,1])

    plt.tight_layout()
    plt.savefig(out_dir+'/equil1d.png')
    #plt.show()

def ergos_benchmark_toroidal(bplas_vac_upper_geom, bplas_vac_lower_geom, b0exp, currents_u, currents_l, out_dir):

  ergos_home = '/home/dar517/Dropbox/ERGOS-ASDEX'

  r0_u, z0_u = 1.6, 0.6
  r0_l, z0_l = 1.6, -0.6

  param=sp.loadtxt(ergos_home+'/bu1_field/param.dat')
  nR=int(param[0]); nZ=int(param[1]); nphi=int(param[2]); Rmin=param[3]; Rmax=param[4]; Zmin=param[5]; Zmax=param[6]

  r_erg = sp.linspace(Rmin,Rmax,nR)
  z_erg = sp.linspace(Zmin,Zmax,nZ)
  phi = sp.linspace(0,2*sp.pi,nphi)

  r_erg,z_erg = sp.meshgrid(r_erg,z_erg)

  br_erg = sp.zeros(nR*nZ*nphi)
  for i in range(len(currents_u)):
    print('loading br_erg upper %d/%d'%(i+1,len(currents_u)))
    if currents_u[i]!=0.0:
      br_erg += currents_u[i]*sp.loadtxt(ergos_home+'/bu%d_field'%(i+1)+'/BR.dat')
  for i in range(len(currents_l)):
    print('loading br_erg lower %d/%d'%(i+1,len(currents_l)))
    if currents_l[i]!=0.0:
      br_erg += currents_l[i]*sp.loadtxt(ergos_home+'/bl%d_field'%(i+1)+'/BR.dat')

  br_erg*=1e4*5 # 5 turns

  #bz_erg = sp.loadtxt('Bz.dat')*1e4
  #bp_erg = sp.loadtxt('Bphi.dat')*1e4

  br_erg = br_erg.reshape((nR,nZ,nphi)).transpose((1,0,2))
  #bz_erg = bz_erg.reshape((nR,nZ,nphi)).transpose((1,0,2))
  #bp_erg = bp_erg.reshape((nR,nZ,nphi)).transpose((1,0,2))

  z_ind_erg_u, r_ind_erg_u = sp.unravel_index(sp.argmin((r_erg-r0_u)**2+(z_erg-z0_u)**2),r_erg.shape)
  z_ind_erg_l, r_ind_erg_l = sp.unravel_index(sp.argmin((r_erg-r0_l)**2+(z_erg-z0_l)**2),r_erg.shape)

  br_erg_u = br_erg[z_ind_erg_u, r_ind_erg_u,:]
  br_erg_l = br_erg[z_ind_erg_l, r_ind_erg_l,:]

  # get n=2 component of ergos
  Y = fft(br_erg_u)
  Yn2 = sp.zeros(len(Y),dtype=complex); Yn6 = sp.zeros(len(Y),dtype=complex); Yn26 = sp.zeros(len(Y),dtype=complex)
  Yn2[2] = Y[2]; Yn2[-2] = Y[-2]; Yn6[6] = Y[6]; Yn6[-6] = Y[-6]; Yn26[2] = Y[2]; Yn26[6] = Y[6]; Yn26[-2] = Y[-2]; Yn26[-6] = Y[-6]
  br_erg_u_n2 = ifft(Yn2)

  Y = fft(br_erg_l)
  Yn2 = sp.zeros(len(Y),dtype=complex); Yn6 = sp.zeros(len(Y),dtype=complex); Yn26 = sp.zeros(len(Y),dtype=complex)
  Yn2[2] = Y[2]; Yn2[-2] = Y[-2]; Yn6[6] = Y[6]; Yn6[-6] = Y[-6]; Yn26[2] = Y[2]; Yn26[6] = Y[6]; Yn26[-2] = Y[-2]; Yn26[-6] = Y[-6]
  br_erg_l_n2 = ifft(Yn2)

  # get n=1 component of ergos
  Y = fft(br_erg_u)
  Yn1 = sp.zeros(len(Y),dtype=complex); 
  Yn1[1] = Y[1]; Yn1[-1] = Y[-1]; 
  br_erg_u_n1 = ifft(Yn1)

  Y = fft(br_erg_l)
  Yn1 = sp.zeros(len(Y),dtype=complex);
  Yn1[1] = Y[1]; Yn1[-1] = Y[-1]; 
  br_erg_l_n1 = ifft(Yn1)

  rot_mars_n2 =   0*(sp.pi/180)
  phase_diff_n2 = 0*(sp.pi/180)
  br_mars_n2 = (bplas_vac_upper_geom.Br+bplas_vac_lower_geom.Br*sp.exp(1j*phase_diff_n2))*b0exp*2*1e4
  #Bz_mars = (bplas_u.Bz+bplas_l.Bz)*B0EXP*2*1e4
  #Bp_mars = (bplas_u.Bphi+bplas_l.Bphi)*B0EXP*2*1e4 ##++

  r_ind_mar_u, z_ind_mar_u = sp.unravel_index(sp.argmin((rz_geom.R*r0exp-r0_u)**2+(rz_geom.Z*r0exp-z0_u)**2),rz_geom.R.shape)
  r_ind_mar_l, z_ind_mar_l = sp.unravel_index(sp.argmin((rz_geom.R*r0exp-r0_l)**2+(rz_geom.Z*r0exp-z0_l)**2),rz_geom.R.shape)

  br_mars_u_n2 = br_mars_n2[r_ind_mar_u, z_ind_mar_u]
  br_mars_l_n2 = br_mars_n2[r_ind_mar_l, z_ind_mar_l]

  plt.figure(1, figsize=(10,6))
  plt.subplot(211)
  plt.title('Toroidal angle lineout, ERGOS v MARS (vacuum field)')
  plt.plot(phi*180/sp.pi,(br_mars_u_n2*sp.exp(1j*(2*phi + rot_mars_n2))).real,linewidth=2.0, label='mars n=2')
  plt.plot(phi*180/sp.pi,br_erg_u,linewidth=2.0, label='ergos')
  plt.plot(phi*180/sp.pi,br_erg_u_n2.real,linewidth=2.0, label='erg n=2')
  plt.xlabel('Toroidal angle')
  plt.ylabel('Br (G)')
  plt.legend(loc=4)
  plt.xlim([0,360])

  plt.subplot(212)
  plt.plot(phi*180/sp.pi,(br_mars_l_n2*sp.exp(1j*(2*phi + rot_mars_n2))).real,linewidth=2.0)
  plt.plot(phi*180/sp.pi,br_erg_l,linewidth=2.0)
  plt.plot(phi*180/sp.pi,br_erg_l_n2.real,linewidth=2.0)
  plt.xlabel('Toroidal angle')
  plt.ylabel('Br (G)')
  plt.xlim([0,360])

  plt.tight_layout()
  plt.savefig(out_dir+'/toroidal-benchmark.png')
  #plt.show()




