import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import tkinter.font
from tkinter import messagebox
import scipy as sp
from scipy.interpolate import UnivariateSpline
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import os
import plotting_base as pb
import plotting_functions as plotf
import pexpect as pex
import datetime as dt
import sys
from subprocess import check_output

class basic_tab_contents():

    def __init__(self, mogui):
        mogui.basicframe.columnconfigure(0,minsize=100)
        mogui.basicframe.rowconfigure(0,minsize=30)
        mogui.basicframe.rowconfigure(2,minsize=35)
        mogui.basicframe.rowconfigure(4,minsize=35)
        mogui.basicframe.rowconfigure(7,minsize=35)
        ### Set all entry boxes and labels ###
        # local working directory
        localdir_label = tk.Label(mogui.basicframe, text="Local Working Directory", font=mogui.textfont1)
        localdir_label.grid(row=6+1, column=1+1, sticky=tk.S+tk.E+tk.W, padx=5)
        localdir_box = tk.Entry(mogui.basicframe, textvariable=mogui.workingdir_local, width=60, font=mogui.textfont1)
        localdir_box.grid(row=7+1, column=1+1, columnspan=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

        # remote working directory on gpu005
        remotedir_label = tk.Label(mogui.basicframe, text="Remote Working Directory", font=mogui.textfont1)
        remotedir_label.grid(row=8+1, column=1+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        self.remotedir_box = tk.Entry(mogui.basicframe, textvariable=mogui.workingdir_gpu005, width=60, font=mogui.textfont1)
        self.remotedir_box.grid(row=9+1, column=1+1, columnspan=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

        # local mode tick box and variable 
        mogui.local_mode = tk.IntVar()
        mogui.local_mode.set(1)
        local_mode_button = tk.Checkbutton(mogui.basicframe, variable=mogui.local_mode, command= lambda: self.updateLocalMode(mogui), text="Local mode", font=mogui.textfont1)
        local_mode_button.grid(row=8+1, column=2+1, sticky=tk.S+tk.E+tk.W, padx=5)

        # B0EXP entry
        B0EXP_label = tk.Label(mogui.basicframe, text=u"B\u2080: Equilibrium\nB Field (T)", font=mogui.textfont1, justify=tk.RIGHT)
        B0EXP_label.grid(row=0+1, column=0+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        B0EXP_entry = tk.Entry(mogui.basicframe, textvariable=mogui.B0EXP, width=12, font=mogui.textfont1, justify=tk.CENTER)
        B0EXP_entry.grid(row=1+1, column=0+1, sticky=tk.N, padx=5)

        # R0EXP entry
        R0EXP_label = tk.Label(mogui.basicframe, text=u"R\u2080: Magnetic\nAxis (m)", font=mogui.textfont1, justify=tk.RIGHT)
        R0EXP_label.grid(row=2+1, column=0+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        R0EXP_entry = tk.Entry(mogui.basicframe, textvariable=mogui.R0EXP, width=12, font=mogui.textfont1, justify=tk.CENTER)
        R0EXP_entry.grid(row=3+1, column=0+1, sticky=tk.N, padx=5)

        # ntor entry
        ntor_label = tk.Label(mogui.basicframe, text="Toroidal mode num, n", font=mogui.textfont1)
        ntor_label.grid(row=4+1, column=0+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        ntor_entry = tk.Entry(mogui.basicframe, textvariable=mogui.NTOR, width=6, font=mogui.textfont1, justify=tk.CENTER)
        ntor_entry.grid(row=5+1, column=0+1, sticky=tk.N+tk.S, padx=5)

        # vt0 entry
        vt0_label = tk.Label(mogui.basicframe, text="Core plasma rotation\nvelocity (rad/s)", font=mogui.textfont1)
        vt0_label.grid(row=0+1, column=1+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        vt0_entry = tk.Entry(mogui.basicframe, textvariable=mogui.vt0, width=12, font=mogui.textfont1, justify=tk.CENTER)
        vt0_entry.grid(row=1+1, column=1+1, sticky=tk.N, padx=5)

        # ne0 entry
        ne0_label = tk.Label(mogui.basicframe, text="Core electron\ndensity (/m^3)", font=mogui.textfont1)
        ne0_label.grid(row=2+1, column=1+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        ne0_entry = tk.Entry(mogui.basicframe, textvariable=mogui.ne0, width=12, font=mogui.textfont1, justify=tk.CENTER)
        ne0_entry.grid(row=3+1, column=1+1, sticky=tk.N, padx=5)

        # Te0 entry
        Te0_label = tk.Label(mogui.basicframe, text="Core electron\ntemp. (eV)", font=mogui.textfont1)
        Te0_label.grid(row=0+1, column=2+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        Te0_entry = tk.Entry(mogui.basicframe, textvariable=mogui.Te0, width=12, font=mogui.textfont1, justify=tk.CENTER)
        Te0_entry.grid(row=1+1, column=2+1, sticky=tk.N, padx=5)

        # Ti0 entry
        Ti0_label = tk.Label(mogui.basicframe, text="Core ion\ntemp (eV)", font=mogui.textfont1)
        Ti0_label.grid(row=2+1, column=2+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        Ti0_entry = tk.Entry(mogui.basicframe, textvariable=mogui.Ti0, width=12, font=mogui.textfont1, justify=tk.CENTER)
        Ti0_entry.grid(row=3+1, column=2+1, sticky=tk.N, padx=5)

        # aspect ratio entry
        aspect_label = tk.Label(mogui.basicframe, text="Aspect ratio R/a", font=mogui.textfont1)
        aspect_label.grid(row=4+1, column=2+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
        aspect_entry = tk.Entry(mogui.basicframe, textvariable=mogui.aspect, width=12, font=mogui.textfont1, justify=tk.CENTER)
        aspect_entry.grid(row=5+1, column=2+1, sticky=tk.N+tk.S, padx=5)

        # verbose mode tick box and variable
        mogui.verbose_mode = tk.IntVar()
        mogui.verbose_mode.set(0)
        verbose_mode_button = tk.Checkbutton(mogui.basicframe, variable=mogui.verbose_mode, text="Verbose mode", font=mogui.textfont1)
        verbose_mode_button.grid(row=6+1, column=2+1, sticky=tk.S+tk.E+tk.W, padx=5)

        calcQuantitiesButton = tk.Button(mogui.basicframe, text='Calc Quantities', command= lambda: self.calcQuantities(mogui), font=mogui.textfont1, width=18)
        #calcQuantitiesButton = tk.Button(mogui.basicframe, text='test button', command= lambda: mogui.chease_content.runChease(mogui), font=mogui.textfont1, width=18)##++
        calcQuantitiesButton.grid(row=4+1, column=1+1, sticky=tk.N+tk.S, padx=5)

        # Username variable and text box
        uname_label = tk.Label(mogui.basicframe, text="GPU005 username", font=mogui.textfont1)
        uname_label.grid(row=6+1, column=0+1, sticky=tk.S+tk.E+tk.W, padx=5)
        self.uname_box = tk.Entry(mogui.basicframe, textvariable=mogui.uname_gpu005, font=mogui.textfont1)
        self.uname_box.grid(row=7+1, column=0+1, sticky=tk.E+tk.W, padx=5)

        # Password variable and text box
        pw_label = tk.Label(mogui.basicframe, text="GPU005 password", font=mogui.textfont1)
        pw_label.grid(row=8+1, column=0+1, sticky=tk.S+tk.E+tk.W, padx=5)
        self.pw_box = tk.Entry(mogui.basicframe, textvariable=mogui.pw_gpu005,show='*', font=mogui.textfont1)
        self.pw_box.grid(row=9+1, column=0+1, sticky=tk.E+tk.W, padx=5)

        readR0B0Button = tk.Button(mogui.basicframe, text=u'Read (R\u2080,B\u2080) from gfile', command= lambda: self.gatherB0R0(mogui), font=mogui.textfont1, width=18)
        readR0B0Button.grid(row=5+1, column=1+1, sticky=tk.N+tk.S, padx=5)

        read_Te0Ti0Ne0Wt0_Button = tk.Button(mogui.basicframe, text=u'Read (Te\u2080,Ti\u2080,ne\u2080,wt\u2080)\nfrom PROF* files', command= lambda: self.gatherTe0Ti0Ne0Wt0(mogui), font=mogui.textfont1, width=18)
        read_Te0Ti0Ne0Wt0_Button.grid(row=11,column=2, rowspan=2, sticky=tk.N+tk.S, padx=5)

        update_default_paths_button = tk.Button(mogui.basicframe, text='Update file paths', command= lambda: self.updatePaths(mogui), font=mogui.textfont1, width=18)
        update_default_paths_button.grid(row=11,column=1, rowspan=2, sticky=tk.N+tk.S, padx=5)

        read_aspect_Button = tk.Button(mogui.basicframe, text=u'Read aspect ratio from\nlogchease file', command= lambda: self.gatherAspect(mogui), font=mogui.textfont1, width=18)
        read_aspect_Button.grid(row=11,column=3, rowspan=2, sticky=tk.N+tk.S, padx=5)

        self.updateLocalMode(mogui)


    def updateLocalMode(self, mogui):
      if mogui.local_mode.get()==0: # then enable
        self.pw_box.config(state=tk.NORMAL)
        self.uname_box.config(state=tk.NORMAL)
        self.remotedir_box.config(state=tk.NORMAL)
      if mogui.local_mode.get()==1: # then disable
        self.pw_box.config(state=tk.DISABLED)
        self.uname_box.config(state=tk.DISABLED)
        self.remotedir_box.config(state=tk.DISABLED)

    def gatherTe0Ti0Ne0Wt0(self, mogui):
      if os.path.isfile('%s/PROFTE.IN'%(mogui.workingdir_local.get(),)):
        s,te = sp.loadtxt('%s/PROFTE.IN'%(mogui.workingdir_local.get(),), skiprows=1, unpack=True)
        mogui.Te0.set(te[0])
      else:
        print('no PROFTE.IN found')
      if os.path.isfile('%s/PROFTI.IN'%(mogui.workingdir_local.get(),)):
        s,ti = sp.loadtxt('%s/PROFTI.IN'%(mogui.workingdir_local.get(),), skiprows=1, unpack=True)
        mogui.Ti0.set(ti[0])
      else:
        print('no PROFTI.IN found')
      if os.path.isfile('%s/PROFDEN.IN'%(mogui.workingdir_local.get(),)):
        s,ne = sp.loadtxt('%s/PROFDEN.IN'%(mogui.workingdir_local.get(),), skiprows=1, unpack=True)
        mogui.ne0.set(ne[0])
      else:
        print('no PROFDEN.IN found')
      if os.path.isfile('%s/PROFROT.IN'%(mogui.workingdir_local.get(),)):
        s,wt = sp.loadtxt('%s/PROFROT.IN'%(mogui.workingdir_local.get(),), skiprows=1, unpack=True)
        mogui.vt0.set(wt[0])
      else:
        print('no PROFROT.IN found')

    def gatherAspect(self, mogui):
      if os.path.isfile(mogui.logchease.get()):
        path=mogui.logchease.get()
        f=open(path,'r')
        lines=f.readlines()
        f.close()
        for line in lines:
          if 'ASPECT RATIO ; a/R=' in line:
            mogui.aspect.set(eval(line.split()[0]))
      else:
        path = mogui.workingdir_local.get()+'/log_chease'
        if os.path.isfile(str(path)):
          mogui.logchease.set(path)
          mogui.logchease_shortened.set('...'+path[-15:])
          f=open(path,'r')
          lines=f.readlines()
          f.close()
          for line in lines:
            if 'ASPECT RATIO ; a/R=' in line:
              mogui.aspect.set(eval(line.split()[0]))
        else:
          print('no log chease file found')


    def calcQuantities(self, mogui, silent=False):

        elementary_charge = 1.6021917e-19;  #[C]
        me = 9.1095e-31;    #[kg]
        mi = 1.67261e-27;   #[kg]
        epsilon0 = 8.8542e-12; #[C^2/(N m^2)]
        mu0 = sp.pi*4e-7;      #[Wb/(A m)]
        c = 2.99792e+8; #[m/s]

        T0 = -273.15; #[K], absolute temperature
        kB = 1.38e-23; #[J/K], Boltzmann's constant

        A = mogui.aspect.get()
        R = mogui.R0EXP.get()
        B = mogui.B0EXP.get()
        Te = mogui.Te0.get()
        Ti = mogui.Ti0.get()
        n  = mogui.ne0.get()
        vT = mogui.vt0.get()
        Z  = 2.0
        Zeff = 1.0


        a = R/A;
        Te = Te*elementary_charge/kB; #[K]
        Ti = Ti*elementary_charge/kB; #[K]
        vthi = sp.sqrt(2*kB*Ti/mi);
        vthe = sp.sqrt(2*kB*Te/me);

        omega_0 = vT; # assuming vT is in rad/s not m/s
        if silent==False: print('')
        # Temperatures
        if silent==False: print('Ion temperature,\t\tTi = %0.2f MK'%(Ti/1e6,))
        if silent==False: print('Electron temperature,\t\tTe = %0.2f MK'%(Te/1e6,))

        # gyrofrequencies
        omega_ce = elementary_charge*B/me; 
        omega_ci = elementary_charge*B/mi/Z; 
        if silent==False: print('Ion gyrofrequency,\t\tf_ci = %0.2f MHz'%(omega_ci/2/sp.pi/1e6,))
        if silent==False: print('Electron gyrofrequency,\t\tf_ce = %0.2f GHz'%(omega_ce/2/sp.pi/1e9,))

        # plasma frequency
        omega_pi = sp.sqrt(elementary_charge**2*n/mi/epsilon0);
        omega_pe = sp.sqrt(elementary_charge**2*n/me/epsilon0);
        if silent==False: print('Ion plasma frequency,\t\tf_pi = %0.2f GHz'%(omega_pi/2/sp.pi/1e9,))
        if silent==False: print('Electron plasma frequency,\tf_pe = %0.2f GHz'%(omega_pe/2/sp.pi/1e9,))

        # gyro-radius
        r_Le = vthe/(omega_ce); #[m]
        r_Li = vthi/(omega_ci); #[m]
        if silent==False: print('Ion Larmor radius,\t\tr_Li = %0.2f mm'%(r_Li*1e3,))
        if silent==False: print('Electron Larmor radius,\t\tr_Le = %0.2f mm'%(r_Le*1e3,))

        # Debye length
        r_D = sp.sqrt(epsilon0*kB*Te/n/elementary_charge**2) #[m]
        if silent==False: print('Debye length,\t\t\tr_D = %0.2f \u03BCm'%(r_D*1e6,))

        # Alfven frequency
        v_A = B/sp.sqrt(mu0*mi*Z*n); #[m/s]
        omega_A = v_A/R;  
        tau_A = R/v_A; #[s]
        if silent==False: print('Alfven frequency,\t\tf_A = %0.2f MHz'%(omega_A/2/sp.pi/1e6,))

        # ion sound frequency
        Gamma = 5.0/3.0;
        rho = mi*Z*n;
        P = n*kB*(Ti+Te);
        v_s = sp.sqrt(Gamma*P/rho);
        omega_s = v_s/R;
        tau_s = R/v_s;
        if silent==False: print('Ion sound frequency,\t\tf_s = %0.2f GHz'%(omega_s/2/sp.pi/1e6,))

        # diamagnetic drift frequency, estimation!
        omega_stari = 2*n*kB*Ti/a/(elementary_charge*n*B)/R;
        omega_stare = 2*n*kB*Te/a/(elementary_charge*n*B)/R;
        if silent==False: print('Ion diamag drift freq (est),\tf_*i = %0.2f kHz'%(omega_stari/2/sp.pi/1e3,))
        if silent==False: print('Elec diamag drift freq (est),\tf_*e = %0.2f kHz'%(omega_stare/2/sp.pi/1e3,))

        # magnetic drift frequency
        q = 2;
        omega_Bi = q*vthi**2/omega_ci/R/a;
        omega_Be = q*vthe**2/omega_ce/R/a;
        if silent==False: print('Ion mag drift freq,\t\tf_Bi = %0.2f kHz'%(omega_Bi/2/sp.pi/1e3,))
        if silent==False: print('Elec mag drift freq,\t\tf_Be = %0.2f kHz'%(omega_Be/2/sp.pi/1e3,))

        # transit frequency (circulating ions and electrons)
        omega_ti = vthi/q/R;
        omega_te = vthe/q/R;
        if silent==False: print('Ion transit frequency,\t\tf_ti = %0.2f kHz'%(omega_ti/2/sp.pi/1e3,))
        if silent==False: print('Electron transit freq,\t\tf_te = %0.2f MHz'%(omega_te/2/sp.pi/1e6,))

        # bounce frequency (trapped ions and electrons)
        omega_bi = omega_ti/sp.sqrt(2*A);
        omega_be = omega_te/sp.sqrt(2*A);
        if silent==False: print('Ion bounce freq,\t\tf_bi = %0.2f kHz'%(omega_bi/2/sp.pi/1e3,))
        if silent==False: print('Electron bounce freq,\t\tf_be = %0.2f MHz'%(omega_be/2/sp.pi/1e6,))

        OMEGACI0 = omega_ci/omega_A
        ROTE = omega_0/omega_A

        lnL = 15.0
        eta0 = (mu0*(R**2)*omega_A)/(A**2) #  Normalise to MARS unit = 1/S

        etaP = (sp.pi*Zeff*(elementary_charge**2)*(me**0.5)*lnL)/(((4*sp.pi*epsilon0)**2)*(kB*Te)**1.5)

        eta_mars = etaP/eta0

        S = mu0*v_A*a**2/etaP/R # Lundquist number

        if silent==False: print('Lundquist number,\t\tS = %0.3e '%(S,))
        if silent==False: print('')        
        if silent==False: print('MARS namelist variables:')
        if silent==False: print('ETA =\t\t\t\t%0.5e' %(eta_mars,)) #'%e' % 1.0
        if silent==False: print('ROTE = ROTWE0 =\t\t\t%0.6e'%(ROTE,))
        if silent==False: print('OMEGACI0 =\t\t\t%0.6e'%(OMEGACI0,))
        mogui.rote.set(ROTE)
        mogui.eta.set(eta_mars)
        mogui.rote_short.set(eval(str('%0.4f'%(ROTE,))))
        mogui.eta_short.set(eval(str('%0.4e'%(eta_mars,))))

    def gatherB0R0(self, mogui):
      gfile_path = mogui.chease_equilib_file.get()
      if os.path.isfile(gfile_path):
        with open(gfile_path) as gfile:
            head = [next(gfile) for x in range(4)]
        if (len(head[1])<81)or(len(head[2])<81):
              print('unexpected format when gathering R0 B0 from gfile')
              return
        else:
              mogui.R0EXP.set(abs(eval(head[1][32:48])))
              mogui.B0EXP.set(abs(eval(head[2][64:80])))

    def updatePaths(self, mogui):
      print('Update all default paths with new local working directory. To be implemented.')
                  
class chease_tab_contents():

    def __init__(self, mogui):

        # Input equilib file for CHEASE
        if os.path.isfile(mogui.workingdir_local.get()+'/expeq_s_cs'):
          mogui.chease_equilib_file.set(mogui.workingdir_local.get()+'/expeq_s_cs')
          mogui.chease_equilib_file_shortened.set('...'+(mogui.workingdir_local.get()+'/expeq_s_cs')[-15:])
        else:
          mogui.chease_equilib_file_shortened.set('No Input Equilibrium File Selected')
          mogui.chease_equilib_file.set('No Input Equilibrium File Selected')
        # Select equilib file button and label
        getInputButton = tk.Button(mogui.cheaseframe, text='Select Input Equilibrium File', command=lambda: self.getInputFile(mogui), font=mogui.textfont1)
        getInputButton.grid(row=1, column=0)
        InputLabel = tk.Label(mogui.cheaseframe, textvariable=mogui.chease_equilib_file_shortened, width=30, font=mogui.textfont1)
        InputLabel.grid(row=2, column=0)

        # Output chease_log file
        mogui.logchease.set("No output file selected")
        mogui.logchease_shortened.set("No output file selected")
        # Select chease_log file button and label
        getLogCheaseButton = tk.Button(mogui.cheaseframe, text='Select Chease Log File', command=lambda: self.getlogCheaseFile(mogui), font=mogui.textfont1)
        getLogCheaseButton.grid(row=3, column=0)
        logchease_label = tk.Label(mogui.cheaseframe, textvariable=mogui.logchease_shortened, width=50, font=mogui.textfont1)
        logchease_label.grid(row=4, column=0)  

        # Boolean for whether to run mars for profeq and rmzm
        self.RunMarsBasicOption = tk.IntVar()
        self.RunMarsBasicOption.set(1)
        RunMarsBasicButton = tk.Checkbutton(mogui.cheaseframe, variable=self.RunMarsBasicOption, text="Run MARS for profeq/rmzm", font=mogui.textfont1)
        RunMarsBasicButton.grid(row=5, column=0)

        # File containing q profile from gfile 
        mogui.gproffile_shortened.set('No g_qprof File Selected')
        mogui.gproffile.set('No g_qprof File Selected')
        getGproffileButton = tk.Button(mogui.cheaseframe, text='Select g_qprof File', command=lambda: self.getGqprofFile(mogui), font=mogui.textfont1)
        getGproffileButton.grid(row=6, column=0)
        GproffileLabel = tk.Label(mogui.cheaseframe, textvariable=mogui.gproffile_shortened, width=30, font=mogui.textfont1)
        GproffileLabel.grid(row=7, column=0)

        # Profeq file from MARS run 
        if os.path.isfile(mogui.workingdir_local.get()+'/profeq'):
          mogui.profeqfile.set(mogui.workingdir_local.get()+'/profeq')
          mogui.profeqfile_shortened.set('...'+(mogui.workingdir_local.get()+'/profeq')[-15:])
        else:
          mogui.profeqfile_shortened.set('No profeq found')
        getProfeqButton = tk.Button(mogui.cheaseframe, text='Select Profeq File', command=lambda: self.getProfeqFile(mogui), font=mogui.textfont1)
        getProfeqButton.grid(row=8, column=0)
        ProfeqLabel = tk.Label(mogui.cheaseframe, textvariable=mogui.profeqfile_shortened, width=30, font=mogui.textfont1)
        ProfeqLabel.grid(row=9, column=0)

        # Plot q profiles button
        PlotQprofButton = tk.Button(mogui.cheaseframe, text="Plot profeq profiles", command=lambda: self.plotGeneralProfeq(mogui), font=mogui.textfont1)
        PlotQprofButton.grid(row=10, column=0)

        # Boolean for whether x coord of q profile is psi or s
        self.psi_or_s = tk.IntVar()
        self.psi_or_s.set(1)
        psi_or_s_Button = tk.Checkbutton(mogui.cheaseframe, variable=self.psi_or_s, text="Radial coord is s", font=mogui.textfont1)
        psi_or_s_Button.grid(row=11, column=0)

        # Launch CHEASE button and label
        launchCheaseButton = tk.Button(mogui.cheaseframe, text='Run Chease', command=lambda: self.launchChease(mogui), font=mogui.textfont1)
        launchCheaseButton.grid(row=11, column=1,sticky="ew")
        self.waitMessage = tk.StringVar()
        self.waitMessage.set("")
        waitLabel = tk.Label(mogui.cheaseframe, textvariable = self.waitMessage, width=30, font=mogui.textfont1)
        waitLabel.grid(row=16, column=1)

        # Namelist of CHEASE with all available meaning/value pairs
        mogui.NEQDSK_list=[("Expeq format",0),("Gfile format",1)]
        mogui.NCSCAL_list=[("Specify q(0)=QSPEC, MARS definition of radius",1),("Specify total plasma current",2),("Specify q(0)=QSPEC, CHEASE definition of radius",3),("No scaling",4)]
        mogui.CoordSys_list = [("PEST", (0,2)), ("Geometric", (-1,1))]

        # Set all namelist variables to default value
        mogui.NEQDSK.set(0)
        mogui.NCSCAL.set(4)
        mogui.CURRT.set(0.298)
        mogui.NEGP.set(-1)
        mogui.NER.set(1)
        mogui.QSPEC.set(1.2)
        mogui.CFBAL.set(1.00)

        # NEQDSK dropdown and label
        self.NEQDSK_name = tk.StringVar()
        self.NEQDSK_name.set("Expeq format")
        NEQDSK_label = tk.Label(mogui.cheaseframe, text="NEQDSK: Input File Format", font=mogui.textfont1)
        NEQDSK_label.grid(row=2, column=1, sticky="w")
        NEQDSK_dropdown = tk.OptionMenu(mogui.cheaseframe, mogui.NEQDSK, *[mogui.NEQDSK_list[x][1] for x in range(len(mogui.NEQDSK_list))], command=lambda x: self.updateNEQDSK_name(mogui))
        NEQDSK_dropdown.grid(row=2, column=2, sticky="w")
        NEQDSK_name_label = tk.Label(mogui.cheaseframe, textvariable=self.NEQDSK_name, font=mogui.textfont1)
        NEQDSK_name_label.grid(row=3, column=1, sticky="w",columnspan=3)

        # NCSCAL dropdown and label
        self.NCSCAL_name = tk.StringVar()
        self.NCSCAL_name.set("No scaling")
        NCSCAL_label = tk.Label(mogui.cheaseframe, text="NCSCAL: Equilibrium Scaling", font=mogui.textfont1)
        NCSCAL_label.grid(row=5, column=1, sticky="w")
        NCSCAL_dropdown = tk.OptionMenu(mogui.cheaseframe, mogui.NCSCAL, *[mogui.NCSCAL_list[x][1] for x in range(len(mogui.NCSCAL_list))], command=lambda x: self.updateNCSCAL_name(mogui))
        NCSCAL_dropdown.grid(row=5, column=2, sticky="w")
        NCSCAL_name_label = tk.Label(mogui.cheaseframe, textvariable=self.NCSCAL_name, font=mogui.textfont1)
        NCSCAL_name_label.grid(row=6, column=1, sticky="w",columnspan=3)

        # CURRT entry
        CURRT_label = tk.Label(mogui.cheaseframe, text="CURRT: Plasma Current", font=mogui.textfont1)
        CURRT_label.grid(row=7, column=1, sticky="w")
        self.CURRT_entry = tk.Entry(mogui.cheaseframe, textvariable=mogui.CURRT, width=6, font=mogui.textfont1)
        self.CURRT_entry.grid(row=7, column=2, sticky="w")
        self.CURRT_entry.config(state=tk.DISABLED)

        # QSPEC entry
        QSPEC_label = tk.Label(mogui.cheaseframe, text="QSPEC: q at s=0", font=mogui.textfont1)
        QSPEC_label.grid(row=8, column=1)
        self.QSPEC_entry = tk.Entry(mogui.cheaseframe, textvariable=mogui.QSPEC, width=6, font=mogui.textfont1)
        self.QSPEC_entry.grid(row=8, column=2, sticky="w")
        self.QSPEC_entry.config(state=tk.DISABLED)
        
        # CFBAL entry
        CFBAL_label = tk.Label(mogui.cheaseframe, text="CFBAL, pressure scaling factor", font=mogui.textfont1)
        CFBAL_label.grid(row=9, column=1)
        self.CFBAL_entry = tk.Entry(mogui.cheaseframe, textvariable=mogui.CFBAL, width=6, font=mogui.textfont1)
        self.CFBAL_entry.grid(row=9, column=2, sticky="w")

        # Coord system dropdown 
        self.CoordSys_name = tk.StringVar()
        self.CoordSys_name.set("Geometric")
        CoordSys_dropdown = tk.OptionMenu(mogui.cheaseframe, self.CoordSys_name, *[mogui.CoordSys_list[x][0] for x in range(len(mogui.CoordSys_list))], command=lambda x: self.updateCoordSys(mogui))
        CoordSys_dropdown.grid(row=10, column=2,sticky="w")
        CoordSys_label = tk.Label(mogui.cheaseframe, text="NEGP/NER: Coord System", font=mogui.textfont1)
        CoordSys_label.grid(row=10, column=1, sticky="w")

    def plotGeneralProfeq(self, mogui):
      plt.close('all')
      profeq_data=sp.loadtxt(mogui.profeqfile.get())
      s=profeq_data[:,0]
      q=profeq_data[:,1]
      j=profeq_data[:,2]
      P=profeq_data[:,3]

      mu0 = sp.pi*4e-7
      j_norm = mogui.B0EXP.get()/(mu0*mogui.R0EXP.get())
      P_norm = (mogui.B0EXP.get()**2)/mu0

      plt.figure(figsize=(14,5))
      plt.subplot(131)
      if os.path.isfile(str(mogui.gproffile.get())):
        g_qprof=sp.loadtxt(mogui.gproffile.get())
        if self.psi_or_s.get()==1:
          plt.plot(g_qprof[:,0], g_qprof[:,1], s, q)
        else:
          plt.plot(g_qprof[:,0]**0.5, g_qprof[:,1], s, q)
        plt.legend(["Gfile","Profeq"],loc=2)
      else:
        plt.plot(s,q)
      plt.xlabel('s=sqrt(Psi)', fontsize=16)
      plt.ylabel('q', fontsize=16)

      plt.subplot(132)
      plt.plot(s, j*j_norm/1e6)
      plt.xlabel('s=sqrt(Psi)', fontsize=16)
      plt.ylabel('Current Density (approx) \n (MAm$^{-2}$)', fontsize=16)

      plt.subplot(133)
      plt.plot(s, P*P_norm/1e3)
      plt.xlabel('s=sqrt(Psi)', fontsize=16)
      plt.ylabel('Plasma Pressure\n (kPa)', fontsize=16)
      plt.tight_layout()
      plt.show(block=False)

    def getGqprofFile(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        mogui.gproffile.set(selectinputfile)
        mogui.gproffile_shortened.set('...'+selectinputfile[-15:])

    def getProfeqFile(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        mogui.profeqfile.set(selectinputfile)
        mogui.profeqfile_shortened.set('...'+selectinputfile[-15:])

    def updateNCSCAL_name(self, mogui):
      for name, value in mogui.NCSCAL_list:
        if mogui.NCSCAL.get()==value:
          self.NCSCAL_name.set(name)
      if mogui.NCSCAL.get()==1 or mogui.NCSCAL.get()==3:
        self.CURRT_entry.config(state=tk.DISABLED)
        self.QSPEC_entry.config(state=tk.NORMAL)
      elif mogui.NCSCAL.get()==2:
        self.CURRT_entry.config(state=tk.NORMAL)
        self.QSPEC_entry.config(state=tk.DISABLED)
      else:
        self.CURRT_entry.config(state=tk.DISABLED)
        self.QSPEC_entry.config(state=tk.DISABLED)

    def updateNEQDSK_name(self, mogui):
      for name, value in mogui.NEQDSK_list:
        if mogui.NEQDSK.get()==value:
          self.NEQDSK_name.set(name)

    def updateCoordSys(self, mogui):
      for name, value in mogui.CoordSys_list:
        if self.CoordSys_name.get()==name:
          mogui.NEGP.set(value[0])
          mogui.NER.set(value[1])

    def getInputFile(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        mogui.chease_equilib_file.set(selectinputfile)
        mogui.chease_equilib_file_shortened.set('...'+selectinputfile[-15:])

    def getlogCheaseFile(self, mogui):
      selectoutputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectoutputfile)):
        mogui.logchease.set(selectoutputfile)
        mogui.logchease_shortened.set('...'+selectoutputfile[-15:])
        self.getIndicatorsFromLog(selectoutputfile)

    def getIndicatorsFromLog(self,filename):
      f=open(filename)
      lines=f.readlines()
      f.close()
      q95='Not found'
      gexp='Not found'
      ppf='Not found'
      for line in lines:
        if 'GEXP' in line:
          gexp = line.split()[3]
        if 'Q AT 95% FLUX SURFACE' in line:
          q95= line.split()[-1]
        if 'PRESSURE PEAKING FACTOR' in line:
          ppf = line.split()[-1]

      # these print lines[x] need to be in the if statements above, otherwise set them to 'Not found'
      print('Normalised beta = ', gexp)
      print('Self inductance li = ', lines[-14].split()[0])
      print('Pressure peaking factor = ', ppf)
      print('I (MA) = ',float(lines[-19].split(' ')[13])/1e6)
      print( 'I (Chease) =',float(lines[-19].split(' ')[4]))
      print('q_0 = ', float(lines[-13].split()[-2])) # q_0
      print('q_95 =', q95)
      print('q_a = ', float(lines[-12].split(' ')[-2])) # q_a


    def writeDatain(self, mogui):
      f=open('NamelistChease')
      lines=f.readlines()
      f.close()
      for index, line in enumerate(lines):
        if ' NTOR' in line:
            lines[index] = '  NTOR=%d,\n'%(mogui.NTOR.get(),)
        if ' NEQDSK' in line:
            lines[index] = '  NEQDSK=%d,\n'%(mogui.NEQDSK.get(),)
        if ' CFBAL' in line:
            lines[index] = '  CFBAL=%0.10f,\n'%(mogui.CFBAL.get(),)
        if ' QSPEC' in line:
            lines[index] = '  QSPEC =%0.10f,\n'%(mogui.QSPEC.get(),)
        if ' NCSCAL' in line:
            lines[index] = '  NCSCAL=%d,\n'%(mogui.NCSCAL.get(),)
        if ' CURRT' in line:
            lines[index] = '  CURRT=%0.10f,\n'%(mogui.CURRT.get(),)
        if ' NEGP' in line:
            lines[index] = '  NEGP=%d,\n'%(mogui.NEGP.get(),)
        if ' NER' in line:
            lines[index] = '  NER=%d,\n'%(mogui.NER.get(),)
        if ' B0EXP' in line:
            lines[index] = '  B0EXP=%0.10f,\n'%(mogui.B0EXP.get(),)
        if ' R0EXP' in line:
            lines[index] = '  R0EXP=%0.10f,\n'%(mogui.R0EXP.get(),)

      f=open('datain','w')
      f.writelines(lines)
      f.close()

      f=open('BasicMars')
      lines=f.readlines()
      f.close()

      for index, line in enumerate(lines):
        if 'NCONVCS' in line:
          if self.CoordSys_name.get()=="Geometric":
            lines[index] =' NCONVCS= 1,\n'
          if self.CoordSys_name.get()=="PEST":
            lines[index] =' NCONVCS= 0,\n'

      f=open('BasicMars','w')
      f.writelines(lines)
      f.close()

    def getCheaseOuput(self, mogui):
      if mogui.local_mode.get()==1: # running in local mode. 

        curr_date = dt.datetime.now()
        if os.path.isfile(mogui.workingdir_local.get()+'/EXPEQ.OUT'):
          expeq_date = dt.datetime.fromtimestamp(os.stat(mogui.workingdir_local.get()+'/EXPEQ.OUT').st_mtime)
          delta_t = curr_date - expeq_date
          if delta_t.seconds<120:
              print(' --------------------------- ')
              print(' ')
              print('New EXPEQ.OUT found')
              print(' ') 
              print(' --------------------------- ')
              new_expeq = True
          else:
              print(' --------------------------- ')
              print(' ')
              print('EXPEQ.OUT found, but >120s old.')
              print(' ') 
              print(' --------------------------- ')
              new_expeq = False
        else:
            print(' --------------------------- ')
            print(' ')
            print('EXPEQ.OUT not found.')
            print(' ') 
            print(' --------------------------- ')
            new_expeq = False

        if os.path.isfile(mogui.workingdir_local.get()+'/EXPEQG.OUT'):
          expeqg_date = dt.datetime.fromtimestamp(os.stat(mogui.workingdir_local.get()+'/EXPEQG.OUT').st_mtime)
          delta_t = curr_date - expeqg_date
          if delta_t.seconds<120:
              print(' --------------------------- ')
              print(' ')
              print('New EXPEQG.OUT found')
              print(' ') 
              print(' --------------------------- ')
              new_expeqg = True
          else:
              print(' --------------------------- ')
              print(' ')
              print('EXPEQG.OUT found, but >120s old.')
              print(' ') 
              print(' --------------------------- ')
              new_expeqg = False
        else:
            print(' --------------------------- ')
            print(' ')
            print('EXPEQ.OUT not found.')
            print(' ') 
            print(' --------------------------- ')
            new_expeqg = False

        if new_expeq==True:
          os.system('mv %s/EXPEQ.OUT %s/expeq_out'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if new_expeqg==True:
          os.system('mv %s/EXPEQG.OUT %s/expeq_g_out'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if new_expeq==True:
          return True
        else:
          return False

      else: # running in remote mode
        p = pex.spawnu('ssh %s@gpu005'%(mogui.uname_gpu005.get(),), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.sendline('cd %s'%(mogui.workingdir_gpu005.get(),))
        p.expect('$')
        p.sendline('date -r EXPEQ.OUT')
        p.expect('$')
        p.sendline('date -r EXPEQG.OUT')
        p.expect('$')
        p.sendline('date')
        p.expect('$')
        p.sendline('logout')
        output = p.readlines()
        p.terminate(force=True)
        p.close()
    
        expeq_date_line=""
        expeqg_date_line=""

        for index, line in enumerate(output):
            if '$ date' in line:
               if '$ date -r EXPEQ.OUT' in line:
                  expeq_date_line=output[index+1]
               elif '$ date -r EXPEQG.OUT' in line:
                  expeqg_date_line=output[index+1]
               else:
                  curr_date_line=output[index+1]

        # patch for bug, sometimes 'logout' would appear at start of date output line, and should be removed 
        if 'logout' in expeq_date_line: expeq_date_line = expeq_date_line.split('logout')[1]
        if 'logout' in expeqg_date_line: expeqg_date_line = expeqg_date_line.split('logout')[1]
        if 'logout' in curr_date_line: curr_date_line = curr_date_line.split('logout')[1]

        # if bug returns, suggest more thorough checking system for ***_date_line variables:
        # check if first 3 characters == 'Mon' or 'Tue' etc
        # and check if last 4 (2?) characters are '\r\n'. 
        # if either test fails then retry getting date, or just output a useful error.

        try:
          curr_date = dt.datetime.strptime(curr_date_line, '%a %b %d %H:%M:%S %Z %Y\r\n')
        except ValueError:
          curr_date = dt.datetime.now()
          print('Val error at curr_date: ', curr_date_line)
          print('')
          print(output)
        # check if EXPEQ.OUT exists, and is less than 120 seconds old
        if 'o such file' not in expeq_date_line and expeq_date_line!="":
            try:
              expeq_date = dt.datetime.strptime(expeq_date_line, '%a %b %d %H:%M:%S %Z %Y\r\n')
            except ValueError:
              expeq_date = dt.datetime.now()
              print('Val error at expeq_date: ', expeq_date_line)
              print('')
              print(output)
            delta_t = curr_date - expeq_date
            if delta_t.seconds<120:
                print(' --------------------------- ')
                print(' ')
                print('New EXPEQ.OUT found')
                print(' ') 
                print(' --------------------------- ')
                new_expeq = True
            else:
                print(' --------------------------- ')
                print(' ')
                print('EXPEQ.OUT found, but >120s old.')
                print(' ') 
                print(' --------------------------- ')
                new_expeq = False
        else:
            print(' --------------------------- ')
            print(' ')
            print('EXPEQ.OUT not found.')
            print(' ') 
            print(' --------------------------- ')
            new_expeq = False

        if 'o such file' not in expeqg_date_line and expeqg_date_line!="":
            try:
              expeqg_date = dt.datetime.strptime(expeqg_date_line, '%a %b %d %H:%M:%S %Z %Y\r\n')
            except ValueError:
              expeqg_date = dt.datetime.now()
              print('Val error at expeqg_date: ', expeqg_date_line)
              print('')
              print(output)
            delta_t = curr_date - expeqg_date
            if delta_t.seconds<120:
                print(' --------------------------- ')
                print(' ')
                print('New EXPEQG.OUT found')
                print(' ') 
                print(' --------------------------- ')
                new_expeqg = True
            else:
                print(' --------------------------- ')
                print(' ')
                print('EXPEQG.OUT found, but >120s old.')
                print(' ') 
                print(' --------------------------- ')
                new_expeqg = False
        else:
            print(' --------------------------- ')
            print(' ')
            print('EXPEQG.OUT not found.')
            print(' ') 
            print(' --------------------------- ')
            new_expeqg = False

        # Get log_chease
        p = pex.spawnu('scp %s@gpu005:%s/log_chease %s/log_chease'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

        # If a new EXPEQ.OUT file found, collect it.
        if new_expeq==True:
          p = pex.spawnu('scp %s@gpu005:%s/EXPEQ.OUT %s/expeq_out'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()

        # If a new EXPEQG.OUT file found, collect it.
        if new_expeqg==True:
          p = pex.spawnu('scp %s@gpu005:%s/EXPEQG.OUT %s/expeq_g_out'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()

        if new_expeq==True:
          return True
        else:
          return False

    def sendCheaseInput(self, mogui):

      if mogui.local_mode.get()==1: # running in local mode. 
          print(' ')
          print(' Copying namelists to working directory, and input equilibrium to EXPEQ')
          print(' ')
          os.system('cp %s %s/EXPEQ'%(mogui.chease_equilib_file.get(), mogui.workingdir_local.get()))
          os.system('cp datain %s'%(mogui.workingdir_local.get(),))
          os.system('cp BasicMars %s/RUN.IN'%(mogui.workingdir_local.get(),))
          print(' Done')
          print(' ')
      else: ## running in remote mode
          print(' --------------------------- ')
          print(' ')
          print(' Copying input equilibrium to GPU005 via scp')

          # scp expeq or gfile over to GPU005
          p = pex.spawnu('scp %s %s@gpu005:%s/EXPEQ'%(mogui.chease_equilib_file.get(), mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
          print('   Done...')
          print(' ') 
          print(' --------------------------- ')

          print(' --------------------------- ')
          print(' ')
          print(' Copying DATAIN (contains chease namelist) to GPU005 via scp')

          # scp datain (containing chease namelist variables) over to GPU005
          p = pex.spawnu('scp datain %s@gpu005:%s'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
          print('   Done...')
          print(' ') 
          print(' --------------------------- ')

          print(' --------------------------- ')
          print(' ')
          print(' Copying BasicMars (contains mars namelist) to RUN.IN on GPU005 via scp')

          # scp BasicMars to RUN.IN on GPU005
          p = pex.spawnu('scp BasicMars %s@gpu005:%s/RUN.IN'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
          print('   Done...')
          print(' ') 
          print(' --------------------------- ')

    def runChease(self, mogui):
      if mogui.local_mode.get()==1: # running in local mode. 
        print(' --------------------------- ')
        print(' ')
        print(' Running CHEASE locally')
        start_time = dt.datetime.now()
        os.system('cd %s; rm OUT*MAR'%(mogui.workingdir_local.get(),))
        os.system('cd %s; /home/dryan/MarsQ_Package_v1.0/CheaseMerge/chease.x < datain  > log_chease'%(mogui.workingdir_local.get(),))
        os.system('cd %s; rm EQU01 EQU02 EQU03 ETAVAC INP1 INP1_FORMATTED JACOB_F JSOLVER NDES NGA NOUT NSAVE NUPLO RZPEEL'%(mogui.workingdir_local.get(),))
        end_time = dt.datetime.now()
        delta_t = end_time - start_time
        print(' ...CHEASE finished in around', delta_t.seconds, 'seconds.')
        print(' ') 
        print(' --------------------------- ')

      else: ## running in remote mode
        print(' --------------------------- ')
        print(' ')
        print(' Running CHEASE on GPU005 via ssh')
        p = pex.spawnu('ssh %s@gpu005'%(mogui.uname_gpu005.get(),), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.sendline('cd %s'%(mogui.workingdir_gpu005.get(),))
        p.expect('$')
        p.sendline('rm OUT*MAR')
        start_time = dt.datetime.now()
        p.expect('$')
        p.sendline('time /home/dryan/MarsQ_Package_v1.0/CheaseMerge/chease.x < datain  > log_chease')
        p.expect('$')
        p.sendline('rm EQU01 EQU02 EQU03 ETAVAC INP1 INP1_FORMATTED JACOB_F JSOLVER NDES NGA NOUT NSAVE NUPLO RZPEEL')
        p.expect('$')
        p.sendline('logout')
        p.expect('$')
        output = p.readlines()
        p.terminate(force=True)
        p.close()
        end_time = dt.datetime.now()
        delta_t = end_time - start_time
        print(' ...CHEASE finished in around', delta_t.seconds, 'seconds.')
        print(' ') 
        print(' --------------------------- ')

    def runMarsBasic(self, mogui):
      if mogui.local_mode.get()==1: # running in local mode. 
        print(' --------------------------- ')
        print(' ')
        print(' Running basic MARS locally')
        start_time = dt.datetime.now()
        os.system('cd %s; /home/dryan/MarsQ/marsq_v1.8.x > log_mars'%(mogui.workingdir_local.get(),))
        os.system('cd %s; rm B123U2.OUT B123U.OUT B1UP.OUT B1US00.OUT B2U.OUT B3U.OUT BFEEDF.OUT BNORM01.OUT CFEED.OUT CURRSENS.OUT EFAF.OUT ENERGY_ANALYZE_INPUT.OUT ENERGY.OUT J2UWF.OUT EQ4T7.OUT FEEDI.OUT FLUX.OUT J3UMAX.OUT J3UWF.OUT JACOBIAN.OUT JFD.OUT JPS.OUT JRW.OUT METRICS.OUT PPVISC.OUT PROFDISP.OUT PROFNTV.OUT PROFOFFSET.OUT SHELLRESULT.OUT TMP_JB.OUT TORQUEJXB.OUT TORQUENTV.OUT TORQUEREY.OUT PROFNUSTAR.OUT TORQUENTV2.OUT RESULT.OUT *PLASMA.OUT B1US0.OUT Error_Messages'%(mogui.workingdir_local.get(),))
        end_time = dt.datetime.now()
        delta_t = end_time - start_time
        print(' ...Basic MARS finished in around', delta_t.seconds, 'seconds.')
        print(' ') 
        print(' --------------------------- ')

      else:# running in remote mode. 
        print(' --------------------------- ')
        print(' ')
        print(' Running MARS on GPU005 via ssh')
        p = pex.spawnu('ssh %s@gpu005'%(mogui.uname_gpu005.get(),), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.sendline('cd %s'%(mogui.workingdir_gpu005.get(),))
        p.expect('$')
        p.sendline('module load icc; module load openmpi_icc; module load ifort/12.0.1.107')
        start_time = dt.datetime.now()
        p.expect('$')
        #p.sendline('time /home/dryan/MarsQ/marsq_v1.5.x > log_mars') # M1,M2=-59,59
        #p.sendline('time /home/dryan/MarsQ/marsq_v1.4.x > log_mars')
        p.sendline('time /home/dryan/MarsQ/marsq_v1.3.x > log_mars') # M1,M2=-29,29
        p.expect('$')
        p.sendline('rm B123U2.OUT B123U.OUT B1UP.OUT B1US00.OUT B2U.OUT B3U.OUT BFEEDF.OUT BNORM01.OUT CFEED.OUT CURRSENS.OUT EFAF.OUT ENERGY_ANALYZE_INPUT.OUT ENERGY.OUT J2UWF.OUT EQ4T7.OUT FEEDI.OUT FLUX.OUT J3UMAX.OUT J3UWF.OUT JACOBIAN.OUT JFD.OUT JPS.OUT JRW.OUT METRICS.OUT PPVISC.OUT PROFDISP.OUT PROFNTV.OUT PROFOFFSET.OUT SHELLRESULT.OUT TMP_JB.OUT TORQUEJXB.OUT TORQUENTV.OUT TORQUEREY.OUT')
        p.expect('$')
        p.sendline('logout')
        p.expect('$')
        output = p.readlines()
        p.terminate(force=True)
        p.close()
        end_time = dt.datetime.now()
        delta_t = end_time - start_time
        print(' ...Basic MARS finished in around', delta_t.seconds, 'seconds.')
        print(' ') 
        print(' --------------------------- ')

    def collectMarsBasicOuput(self, mogui):

      if mogui.local_mode.get()==1: # running in local mode. 
        os.system('mv %s/PROFEQ.OUT %s/profeq'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        os.system('mv %s/RMZM_F.OUT %s/rmzm'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

      else:
        p = pex.spawnu('scp %s@gpu005:%s/PROFEQ.OUT %s/profeq'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

        p = pex.spawnu('scp %s@gpu005:%s/RMZM_F.OUT %s/rmzm'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

      if self.CoordSys_name.get()=="Geometric":
        os.system('mv %s/rmzm %s/rmzm_geom'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
      if self.CoordSys_name.get()=="PEST":
        os.system('mv %s/rmzm %s/rmzm_pest'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))


    def launchChease(self, mogui, silent=False):
      if mogui.local_mode.get()==0: # running in remote mode. 
        if mogui.uname_gpu005.get()=="" or mogui.pw_gpu005.get()=="":
          print('Missing username or password')
          return
      if silent==False:
        self.waitMessage.set("Waiting for Chease to complete...")
        mogui.update()

      print(' ')
      print(' ++++++++++++++++++++++++++++++ ')
      print(' ')

      self.writeDatain(mogui)
      self.sendCheaseInput(mogui)
      self.runChease(mogui)

      chease_succeeded = self.getCheaseOuput(mogui)
      if chease_succeeded:
        path = mogui.workingdir_local.get()+'/log_chease'
        if os.path.isfile(path):
          mogui.logchease.set(path)
          mogui.logchease_shortened.set('...'+path[-15:])
          self.getIndicatorsFromLog(path)
        if self.RunMarsBasicOption.get()==1:
          self.runMarsBasic(mogui)
          self.collectMarsBasicOuput(mogui)

      print(' ') 
      print(' ############################## ')
      print(' ')


      if silent==False:
        self.waitMessage.set("Done")
        mogui.update()
        mogui.after(250)
        self.waitMessage.set("")
        messagebox.showinfo("Ahoy!", "CHEASE run finished.")




class boundsmoother_tab_contents():

  def __init__(self, mogui):
    mogui.boundsmootherframe.columnconfigure(0,minsize=100)
    mogui.boundsmootherframe.rowconfigure(0,minsize=30)
    self.R=[]
    self.Z=[]

    # Smoothing factor variable and entry box
    smoothing_factor_label = tk.Label(mogui.boundsmootherframe, text="smoothing factor", font=mogui.textfont1)
    smoothing_factor_label.grid(row=2, column=1, padx=5)
    self.SmoothingFactor = tk.DoubleVar()
    self.SmoothingFactor.set('1e-5')
    self.SmoothingFactorEntry = tk.Entry(mogui.boundsmootherframe, textvariable=self.SmoothingFactor, width=10, font=mogui.textfont1, justify=tk.CENTER)
    self.SmoothingFactorEntry.grid(row=3, column=1, padx=5)

    calcBoundaryButton = tk.Button(mogui.boundsmootherframe, text="Smooth boundary", command = lambda: self.calcBoundary(mogui), font=mogui.textfont1)
    calcBoundaryButton.grid(row=3, column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    # Input expeq_out file
    if os.path.isfile(mogui.workingdir_local.get() + '/expeq_out'):
      mogui.boundsmoother_equilib_file.set(mogui.workingdir_local.get() + '/expeq_out')
      mogui.boundsmoother_equilib_file_shortened.set('...'+mogui.boundsmoother_equilib_file.get()[-25:])
    else:
      mogui.boundsmoother_equilib_file_shortened.set('no expeq_out found')

    # Select expeq file button and label
    self.getExpeqFileButton = tk.Button(mogui.boundsmootherframe, text='Select EXPEQ File', command = lambda: self.getExpeqFile(mogui), font=mogui.textfont1)
    self.getExpeqFileButton.grid(row=1, column=1, padx=5)
    self.boundsmoother_equilib_file_label = tk.Label(mogui.boundsmootherframe, textvariable=mogui.boundsmoother_equilib_file_shortened, font=mogui.textfont1, justify=tk.LEFT)
    self.boundsmoother_equilib_file_label.grid(row=1, column=2, sticky=tk.N+tk.S+tk.W, padx=5)

    # Upper shunt list
    self.upper_shuntlist_label = tk.Label(mogui.boundsmootherframe, text="List of upper z_shift values 1e-3", font=mogui.textfont1)
    self.upper_shuntlist_label.grid(row=2, column=2, padx=5)
    UpperShuntListEntry = tk.Entry(mogui.boundsmootherframe, textvariable=mogui.UpperShuntList, width=30, font=mogui.textfont1)
    UpperShuntListEntry.grid(row=3, column=2, padx=5)

    # Lower shunt list
    self.lower_shuntlist_label = tk.Label(mogui.boundsmootherframe, text="List of lower z_shift values 1e-3", font=mogui.textfont1)
    self.lower_shuntlist_label.grid(row=4, column=2, padx=5)
    LowerShuntListEntry = tk.Entry(mogui.boundsmootherframe, textvariable=mogui.LowerShuntList, width=30, font=mogui.textfont1)
    LowerShuntListEntry.grid(row=5, column=2, padx=5)

    # Interp points
    mogui.InterpPoints.set(100)
    InterpPoints_label = tk.Label(mogui.boundsmootherframe, text="Interp Points", font=mogui.textfont1)
    InterpPoints_label.grid(row=4, column=1, padx=5)
    InterpPoints_entry = tk.Entry(mogui.boundsmootherframe, textvariable=mogui.InterpPoints, width=10, font=mogui.textfont1, justify=tk.CENTER)
    InterpPoints_entry.grid(row=5, column=1, padx=5)
  
    # Save expeq_s button 
    saveButton = tk.Button(mogui.boundsmootherframe, text="Save Smoothed Boundary", command= lambda: self.saveBoundary(mogui), font=mogui.textfont1)
    saveButton.grid(row=5, column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    if os.path.isfile(mogui.boundsmoother_equilib_file.get()):
        f=open(mogui.boundsmoother_equilib_file.get())
        lines=f.readlines()
        f.close()
        numRZ=eval(lines[3].strip().split()[0])

        self.R = sp.array([eval(i.strip().split()[0]) for i in lines[4:numRZ+4]])
        self.Z = sp.array([eval(i.strip().split()[1]) for i in lines[4:numRZ+4]])


  def getExpeqFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      mogui.boundsmoother_equilib_file.set(selectinputfile)
      mogui.boundsmoother_equilib_file_shortened.set('...'+selectinputfile[-25:])
      
    f=open(mogui.boundsmoother_equilib_file.get())
    lines=f.readlines()
    f.close()
    numRZ=eval(lines[3].strip().split()[0])

    self.R = sp.array([eval(i.strip().split()[0]) for i in lines[4:numRZ+4]])
    self.Z = sp.array([eval(i.strip().split()[1]) for i in lines[4:numRZ+4]])

  def calcBoundary(self, mogui):
    if not os.path.isfile(mogui.boundsmoother_equilib_file.get()):
      self.getExpeqFile(mogui)

    R=self.R
    Z=self.Z
    smoothing_factor = self.SmoothingFactor.get()

    R0EXP = mogui.R0EXP.get()
    Z0EXP = (Z.max()+Z.min())/2.0


    t = sp.arctan2(Z*R0EXP - Z0EXP,R*R0EXP - R0EXP)

    # Get unique sorted values of t, and also indices of these values
    t, indices = sp.unique(t, return_index=True)
    R = R[indices]
    Z = Z[indices]


    tp=sp.linspace(t.min(), t.max(), mogui.InterpPoints.get())
    Rp = UnivariateSpline(t, R,s=smoothing_factor)(tp)
    Zp = UnivariateSpline(t, Z,s=smoothing_factor)(tp)

    if mogui.LowerShuntList.get()!="":
      lowershuntlist = [float(s)*1e-3 for s in mogui.LowerShuntList.get().strip().split(" ")]
      
      Zp_sorted = sp.sort(Zp)
      lowershuntIndices=[]
      for i in range(len(lowershuntlist)):
        lowershuntIndices.append(sp.where(Zp==Zp_sorted[i])[0][0])
      for i in range(len(lowershuntIndices)):
        Zp[lowershuntIndices[i]]+=lowershuntlist[i]*R0EXP
    if mogui.UpperShuntList.get()!="":
      uppershuntlist = [float(s)*1e-3 for s in mogui.UpperShuntList.get().strip().split(" ")]
      
      Zp_sorted = sp.sort(Zp)
      uppershuntIndices=[]
      for i in range(len(uppershuntlist)):
        uppershuntIndices.append(sp.where(Zp==Zp_sorted[-i-1])[0][0])
      for i in range(len(uppershuntIndices)):
        Zp[uppershuntIndices[i]]-=uppershuntlist[i]*R0EXP
    

    self.Rp=Rp
    self.Zp=Zp
    plt.clf()
    plt.subplot2grid((2,2), (0,0), rowspan=2)
    plt.plot(R*R0EXP,Z*R0EXP,'--')
    plt.plot(Rp*R0EXP,Zp*R0EXP,'-')
    plt.axis('equal')
    plt.xlabel('R')
    plt.ylabel('Z')
   
    # Zoom in on upper x point in subplot
    plt.subplot2grid((2,2), (0,1))
    plt.plot(R*R0EXP,Z*R0EXP,'-')
    plt.plot(Rp*R0EXP,Zp*R0EXP,'-o')
    plt.axis('equal')
    if mogui.UpperShuntList.get()!="":
      for i in range(len(uppershuntlist)):
        plt.text(Rp[uppershuntIndices[i]]*R0EXP,Zp[uppershuntIndices[i]]*R0EXP+0.01,i+1)
    ZmaxInd = sp.where(Z==Z.max())[0][0]

    plt.xlim([(R[ZmaxInd]-0.08)*R0EXP,(R[ZmaxInd]+0.14)*R0EXP])
    plt.ylim([(Z[ZmaxInd]-0.04)*R0EXP,(Z[ZmaxInd]+0.14)*R0EXP])
    plt.legend(['original', 'smoothed'])
    # Zoom in on lower x point in subplot
    plt.subplot2grid((2,2), (1,1))
    plt.plot(R*R0EXP,Z*R0EXP,'-')
    plt.plot(Rp*R0EXP,Zp*R0EXP,'-o')
    plt.axis('equal')
    if mogui.LowerShuntList.get()!="":
      for i in range(len(lowershuntlist)):
        plt.text(Rp[lowershuntIndices[i]]*R0EXP,Zp[lowershuntIndices[i]]*R0EXP+0.01,i+1)
    ZminInd = sp.where(Z==Z.min())[0][0]

    plt.xlim([(R[ZminInd]-0.08)*R0EXP,(R[ZminInd]+0.14)*R0EXP])
    plt.ylim([(Z[ZminInd]-0.04)*R0EXP,(Z[ZminInd]+0.14)*R0EXP])
    plt.legend(['original', 'smoothed'])

    plt.show(block=False)

  def saveBoundary(self, mogui):
    f=open(mogui.boundsmoother_equilib_file.get())
    lines=f.readlines()
    f.close()
    numRZ=eval(lines[3].strip().split()[0])
   
    # Replace number of points with new value
    lines[3] = "  %d     %s    %s\n"%(mogui.InterpPoints.get(),lines[3].strip().split()[1],lines[3].strip().split()[2])

    # Delete old unsmoothed boundary
    del lines[4:numRZ+4]
    # Write new smoothed boundary
    for i in range(mogui.InterpPoints.get()):
      lines.insert(4,'    %0.9f    %0.9f\n'%(self.Rp[i],self.Zp[i]))

    f=open(mogui.workingdir_local.get() + '/expeq_s','w')
    f.writelines(lines)
    f.close()

    print('expeq_s saved to ',mogui.workingdir_local.get())



class coilsurf_tab_contents():

  def __init__(self, mogui):
    mogui.coilsurfframe.columnconfigure(0,minsize=100)
    mogui.coilsurfframe.rowconfigure(0,minsize=30)
    mogui.coilsurfframe.rowconfigure(3,minsize=35)
    mogui.coilsurfframe.rowconfigure(5,minsize=35)
    self.R=[]
    self.Z=[]

    drawSurfaceButton = tk.Button(mogui.coilsurfframe, text="Draw Coil Surface", command=lambda: self.drawCoilSurface(mogui), font=mogui.textfont1)
    drawSurfaceButton.grid(row=4, column=4, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    
    # Entry boxes for fitting parameters
    label1 = tk.Label(mogui.coilsurfframe,text='R0 offset', font=mogui.textfont1)
    label1.grid(row=3, column=2, sticky=tk.S+tk.E+tk.W, padx=5)
    R0_offset_entry = tk.Entry(mogui.coilsurfframe, textvariable=mogui.R_offset, width=15, font=mogui.textfont1, justify=tk.CENTER)
    R0_offset_entry.grid(row=4, column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    label2 = tk.Label(mogui.coilsurfframe,text='Z0 offset', font=mogui.textfont1)
    label2.grid(row=3, column=3, sticky=tk.S+tk.E+tk.W, padx=5)
    Z0_offset_entry = tk.Entry(mogui.coilsurfframe, textvariable=mogui.Z_offset, width=15, font=mogui.textfont1, justify=tk.CENTER)
    Z0_offset_entry.grid(row=4, column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    label3 = tk.Label(mogui.coilsurfframe,text='R stretch', font=mogui.textfont1)
    label3.grid(row=5, column=2, sticky=tk.S+tk.E+tk.W, padx=5)
    R_stretch_entry = tk.Entry(mogui.coilsurfframe, textvariable=mogui.R_stretch, width=15, font=mogui.textfont1, justify=tk.CENTER)
    R_stretch_entry.grid(row=6, column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    label4 = tk.Label(mogui.coilsurfframe,text='Z stretch', font=mogui.textfont1)
    label4.grid(row=5, column=3, sticky=tk.S+tk.E+tk.W, padx=5)
    Z_stretch_entry = tk.Entry(mogui.coilsurfframe, textvariable=mogui.Z_stretch, width=15, font=mogui.textfont1, justify=tk.CENTER)
    Z_stretch_entry.grid(row=6, column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    label5 = tk.Label(mogui.coilsurfframe,text='Coil RZ locations', font=mogui.textfont1, justify=tk.LEFT)
    label5.grid(row=1, column=1, sticky=tk.N+tk.S+tk.W, padx=5)
    RZCoil_entry = tk.Entry(mogui.coilsurfframe, textvariable=mogui.RZ_coilstring, justify=tk.LEFT, width=60, font=mogui.textfont1)
    RZCoil_entry.grid(row=2, column=1, columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5) 

    # Input expeq_s file
    if os.path.isfile(mogui.workingdir_local.get() + '/expeq_s'):
      mogui.coilsurf_expeqfile.set(mogui.workingdir_local.get() + '/expeq_s')
      mogui.coilsurf_expeqfile_shortened.set('...'+mogui.coilsurf_expeqfile.get()[-20:])
    else:
      mogui.coilsurf_expeqfile_shortened.set('No expeq_s found')

    # Select file button and label
    getExpeqFileButton = tk.Button(mogui.coilsurfframe, text='Select EXPEQ File', command=lambda: self.getExpeqFile(mogui), font=mogui.textfont1)
    getExpeqFileButton.grid(row=3, column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    coilsurf_expeqfile_label = tk.Label(mogui.coilsurfframe, textvariable=mogui.coilsurf_expeqfile_shortened, width=20, font=mogui.textfont1)
    coilsurf_expeqfile_label.grid(row=4, column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    

    # Save expeq_s_cs button 
    saveButton = tk.Button(mogui.coilsurfframe, text="Save coil surface\nto expeq_s_cs", command=lambda: self.saveSurface(mogui), font=mogui.textfont1)
    saveButton.grid(row=5, column=4, rowspan=2, sticky=tk.S+tk.E+tk.W, padx=5)

    # Autofit button
    autofitButton = tk.Button(mogui.coilsurfframe, text="Autofit surface", command=lambda: self.autofitSurface(mogui), font=mogui.textfont1)
    autofitButton.grid(row=6, column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    if os.path.isfile(mogui.coilsurf_expeqfile.get()):
        f=open(mogui.coilsurf_expeqfile.get())
        lines=f.readlines()
        f.close()
        numRZ=eval(lines[3].strip().split()[0])

        self.R = sp.array([eval(i.strip().split()[0]) for i in lines[4:numRZ+4]])
        self.Z = sp.array([eval(i.strip().split()[1]) for i in lines[4:numRZ+4]])


  def getExpeqFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      mogui.coilsurf_expeqfile.set(selectinputfile)
      mogui.coilsurf_expeqfile_shortened.set('...'+selectinputfile[-20:])
      
    f=open(mogui.coilsurf_expeqfile.get())
    lines=f.readlines()
    f.close()
    numRZ=eval(lines[3].strip().split()[0])

    self.R = sp.array([eval(i.strip().split()[0]) for i in lines[4:numRZ+4]])
    self.Z = sp.array([eval(i.strip().split()[1]) for i in lines[4:numRZ+4]])


  def autofitSurface(self, mogui):
    def calc_min_distance(fit_params, R, Z, R0EXP):
      Ro = fit_params[0]
      Zo = fit_params[1]
      Rs = fit_params[2]
      Zs = fit_params[3]

      R0 = (max(R)+min(R))/2 + Ro;
      Z0 = (max(Z)+min(Z))/2 + Zo;
      
      R1 = R0 + Rs*(R-R0);
      Z1 = Z0 + Zs*(Z-Z0);



      coilsRZ=[]
      RC = []
      ZC = []
      for j in mogui.RZ_coilstring.get().split(';'):
        coilsRZ.append([eval(i) for i in j.split(',')])
      coilsRZ = [val for sublist in coilsRZ for val in sublist]
      RC = coilsRZ[::2]
      ZC = coilsRZ[1::2]
      mindist=0

      for i in range(len(RC)):
        mindist+=min((R1*R0EXP-RC[i])**2 + (Z1*R0EXP-ZC[i])**2)
      if R1.min()>R.min():  
        mindist+=10.0
      if R1.max()<R.max():  
        mindist+=10.0
      if Z1.min()>Z.min():  
        mindist+=10.0
      if Z1.max()<Z.max():  
        mindist+=10.0
      return mindist

    if not os.path.isfile(mogui.coilsurf_expeqfile.get()):
      self.getExpeqFile(mogui)

    params = [mogui.R_offset.get(), mogui.Z_offset.get(), mogui.R_stretch.get(), mogui.Z_stretch.get()]
    res = minimize(calc_min_distance, params, (self.R, self.Z, mogui.R0EXP.get()))
    mogui.R_offset.set(res['x'][0])
    mogui.Z_offset.set(res['x'][1])
    mogui.R_stretch.set(res['x'][2])
    mogui.Z_stretch.set(res['x'][3])

    self.drawCoilSurface(mogui)


  
  def drawCoilSurface(self,mogui):

    if not os.path.isfile(mogui.coilsurf_expeqfile.get()):
      self.getExpeqFile(mogui)

    R=self.R
    Z=self.Z
    
    R0EXP = mogui.R0EXP.get()
    Z0EXP = (Z.max()+Z.min())/2.0

    coilsRZ=[]
    RC = []
    ZC = []

    numcoils = len(mogui.RZ_coilstring.get().split(';'))
    if numcoils==0:
      print('Error: no coil RZ input detected.')
      return

    for j in mogui.RZ_coilstring.get().split(';'):
      coilsRZ.append([eval(i) for i in j.split(',')])
    coilsRZ = [val for sublist in coilsRZ for val in sublist]
    RC = coilsRZ[::2]
    ZC = coilsRZ[1::2]

    R0 = (max(R)+min(R))/2 + mogui.R_offset.get();
    Z0 = (max(Z)+min(Z))/2 + mogui.Z_offset.get();
    self.R1 = R0 + mogui.R_stretch.get()*(R-R0);
    self.Z1 = Z0 + mogui.Z_stretch.get()*(Z-Z0);

    plt.clf()
    if numcoils==2:
      Rcoil_upper=(RC[0]+RC[1])/2
      Rcoil_lower=(RC[2]+RC[3])/2

      Zcoil_upper=(ZC[0]+ZC[1])/2
      Zcoil_lower=(ZC[2]+ZC[3])/2

      plt.subplot2grid((2,2), (0,1))
      plt.plot(R*R0EXP,Z*R0EXP)
      plt.plot(self.R1*R0EXP,self.Z1*R0EXP,'--')
      plt.plot(RC,ZC,'x', markeredgewidth=2.0, markersize=5.0)
      plt.xlim([Rcoil_upper-0.2, Rcoil_upper+0.2])
      plt.ylim([Zcoil_upper-0.2, Zcoil_upper+0.2])
      plt.xlabel('R')
      plt.ylabel('Z')

      plt.subplot2grid((2,2), (1, 1))
      plt.plot(R*R0EXP,Z*R0EXP)
      plt.plot(self.R1*R0EXP,self.Z1*R0EXP,'--')
      plt.plot(RC,ZC,'x', markeredgewidth=2.0, markersize=5.0)
      plt.xlim([Rcoil_lower-0.2, Rcoil_lower+0.2])
      plt.ylim([Zcoil_lower-0.2, Zcoil_lower+0.2])
      plt.xlabel('R')
      plt.ylabel('Z')

    if numcoils==1:
      Rcoil_upper=(RC[0]+RC[1])/2

      Zcoil_upper=(ZC[0]+ZC[1])/2

      plt.subplot2grid((2,2), (0,1))
      plt.plot(R*R0EXP,Z*R0EXP)
      plt.plot(self.R1*R0EXP,self.Z1*R0EXP,'--')
      plt.plot(RC,ZC,'x', markeredgewidth=2.0, markersize=5.0)
      plt.xlim([Rcoil_upper-0.2, Rcoil_upper+0.2])
      plt.ylim([Zcoil_upper-0.2, Zcoil_upper+0.2])
      plt.xlabel('R')
      plt.ylabel('Z')
    if numcoils!=2 and numcoils !=1:
      print('Number of coilsets > 2: Subplots disabled')
      plt.plot(R*R0EXP,Z*R0EXP)
      plt.plot(self.R1*R0EXP,self.Z1*R0EXP,'--')
      plt.plot(RC,ZC,'x', markeredgewidth=2.0, markersize=5.0)
      plt.axis('equal')
      plt.xlabel('R')
      plt.ylabel('Z')
    else:
      plt.subplot2grid((2,2), (0,0), rowspan=2)
      plt.plot(R*R0EXP,Z*R0EXP)
      plt.plot(self.R1*R0EXP,self.Z1*R0EXP,'--')
      plt.plot(RC,ZC,'x', markeredgewidth=2.0, markersize=5.0)
      plt.axis('equal')
      plt.xlabel('R')
      plt.ylabel('Z')
    
    plt.show(block=False)

  def saveSurface(self, mogui):
    f=open(mogui.coilsurf_expeqfile.get())
    lines=f.readlines()
    f.close()
    numRZ=eval(lines[3].strip().split()[0])
   
    # Replace number of surfaces with 2
    lines[3] = "  %s     2    %s\n"%(lines[3].strip().split()[0],lines[3].strip().split()[2])

    # insert coil surface after plasma boundary surface
    for i in range(numRZ):
      lines.insert(4+numRZ,'    %0.9f    %0.9f\n'%(self.R1[i],self.Z1[i]))

    f=open(mogui.workingdir_local.get() + '/expeq_s_cs','w')
    f.writelines(lines)
    f.close()
    print('expeq_s_cs saved to ',mogui.workingdir_local.get())


class profiles_tab_contents():

  def __init__(self,mogui):
    mogui.profilesframe.columnconfigure(0,minsize=10)
    mogui.profilesframe.rowconfigure(0,minsize=30)
    mogui.profilesframe.rowconfigure(3,minsize=35)
    mogui.profilesframe.rowconfigure(5,minsize=35)
    mogui.profilesframe.rowconfigure(7,minsize=35)

    # Input te file
    self.te_input_file = tk.StringVar()
    self.te_input_file.set('no input te file selected')
    self.te_input_file_shortened = tk.StringVar()
    self.te_input_file_shortened.set('no input te file selected')
    getTeInputButton = tk.Button(mogui.profilesframe, text='Select Te Input File', command=lambda: self.getTeInputFile(mogui), font=mogui.textfont1)
    getTeInputButton.grid(row=1,column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    te_input_label = tk.Label(mogui.profilesframe, textvariable=self.te_input_file_shortened, width=18, font=mogui.textfont1)
    te_input_label.grid(row=2,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    # Input ti file
    self.ti_input_file = tk.StringVar()
    self.ti_input_file.set('no input ti file selected')
    self.ti_input_file_shortened = tk.StringVar()
    self.ti_input_file_shortened.set('no input ti file selected')
    getTiInputButton = tk.Button(mogui.profilesframe, text='Select Ti Input File', command=lambda: self.getTiInputFile(mogui), font=mogui.textfont1)
    getTiInputButton.grid(row=3,column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    ti_input_label = tk.Label(mogui.profilesframe, textvariable=self.ti_input_file_shortened, width=18, font=mogui.textfont1)
    ti_input_label.grid(row=4,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    # Input ne file
    self.ne_input_file = tk.StringVar()
    self.ne_input_file.set('no input ne file selected')
    self.ne_input_file_shortened = tk.StringVar()
    self.ne_input_file_shortened.set('no input ne file selected')
    getNeInputButton = tk.Button(mogui.profilesframe, text='Select ne Input File', command=lambda: self.getNeInputFile(mogui), font=mogui.textfont1)
    getNeInputButton.grid(row=5,column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    ne_input_label = tk.Label(mogui.profilesframe, textvariable=self.ne_input_file_shortened, width=18, font=mogui.textfont1)
    ne_input_label.grid(row=6,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    
    # Input wtor file
    self.wtor_input_file = tk.StringVar()
    self.wtor_input_file.set('no input wtor file selected')
    self.wtor_input_file_shortened = tk.StringVar()
    self.wtor_input_file_shortened.set('no input wtor file selected')
    getWtorInputButton = tk.Button(mogui.profilesframe, text='Select wtor Input File', command=lambda: self.getWtorInputFile(mogui), font=mogui.textfont1)
    getWtorInputButton.grid(row=7,column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    wtor_input_label = tk.Label(mogui.profilesframe, textvariable=self.wtor_input_file_shortened, width=18, font=mogui.textfont1)
    wtor_input_label.grid(row=8,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    
    # file format specifiers
    self.skip_num_rows_te = tk.IntVar()
    self.skip_num_rows_ti = tk.IntVar()
    self.skip_num_rows_ne = tk.IntVar()
    self.skip_num_rows_wtor = tk.IntVar()
    self.skip_num_rows_te.set(1)
    self.skip_num_rows_ti.set(1)
    self.skip_num_rows_ne.set(1)
    self.skip_num_rows_wtor.set(1)

    skip_num_rows_te_label = tk.Label(mogui.profilesframe, text='Te File Header Rows', font=mogui.textfont1)
    skip_num_rows_te_label.grid(row=1,column=2, sticky=tk.S+tk.E+tk.W, padx=5)
    skip_num_rows_te_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.skip_num_rows_te, font=mogui.textfont1, width=5, justify=tk.CENTER)
    skip_num_rows_te_Entry.grid(row=2,column=2, sticky=tk.N+tk.S, padx=5)

    skip_num_rows_ti_label = tk.Label(mogui.profilesframe, text='Ti File Header Rows', font=mogui.textfont1)
    skip_num_rows_ti_label.grid(row=3,column=2, sticky=tk.S+tk.E+tk.W, padx=5)
    skip_num_rows_ti_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.skip_num_rows_ti, font=mogui.textfont1, width=5, justify=tk.CENTER)
    skip_num_rows_ti_Entry.grid(row=4,column=2, sticky=tk.N+tk.S, padx=5)

    skip_num_rows_ne_label = tk.Label(mogui.profilesframe, text='Ne File Header Rows', font=mogui.textfont1)
    skip_num_rows_ne_label.grid(row=5,column=2, sticky=tk.S+tk.E+tk.W, padx=5)
    skip_num_rows_ne_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.skip_num_rows_ne, font=mogui.textfont1, width=5, justify=tk.CENTER)
    skip_num_rows_ne_Entry.grid(row=6,column=2, sticky=tk.N+tk.S, padx=5)

    skip_num_rows_wtor_label = tk.Label(mogui.profilesframe, text='Wtor File Header Rows', font=mogui.textfont1)
    skip_num_rows_wtor_label.grid(row=7,column=2, sticky=tk.S+tk.E+tk.W, padx=5)
    skip_num_rows_wtor_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.skip_num_rows_wtor, font=mogui.textfont1, width=5, justify=tk.CENTER)
    skip_num_rows_wtor_Entry.grid(row=8,column=2, sticky=tk.N+tk.S, padx=5)

    self.radial_coord_col_number_te = tk.IntVar()
    self.radial_coord_col_number_ti = tk.IntVar()
    self.radial_coord_col_number_ne = tk.IntVar()
    self.radial_coord_col_number_wtor = tk.IntVar()
    self.radial_coord_col_number_te.set(1)
    self.radial_coord_col_number_ti.set(1)
    self.radial_coord_col_number_ne.set(1)
    self.radial_coord_col_number_wtor.set(1)

    radial_coord_col_number_te_label = tk.Label(mogui.profilesframe, text='Te Radial Col. Num.', font=mogui.textfont1)
    radial_coord_col_number_te_label.grid(row=1,column=3, sticky=tk.S+tk.E+tk.W, padx=5)
    radial_coord_col_number_te_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.radial_coord_col_number_te, font=mogui.textfont1, width=5, justify=tk.CENTER)
    radial_coord_col_number_te_Entry.grid(row=2,column=3, sticky=tk.N+tk.S, padx=5)

    radial_coord_col_number_ti_label = tk.Label(mogui.profilesframe, text='Ti Radial Col. Num.', font=mogui.textfont1)
    radial_coord_col_number_ti_label.grid(row=3,column=3, sticky=tk.S+tk.E+tk.W, padx=5)
    radial_coord_col_number_ti_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.radial_coord_col_number_ti, font=mogui.textfont1, width=5, justify=tk.CENTER)
    radial_coord_col_number_ti_Entry.grid(row=4,column=3, sticky=tk.N+tk.S, padx=5)

    radial_coord_col_number_ne_label = tk.Label(mogui.profilesframe, text='Ne Radial Col. Num.', font=mogui.textfont1)
    radial_coord_col_number_ne_label.grid(row=5,column=3, sticky=tk.S+tk.E+tk.W, padx=5)
    radial_coord_col_number_ne_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.radial_coord_col_number_ne, font=mogui.textfont1, width=5, justify=tk.CENTER)
    radial_coord_col_number_ne_Entry.grid(row=6,column=3, sticky=tk.N+tk.S, padx=5)

    radial_coord_col_number_wtor_label = tk.Label(mogui.profilesframe, text='Wtor Radial Col. Num.', font=mogui.textfont1)
    radial_coord_col_number_wtor_label.grid(row=7,column=3, sticky=tk.S+tk.E+tk.W, padx=5)
    radial_coord_col_number_wtor_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.radial_coord_col_number_wtor, font=mogui.textfont1, width=5, justify=tk.CENTER)
    radial_coord_col_number_wtor_Entry.grid(row=8,column=3, sticky=tk.N+tk.S, padx=5)

    self.data_col_number_te = tk.IntVar()
    self.data_col_number_ti = tk.IntVar()
    self.data_col_number_ne = tk.IntVar()
    self.data_col_number_wtor = tk.IntVar()
    self.data_col_number_te.set(2)
    self.data_col_number_ti.set(2)
    self.data_col_number_ne.set(2)
    self.data_col_number_wtor.set(2)

    data_col_number_te_label = tk.Label(mogui.profilesframe, text='Te Data Col. Num.', font=mogui.textfont1)
    data_col_number_te_label.grid(row=1,column=4, sticky=tk.S+tk.E+tk.W, padx=5)
    data_col_number_te_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.data_col_number_te, font=mogui.textfont1, width=5, justify=tk.CENTER)
    data_col_number_te_Entry.grid(row=2,column=4, sticky=tk.N+tk.S, padx=5)

    data_col_number_ti_label = tk.Label(mogui.profilesframe, text='Ti Data Col. Num.', font=mogui.textfont1)
    data_col_number_ti_label.grid(row=3,column=4, sticky=tk.S+tk.E+tk.W, padx=5)
    data_col_number_ti_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.data_col_number_ti, font=mogui.textfont1, width=5, justify=tk.CENTER)
    data_col_number_ti_Entry.grid(row=4,column=4, sticky=tk.N+tk.S, padx=5)

    data_col_number_ne_label = tk.Label(mogui.profilesframe, text='Ne Data Col. Num.', font=mogui.textfont1)
    data_col_number_ne_label.grid(row=5,column=4, sticky=tk.S+tk.E+tk.W, padx=5)
    data_col_number_ne_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.data_col_number_ne, font=mogui.textfont1, width=5, justify=tk.CENTER)
    data_col_number_ne_Entry.grid(row=6,column=4, sticky=tk.N+tk.S, padx=5)

    data_col_number_wtor_label = tk.Label(mogui.profilesframe, text='Wtor Data Col. Num.', font=mogui.textfont1)
    data_col_number_wtor_label.grid(row=7,column=4, sticky=tk.S+tk.E+tk.W, padx=5)
    data_col_number_wtor_Entry =  tk.Entry(mogui.profilesframe, textvariable=self.data_col_number_wtor, font=mogui.textfont1, width=5, justify=tk.CENTER)
    data_col_number_wtor_Entry.grid(row=8,column=4, sticky=tk.N+tk.S, padx=5)


    plotInputButton = tk.Button(mogui.profilesframe, text='Read and plot input,\nand print to PROF*', command=lambda: self.plotInput(mogui), font=mogui.textfont1)
    plotInputButton.grid(row=5,column=5, rowspan=2, sticky=tk.S+tk.E+tk.W, padx=5)

    uploadProfButton = tk.Button(mogui.profilesframe, text='Upload PROF*\nfiles to gpu005', command=lambda: self.uploadProfs(mogui), font=mogui.textfont1)
    uploadProfButton.grid(row=7,column=5,rowspan=2, sticky=tk.S+tk.E+tk.W, padx=5)

    self.vtorUnit = tk.IntVar() # 0 -> m/s. 1 -> rad/s
    self.vtorUnit.set(0)
    meters_per_sec_option = tk.Radiobutton(mogui.profilesframe, text="rotation input is m/s", variable=self.vtorUnit, value=0, font=mogui.textfont1)
    meters_per_sec_option.grid(row=9,column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    rads_per_sec_option = tk.Radiobutton(mogui.profilesframe, text="rotation input is rad/s", variable=self.vtorUnit, value=1, font=mogui.textfont1)
    rads_per_sec_option.grid(row=9,column=2, sticky=tk.S+tk.E+tk.W, padx=5)


  def getTeInputFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      self.te_input_file.set(selectinputfile)
      self.te_input_file_shortened.set('...'+selectinputfile[-15:])
   
  def getTiInputFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      self.ti_input_file.set(selectinputfile)
      self.ti_input_file_shortened.set('...'+selectinputfile[-15:])
   
  def getNeInputFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      self.ne_input_file.set(selectinputfile)
      self.ne_input_file_shortened.set('...'+selectinputfile[-15:])
   
  def getWtorInputFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      self.wtor_input_file.set(selectinputfile)
      self.wtor_input_file_shortened.set('...'+selectinputfile[-15:])

  def plotInput(self, mogui):
    plt.close('all')
    plotsomething=False
    interp_points = 200
    x_new = sp.linspace(0,1,interp_points)
    if os.path.isfile(self.te_input_file.get()):
      plotsomething=True
      dat = sp.loadtxt(self.te_input_file.get())
      x = dat[:,self.radial_coord_col_number_te.get()-1]
      y = dat[:,self.data_col_number_te.get()-1]
      y_new = UnivariateSpline(x,y,k=1)(x_new)

      plt.figure("Electron Temperature eV")
      plt.plot(x,y)
      plt.plot(x_new,y_new,'-o')
      sp.savetxt('%s/PROFTE.IN'%(mogui.workingdir_local.get(),), sp.transpose([x_new,y_new]), header = '%d 1'%(interp_points,), comments='')

    if os.path.isfile(self.ti_input_file.get()):
      plotsomething=True
      dat = sp.loadtxt(self.ti_input_file.get())
      x = dat[:,self.radial_coord_col_number_ti.get()-1]
      y = dat[:,self.data_col_number_ti.get()-1]
      y_new = UnivariateSpline(x,y,k=1)(x_new)

      plt.figure("Ion Temperature eV")
      plt.plot(x,y)
      plt.plot(x_new,y_new,'-o')
      sp.savetxt('%s/PROFTI.IN'%(mogui.workingdir_local.get(),), sp.transpose([x_new,y_new]), header = '%d 1'%(interp_points,), comments='')

    if os.path.isfile(self.ne_input_file.get()):
      plotsomething=True
      dat = sp.loadtxt(self.ne_input_file.get())
      x = dat[:,self.radial_coord_col_number_ne.get()-1]
      y = dat[:,self.data_col_number_ne.get()-1]
      y_new = UnivariateSpline(x,y,k=1)(x_new)
      plt.figure("Electron Density m^-3")
      plt.plot(x,y)
      plt.plot(x_new,y_new,'-o')
      sp.savetxt('%s/PROFDEN.IN'%(mogui.workingdir_local.get(),), sp.transpose([x_new,y_new]), header = '%d 1'%(interp_points,), comments='')

    if os.path.isfile(self.wtor_input_file.get()):
      plotsomething=True
      dat = sp.loadtxt(self.wtor_input_file.get())
      x = dat[:,self.radial_coord_col_number_wtor.get()-1]
      y = dat[:,self.data_col_number_wtor.get()-1]
      y_new = UnivariateSpline(x,y,k=1)(x_new)
      if self.vtorUnit.get()==0: # If input is in m/s, divide by r0 to convert to rad/s
        y_new = y_new/mogui.R0EXP.get()
        y = y/mogui.R0EXP.get()
      plt.figure("Plasma rotation rad/s")
      plt.plot(x,y)
      plt.plot(x_new,y_new,'-o')
      sp.savetxt('%s/PROFROT.IN'%(mogui.workingdir_local.get(),), sp.transpose([x_new,y_new]), header = '%d 1'%(interp_points,), comments='')
      sp.savetxt('%s/PROFWE.IN'%(mogui.workingdir_local.get(),), sp.transpose([x_new,y_new]), header = '%d 1'%(interp_points,), comments='')

    if plotsomething==True:
        plt.show(block=False)


  def uploadProfs(self, mogui):
    
    if mogui.local_mode.get()==1: # running in local mode. 
      print('Local mode active, cannot upload')
      return
    else:
      all_present = True
      if not os.path.isfile('%s/PROFTE.IN'%(mogui.workingdir_local.get(),)):
        print('Missing PROFTE.IN file(s), cannot upload')
        all_present = False
      if not os.path.isfile('%s/PROFTI.IN'%(mogui.workingdir_local.get(),)):
        print('Missing PROFTI.IN file(s), cannot upload')
        all_present = False
      if not os.path.isfile('%s/PROFDEN.IN'%(mogui.workingdir_local.get(),)):
        print('Missing PROFDEN.IN file(s), cannot upload')
        all_present = False
      if not os.path.isfile('%s/PROFROT.IN'%(mogui.workingdir_local.get(),)):
        print('Missing PROFROT.IN file(s), cannot upload')
        all_present = False
      if not os.path.isfile('%s/PROFWE.IN'%(mogui.workingdir_local.get(),)):
        print('Missing PROFWE.IN file(s), cannot upload')
        all_present = False

      if all_present:
        if mogui.uname_gpu005.get()=="" or mogui.pw_gpu005.get()=="":
          print('Missing username or password')
          return
        print(' --------------------------- ')
        print(' ')
        print(' Uploading PROF*.IN files to GPU005...')
        p = pex.spawnu('scp %s/PROFTE.IN %s@gpu005:%s/'%(mogui.workingdir_local.get(), mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

        p = pex.spawnu('scp %s/PROFTI.IN %s@gpu005:%s/'%(mogui.workingdir_local.get(), mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

        p = pex.spawnu('scp %s/PROFDEN.IN %s@gpu005:%s/'%(mogui.workingdir_local.get(), mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

        p = pex.spawnu('scp %s/PROFROT.IN %s@gpu005:%s/'%(mogui.workingdir_local.get(), mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()

        p = pex.spawnu('scp %s/PROFWE.IN %s@gpu005:%s/'%(mogui.workingdir_local.get(), mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()
        print('   Done...')
        print(' ')
        print(' --------------------------- ')
      else:
        return


class marscoilgeom_contents():

  def __init__(self,mogui):

    mogui.marscoilgeomframe.columnconfigure(0,minsize=40)
    mogui.marscoilgeomframe.rowconfigure(0,minsize=30)


    self.noplot = tk.IntVar()
    self.noplot.set(0)
    noPlotCheckbox = tk.Checkbutton(mogui.marscoilgeomframe, variable=self.noplot, text="Plot coil geom", font=mogui.textfont1)
    noPlotCheckbox.grid(row=6,column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    calcParamsButton = tk.Button(mogui.marscoilgeomframe, text="Calc Mars Coil Parameters", command= lambda: self.calcCoilParams(mogui), font=mogui.textfont1)
    calcParamsButton.grid(row=7,column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    # Input rmzm_geom file
    self.rmzmfile = tk.StringVar()
    self.rmzmfile.set(mogui.workingdir_local.get() + '/rmzm_geom')
    self.rmzmfile_shortened = tk.StringVar()
    self.rmzmfile_shortened.set('...'+self.rmzmfile.get()[-15:])
    getRmzmFileButton = tk.Button(mogui.marscoilgeomframe, text='Select rmzm_geom File', command=lambda: self.getRmzmFile(mogui), font=mogui.textfont1)
    getRmzmFileButton.grid(row=1,column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    rmzmfile_label = tk.Label(mogui.marscoilgeomframe, textvariable=self.rmzmfile_shortened, width=30, font=mogui.textfont1)
    rmzmfile_label.grid(row=2,column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    self.nchi = tk.IntVar()
    self.nchi.set(400)
    nchi_label = tk.Label(mogui.marscoilgeomframe, text='Nchi', font=mogui.textfont1)
    nchi_label.grid(row=3,column=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    nchiEntry =  tk.Entry(mogui.marscoilgeomframe, textvariable=self.nchi, font=mogui.textfont1, width=8, justify=tk.CENTER)
    nchiEntry.grid(row=4,column=3, sticky=tk.N+tk.S, padx=5)

    # Params for calculating FEEDI, assuming an upper and lower coil set. 

    dphiU_label = tk.Label(mogui.marscoilgeomframe, text='Toroidal width of upper coils (rad)', font=mogui.textfont1)
    dphiU_label.grid(row=3,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.dphiUEntry =  tk.Entry(mogui.marscoilgeomframe, textvariable=mogui.dphiU, font=mogui.textfont1, width=12, justify=tk.CENTER)
    self.dphiUEntry.grid(row=4,column=1, sticky=tk.N+tk.S, padx=5)

    dphiL_label = tk.Label(mogui.marscoilgeomframe, text='Toroidal width of lower coils (rad)', font=mogui.textfont1)
    dphiL_label.grid(row=5,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.dphiLEntry =  tk.Entry(mogui.marscoilgeomframe, textvariable=mogui.dphiL, font=mogui.textfont1, width=12, justify=tk.CENTER)
    self.dphiLEntry.grid(row=6,column=1, sticky=tk.N+tk.S, padx=5)

    upperCurrentLabel = tk.Label(mogui.marscoilgeomframe,text='Upper Coil Currents', font=mogui.textfont1)
    upperCurrentLabel.grid(row=8,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.upperCurrentString_entry = tk.Entry(mogui.marscoilgeomframe, textvariable=mogui.upperCurrentString, justify=tk.CENTER, font=mogui.textfont1) 
    self.upperCurrentString_entry.grid(row=9,column=1,columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    lowerCurrentLabel = tk.Label(mogui.marscoilgeomframe,text='Lower Coil Currents', font=mogui.textfont1)
    lowerCurrentLabel.grid(row=10,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.lowerCurrentString_entry = tk.Entry(mogui.marscoilgeomframe, textvariable=mogui.lowerCurrentString, justify=tk.CENTER, font=mogui.textfont1)
    self.lowerCurrentString_entry.grid(row=11,column=1,columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    genericCurrentLabel = tk.Label(mogui.marscoilgeomframe,text='Generic Coil Currents', font=mogui.textfont1)
    genericCurrentLabel.grid(row=12,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.genericCurrentString_entry = tk.Entry(mogui.marscoilgeomframe, textvariable=mogui.genericCurrentString, justify=tk.CENTER, font=mogui.textfont1)
    self.genericCurrentString_entry.grid(row=13,column=1,columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    dphiGeneric_label = tk.Label(mogui.marscoilgeomframe, text='Toroidal width of all coils (rad)', font=mogui.textfont1)
    dphiGeneric_label.grid(row=14,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.dphiGenericEntry =  tk.Entry(mogui.marscoilgeomframe, textvariable=mogui.dphiGenericString, font=mogui.textfont1, width=12, justify=tk.CENTER)
    self.dphiGenericEntry.grid(row=15,column=1,columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    calcFeediButton = tk.Button(mogui.marscoilgeomframe, text="Calc FEEDI", command=lambda: self.calcFeedi(mogui), font=mogui.textfont1)
    calcFeediButton.grid(row=7,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    coilTurnsLabel = tk.Label(mogui.marscoilgeomframe,text='Coil Turns', font=mogui.textfont1)
    coilTurnsLabel.grid(row=7,column=1, sticky=tk.S+tk.E+tk.W, padx=5)
    coilTurnsEntry = tk.Entry(mogui.marscoilgeomframe,textvariable = mogui.coilTurns, font=mogui.textfont1, width=5, justify=tk.CENTER)
    coilTurnsEntry.grid(row=8,column=1, sticky=tk.N, padx=5)

    coil_set_name_label = tk.Label(mogui.marscoilgeomframe, text="Select coil geometry\nfrom known sets", font=mogui.textfont1)
    coil_set_name_label.grid(row=1,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    coil_set_name_dropdown = tk.OptionMenu(mogui.marscoilgeomframe, mogui.coil_set_name, *[x[0] for x in mogui.coil_geometries_list], command=lambda x: self.updateCoilSetInfo(mogui,0))
    coil_set_name_dropdown.grid(row=2,column=1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    self.applyPhaseShift = tk.IntVar()
    self.applyPhaseShift.set(0)
    apply_manual_shift_button = tk.Checkbutton(mogui.marscoilgeomframe, variable=self.applyPhaseShift, text="Apply manual phase shifts\nto MP coil currents", command=lambda: self.updatePhaseShiftEntry(), font=mogui.textfont1)
    apply_manual_shift_button.grid(row=1,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)

    self.manualUpperPhaseShift = tk.DoubleVar()
    self.manualUpperPhaseShift.set(0.0)
    uppershift_label = tk.Label(mogui.marscoilgeomframe, text='Upper current shift (deg)', font=mogui.textfont1)
    uppershift_label.grid(row=2,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.upperShiftEntry =  tk.Entry(mogui.marscoilgeomframe, textvariable=self.manualUpperPhaseShift, width=12, font=mogui.textfont1, justify=tk.CENTER)
    self.upperShiftEntry.grid(row=3,column=2, sticky=tk.N+tk.S, padx=5)
    self.upperShiftEntry.config(state=tk.DISABLED)

    self.manualLowerPhaseShift = tk.DoubleVar()
    self.manualLowerPhaseShift.set(0.0)
    lowershift_label = tk.Label(mogui.marscoilgeomframe, text='Lower current shift (deg)', font=mogui.textfont1)
    lowershift_label.grid(row=4,column=2, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
    self.lowerShiftEntry =  tk.Entry(mogui.marscoilgeomframe, textvariable=self.manualLowerPhaseShift, width=12, font=mogui.textfont1, justify=tk.CENTER)
    self.lowerShiftEntry.grid(row=5,column=2, sticky=tk.N+tk.S, padx=5)
    self.lowerShiftEntry.config(state=tk.DISABLED)

    self.updateCoilSetInfo(mogui,1)

  def updateCoilSetInfo(self, mogui, init):
    for i in range(len(mogui.coil_geometries_list)):
      if mogui.coil_geometries_list[i][0]==mogui.coil_set_name.get():
        mogui.coil_set_index.set(i)
    mogui.coilTurns.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][4][0])
    mogui.RZ_coilstring.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][1])
    mogui.numCoilSets.set(len(mogui.coil_geometries_list[mogui.coil_set_index.get()][2]))
    mogui.dphiGenericString.set(' ; '.join([str(i) for i in mogui.coil_geometries_list[mogui.coil_set_index.get()][3]]))


    # only do the below if numcoils=1 or 2.
    if mogui.numCoilSets.get()==1 or mogui.numCoilSets.get()==2:
      self.genericCurrentString_entry.config(state=tk.DISABLED)
      self.dphiGenericEntry.config(state=tk.DISABLED)
      if 'lower only' in mogui.coil_set_name.get():
        mogui.dphiU.set(0.0)
        mogui.dphiL.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][3][0])
        self.dphiUEntry.config(state=tk.DISABLED)
        self.dphiLEntry.config(state=tk.NORMAL)
        self.lowerCurrentString_entry.config(state=tk.NORMAL)
        self.upperCurrentString_entry.config(state=tk.DISABLED)
        if init==0:
          mogui.coil_seperate_runs.set(0)
          mogui.seperate_runs_button.config(state=tk.DISABLED)
      if 'upper only' in mogui.coil_set_name.get():
        mogui.dphiU.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][3][0])
        mogui.dphiL.set(0.0)
        self.dphiUEntry.config(state=tk.NORMAL)
        self.dphiLEntry.config(state=tk.DISABLED)
        self.lowerCurrentString_entry.config(state=tk.DISABLED)
        self.upperCurrentString_entry.config(state=tk.NORMAL)
        if init==0:
          mogui.coil_seperate_runs.set(0)
          mogui.seperate_runs_button.config(state=tk.DISABLED)
      if 'upper + lower' in mogui.coil_set_name.get():
        mogui.dphiU.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][3][0])
        mogui.dphiL.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][3][1])
        self.dphiUEntry.config(state=tk.NORMAL)
        self.dphiLEntry.config(state=tk.NORMAL)
        self.lowerCurrentString_entry.config(state=tk.NORMAL)
        self.upperCurrentString_entry.config(state=tk.NORMAL)
        if init==0:
          mogui.coil_seperate_runs.set(0)
          mogui.seperate_runs_button.config(state=tk.NORMAL)
      if 'extra' in mogui.coil_set_name.get():
        mogui.dphiU.set(0.0)
        mogui.dphiL.set(0.0)
        self.dphiUEntry.config(state=tk.DISABLED)
        self.dphiLEntry.config(state=tk.DISABLED)
        self.lowerCurrentString_entry.config(state=tk.DISABLED)
        self.upperCurrentString_entry.config(state=tk.DISABLED)
        self.genericCurrentString_entry.config(state=tk.NORMAL) 
        self.dphiGenericEntry.config(state=tk.NORMAL)
        if init==0:
          mogui.coil_seperate_runs.set(0)
          mogui.seperate_runs_button.config(state=tk.DISABLED)
    else:
      mogui.dphiU.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][3][0])
      mogui.dphiL.set(mogui.coil_geometries_list[mogui.coil_set_index.get()][3][1])
      self.dphiUEntry.config(state=tk.NORMAL)
      self.dphiLEntry.config(state=tk.NORMAL)
      self.lowerCurrentString_entry.config(state=tk.NORMAL)
      self.upperCurrentString_entry.config(state=tk.NORMAL)
      self.genericCurrentString_entry.config(state=tk.NORMAL) 
      self.dphiGenericEntry.config(state=tk.NORMAL)
      if init==0:
        mogui.coil_seperate_runs.set(0)
        mogui.seperate_runs_button.config(state=tk.NORMAL)
    if init==0:    
        self.calcCoilParams(mogui)
        self.calcFeedi(mogui)

  def updatePhaseShiftEntry(self):
    if self.applyPhaseShift.get()==0:
      self.upperShiftEntry.config(state=tk.DISABLED)
      self.lowerShiftEntry.config(state=tk.DISABLED)
    if self.applyPhaseShift.get()==1:
      self.upperShiftEntry.config(state=tk.NORMAL)
      self.lowerShiftEntry.config(state=tk.NORMAL)

  def getRmzmFile(self, mogui):
    selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
    if os.path.isfile(str(selectinputfile)):
      self.rmzmfile.set(selectinputfile)
      self.rmzmfile_shortened.set('...'+selectinputfile[-15:])

  def calcFeedi(self, mogui):
      n = mogui.NTOR.get()
      Turns = mogui.coilTurns.get()
      R0  = mogui.R0EXP.get()
      B0  = mogui.B0EXP.get()
      mu0 = 4.0e-7*sp.pi
      fI  = R0*B0/mu0

      #mogui.upperCurrentString.set(mogui.genericCurrentString.get().split(';')[0])
      #mogui.lowerCurrentString.set(mogui.genericCurrentString.get().split(';')[1])


      print('')
      print('MARS namelist parameter FEEDI:')
      if mogui.numCoilSets.get()==1 or mogui.numCoilSets.get()==2:
        if 'upper only' in mogui.coil_set_name.get() or 'upper + lower' in mogui.coil_set_name.get():
          if mogui.upperCurrentString.get()=="":
            print('no upper coil currents specified')
            return
          IC_U = [eval(i) for i in mogui.upperCurrentString.get().split(',')]
          N=len(IC_U)
          phi = 2*sp.pi/N*(sp.arange(0,N)+0.5)

          # Calc FEEDI_U
          fA_U = n*sp.pi/sp.sin(n*mogui.dphiU.get()/2)/sum(IC_U*sp.exp(-1j*n*phi))
          fAA_U = abs(fA_U)
          FEEDI_U = Turns/fA_U/fI

          print('FEEDI (upper coils) =\t\t', FEEDI_U)
          print('abs(FEEDI (upper coils)) =\t', sp.absolute(FEEDI_U))
          if (self.applyPhaseShift.get()==1)&(self.manualUpperPhaseShift.get()!=0.0)&('upper only' in mogui.coil_set_name.get()):
            FEEDI_U = FEEDI_U*sp.exp(-1j*(self.manualUpperPhaseShift.get()*sp.pi/180))
            print('Upper currents shifted by %0.1f degrees'%(self.manualUpperPhaseShift.get(),))
            print('After shift, FEEDI (upper coils) =\t', FEEDI_U)
          mogui.feedi_string.set('('+str(FEEDI_U.real)+','+str(FEEDI_U.imag)+'),')
          mogui.feedi_shortened.set('(%0.2e,%0.2e),'%(FEEDI_U.real,FEEDI_U.imag))
          x=mogui.feedi_shortened.get()
          if len(x)<60:
            mogui.feedi_shortened_wrapped.set(x)
          else:
            mogui.feedi_shortened_wrapped.set(x[:int(len(x)/2)] + '\n' + x[int(len(x)/2):])

        if 'lower only' in mogui.coil_set_name.get() or 'upper + lower' in mogui.coil_set_name.get():
          if mogui.lowerCurrentString.get()=="":
            print('no lower coil currents specified')
            return
          IC_L = [eval(i) for i in mogui.lowerCurrentString.get().split(',')]
          N=len(IC_L) 
          phi = 2*sp.pi/N*(sp.arange(0,N)+0.5)

          # Calc FEEDI_L
          fA_L = n*sp.pi/sp.sin(n*mogui.dphiL.get()/2)/sum(IC_L*sp.exp(-1j*n*phi))
          fAA_L = abs(fA_L)
          FEEDI_L = Turns/fA_L/fI

          print('FEEDI (lower coils) =\t\t', FEEDI_L)
          print('abs(FEEDI (lower coils)) =\t', sp.absolute(FEEDI_L))
          if (self.applyPhaseShift.get()==1)&(self.manualLowerPhaseShift.get()!=0.0)&('lower only' in mogui.coil_set_name.get()):
            FEEDI_L = FEEDI_L*sp.exp(-1j*(self.manualLowerPhaseShift.get()*sp.pi/180))
            print('Lower currents shifted by %0.1f degrees'%(self.manualLowerPhaseShift.get(),))
            print('After shift, FEEDI (lower coils) =\t', FEEDI_L)
          mogui.feedi_string.set('('+str(FEEDI_L.real)+','+str(FEEDI_L.imag)+'),')
          mogui.feedi_shortened.set('(%0.2e,%0.2e),'%(FEEDI_L.real,FEEDI_L.imag))
          x=mogui.feedi_shortened.get()
          if len(x)<60:
            mogui.feedi_shortened_wrapped.set(x)
          else:
            mogui.feedi_shortened_wrapped.set(x[:int(len(x)/2)] + '\n' + x[int(len(x)/2):])

        if 'upper + lower' in mogui.coil_set_name.get():
          self.manualLowerPhaseShift.set((sp.angle(FEEDI_L)-sp.angle(FEEDI_U))*180/sp.pi)
          print('For input coil currents, DeltaPhi =',(sp.angle(FEEDI_L)-sp.angle(FEEDI_U))*180/sp.pi)
          FEEDI_L = FEEDI_L*sp.exp(-1j*(sp.angle(FEEDI_L)-sp.angle(FEEDI_U)))
          print('Lower currents shifted to be aligned with upper. DeltaPhi set to zero.')
          print('feedi lower * exp(-i deltaphi) = ', FEEDI_L)
          mogui.feedi_string.set('('+str(FEEDI_U.real)+','+str(FEEDI_U.imag)+'),'+'('+str(FEEDI_L.real)+','+str(FEEDI_L.imag)+'),')
          mogui.feedi_shortened.set('(%0.2e,%0.2e),(%0.2e,%0.2e),'%(FEEDI_U.real,FEEDI_U.imag,FEEDI_L.real,FEEDI_L.imag))
          x=mogui.feedi_shortened.get()
          if len(x)<60:
            mogui.feedi_shortened_wrapped.set(x)
          else:
            mogui.feedi_shortened_wrapped.set(x[:int(len(x)/2)] + '\n' + x[int(len(x)/2):])
          return

        if 'extra' in mogui.coil_set_name.get():
          setlist = [' upper only', ' lower only', ' extra set A',' extra set B',' extra set C',' extra set D',' extra set E',' extra set F',' extra set G',' extra set H',' extra set I',' extra set J']
          coilind = 99
          for i in range(len(setlist)):
            if setlist[i]==mogui.coil_set_name.get().split(',')[1]: 
              coilind=i
          if coilind==99:
            print('Error: coilset not found. calcFeedi failed.')
            return
          IC = [eval(x) for x in mogui.genericCurrentString.get().split(';')[coilind].split(',')]
          N=len(IC)
          phi = 2*sp.pi/N*(sp.arange(0,N)+0.5)

          dphi = eval(mogui.dphiGenericString.get())

          fA = n*sp.pi/sp.sin(n*dphi/2)/sum(IC*sp.exp(-1j*n*phi))
          fAA = abs(fA)
          FEEDI = Turns/fA/fI

          mogui.feedi_string.set('('+str(FEEDI.real)+','+str(FEEDI.imag)+'),')
          mogui.feedi_shortened.set('(%0.2e,%0.2e),'%(FEEDI.real,FEEDI.imag))
          x=mogui.feedi_shortened.get()
          if len(x)<60:
            mogui.feedi_shortened_wrapped.set(x)
          else:
            mogui.feedi_shortened_wrapped.set(x[:int(len(x)/2)] + '\n' + x[int(len(x)/2):])
          print(mogui.feedi_shortened.get())
          return

      else:
        currstring = mogui.genericCurrentString.get()
        mogui.genericFEEDIlist = []
        for i in range(mogui.numCoilSets.get()):
          IC = [eval(x) for x in mogui.genericCurrentString.get().split(';')[i].split(',')]
          N=len(IC)
          phi = 2*sp.pi/N*(sp.arange(0,N)+0.5)

          dphi = eval(mogui.dphiGenericString.get().split(';')[i])

          fA = n*sp.pi/sp.sin(n*dphi/2)/sum(IC*sp.exp(-1j*n*phi))
          fAA = abs(fA)
          FEEDI = Turns/fA/fI
          mogui.genericFEEDIlist.append(FEEDI)
        mogui.feedi_string.set(', '.join(['(' + str(x.real) + ',' + str(x.imag) + ')' for x in mogui.genericFEEDIlist]))
        mogui.feedi_shortened.set(', '.join(['(%0.2e,%0.2e)'%(x.real,x.imag) for x in mogui.genericFEEDIlist]))
        x=mogui.feedi_shortened.get()
        if len(x)<60:
          mogui.feedi_shortened_wrapped.set(x)
        else:
          mogui.feedi_shortened_wrapped.set(x[:int(len(x)/2)] + '\n' + x[int(len(x)/2):])
        print(mogui.feedi_shortened.get())
        return
  

  def calcCoilParams(self, mogui):
    rz_geom = pb.rzcoords(self.rmzmfile.get(), self.nchi.get())

    coilRZ=[]
    RC = []
    ZC = []
    for j in mogui.RZ_coilstring.get().split(';'):
      coilRZ.append([eval(i) for i in j.split(',')])
    
    coilRZ_collapsed = [val for sublist in coilRZ for val in sublist]
    R_coil = coilRZ_collapsed[::2]
    Z_coil = coilRZ_collapsed[1::2]

    numCoilSets = mogui.numCoilSets.get() 
    if len(coilRZ) != mogui.numCoilSets.get():
      print('Formatting error in coil RZ input')
      return

    coil_surfaces = []
    FWCHI = sp.zeros(numCoilSets)
    FCCHI = sp.zeros(numCoilSets)
    s_index_1_list = sp.zeros(numCoilSets,int)
    s_index_2_list = sp.zeros(numCoilSets,int)
    chi_index_1_list = sp.zeros(numCoilSets,int)
    chi_index_2_list = sp.zeros(numCoilSets,int)

    for i in range(numCoilSets):
      dist_squared_1 = (rz_geom.R[rz_geom.Ns_plas:,:]*rz_geom.R0EXP-coilRZ[i][0])**2 + (rz_geom.Z[rz_geom.Ns_plas:,:]*rz_geom.R0EXP-coilRZ[i][1])**2
      s_index_1,chi_index_1 = sp.where(dist_squared_1==dist_squared_1.min())
      s_index_1_list[i] = int(s_index_1[0])
      chi_index_1_list[i] = int(chi_index_1[0])
      dist_squared_2 = (rz_geom.R[rz_geom.Ns_plas:,:]*rz_geom.R0EXP-coilRZ[i][2])**2 + (rz_geom.Z[rz_geom.Ns_plas:,:]*rz_geom.R0EXP-coilRZ[i][3])**2
      s_index_2,chi_index_2 = sp.where(dist_squared_2==dist_squared_2.min())
      s_index_2_list[i] = int(s_index_2[0])
      chi_index_2_list[i] = int(chi_index_2[0])

      s_1 = rz_geom.s[s_index_1[0]+rz_geom.Ns_plas]
      chi_1 = rz_geom.chi[chi_index_1[0]]

      s_2 = rz_geom.s[s_index_2[0]+rz_geom.Ns_plas]
      chi_2 = rz_geom.chi[chi_index_2[0]]

      FWCHI[i] = (chi_1 - chi_2)/sp.pi
      FCCHI[i] = (chi_2 + chi_1)/(2*sp.pi)
      coil_surfaces.append(s_index_1[0])
      coil_surfaces.append(s_index_2[0])

    coil_surf_candidates = range(min(coil_surfaces)-1, max(coil_surfaces)+2)
    distances = sp.zeros(len(coil_surf_candidates))
    for index, surf_candidate in enumerate(coil_surf_candidates):
      if surf_candidate + rz_geom.Ns_plas >= rz_geom.Ns:
        print('Warning: surf candidate out of grid, is grid too small?') 
      else:
        s_value = rz_geom.s[surf_candidate + rz_geom.Ns_plas]

        for i in range(numCoilSets):
          distances[index] += sp.absolute(rz_geom.s[s_index_1_list[i]+rz_geom.Ns_plas] - s_value) + sp.absolute(rz_geom.s[s_index_2_list[i]+rz_geom.Ns_plas] - s_value)

    sorted_values, unique_indices  = sp.unique(distances, return_index=True)
    coil_surf_index = coil_surf_candidates[unique_indices[0]]

    IFEED=coil_surf_index+2

    print('')
    print('MARS namelist variables:')
    print('FCCHI =\t\t', FCCHI)
    print('FWCHI =\t\t', FWCHI)
    print('IFEED=ISENS =\t\t', IFEED)
    print('')
    

    if mogui.numCoilSets.get()==1 or mogui.numCoilSets.get()==2:
      if numCoilSets==1:
        mogui.fcchi_string.set(str(FCCHI[0]) + ', ')
        mogui.fcchi_string_short.set(str('%0.5f'%FCCHI[0]) + ', ')
        mogui.fwchi_string.set(str(FWCHI[0]) + ', ')
        mogui.fwchi_string_short.set(str('%0.5f'%FWCHI[0]) + ', ')
        mogui.ifeed_string.set(str(IFEED) + ',')
        mogui.isens_string.set(str(IFEED) + ',')
      if numCoilSets==2:
        mogui.fcchi_string.set(str(FCCHI[0]) + ', ' + str(FCCHI[1]) + ',')
        mogui.fcchi_string_short.set(str('%0.5f'%FCCHI[0]) + ', ' + str('%0.5f'%FCCHI[1]) + ',')
        mogui.fwchi_string.set(str(FWCHI[0]) + ', ' + str(FWCHI[1]) + ',')
        mogui.fwchi_string_short.set(str('%0.5f'%FWCHI[0]) + ', ' + str('%0.5f'%FWCHI[1]) + ',')
        mogui.ifeed_string.set(str(IFEED) + ',')
        mogui.isens_string.set(str(IFEED) + ',')
    else:
      mogui.fcchi_string.set(', '.join([str(x) for x in FCCHI])+', ')
      mogui.fwchi_string.set(', '.join([str(x) for x in FWCHI])+', ')
      mogui.fcchi_string_short.set(', '.join([str(eval('%0.5f'%x)) for x in FCCHI])+', ')
      mogui.fwchi_string_short.set(', '.join([str(eval('%0.5f'%x)) for x in FWCHI])+', ')
      mogui.ifeed_string.set(str(IFEED) + ',')
      mogui.isens_string.set(str(IFEED) + ',')


    if self.noplot.get()==1:
      plt.close('all') #to make sure there are no other figures hanging around
      plt.subplot2grid((2,2), (0,0), rowspan=2)
      for i in range(numCoilSets):
        plt.plot(rz_geom.R[s_index_1_list[i]+rz_geom.Ns_plas,chi_index_1_list[i]]*rz_geom.R0EXP,rz_geom.Z[s_index_1_list[i]+rz_geom.Ns_plas,chi_index_1_list[i]]*rz_geom.R0EXP,'rs')
        plt.plot(rz_geom.R[s_index_2_list[i]+rz_geom.Ns_plas,chi_index_2_list[i]]*rz_geom.R0EXP,rz_geom.Z[s_index_2_list[i]+rz_geom.Ns_plas,chi_index_2_list[i]]*rz_geom.R0EXP,'rs')
      plt.plot(rz_geom.R[rz_geom.Ns_plas-1,:]*rz_geom.R0EXP,rz_geom.Z[rz_geom.Ns_plas-1, :]*rz_geom.R0EXP, color="red")
      plt.plot(rz_geom.R[coil_surf_index+rz_geom.Ns_plas,:]*rz_geom.R0EXP,rz_geom.Z[coil_surf_index+rz_geom.Ns_plas,:]*rz_geom.R0EXP, color="red", linewidth=2.0)
      plt.plot(R_coil,Z_coil,'x', markeredgewidth=2.0, markersize=5.0)
      plt.axis('equal')
      plt.xlabel('R')
      plt.ylabel('Z')

      plt.subplot2grid((2,2), (0,1))
      for i in range(rz_geom.Nchi):
        plt.plot(rz_geom.R[:,i]*rz_geom.R0EXP, rz_geom.Z[:,i]*rz_geom.R0EXP, color="blue")
      for i in range(int(rz_geom.Ns)):
        plt.plot(rz_geom.R[i,:]*rz_geom.R0EXP,rz_geom.Z[i,:]*rz_geom.R0EXP, color="green")
      for i in range(numCoilSets):
        plt.plot(rz_geom.R[s_index_1_list[i]+rz_geom.Ns_plas,chi_index_1_list[i]]*rz_geom.R0EXP,rz_geom.Z[s_index_1_list[i]+rz_geom.Ns_plas,chi_index_1_list[i]]*rz_geom.R0EXP,'rs')
        plt.plot(rz_geom.R[s_index_2_list[i]+rz_geom.Ns_plas,chi_index_2_list[i]]*rz_geom.R0EXP,rz_geom.Z[s_index_2_list[i]+rz_geom.Ns_plas,chi_index_2_list[i]]*rz_geom.R0EXP,'rs')
      plt.plot(rz_geom.R[rz_geom.Ns_plas-1,:]*rz_geom.R0EXP,rz_geom.Z[rz_geom.Ns_plas-1, :]*rz_geom.R0EXP, color="red")
      plt.plot(rz_geom.R[coil_surf_index+rz_geom.Ns_plas,:]*rz_geom.R0EXP,rz_geom.Z[coil_surf_index+rz_geom.Ns_plas,:]*rz_geom.R0EXP, color="red", linewidth=2.0)
      plt.plot(R_coil,Z_coil,'x', markeredgewidth=2.0, markersize=5.0)
      Rcoil_upper=(R_coil[0]+R_coil[1])/2
      Zcoil_upper=(Z_coil[0]+Z_coil[1])/2
      plt.xlim([Rcoil_upper-0.2, Rcoil_upper+0.2])
      plt.ylim([Zcoil_upper-0.2, Zcoil_upper+0.2])
      plt.xlabel('R')
      plt.ylabel('Z')
      
      if numCoilSets==2:
        plt.subplot2grid((2,2), (1, 1))
        for i in range(rz_geom.Nchi):
          plt.plot(rz_geom.R[:,i]*rz_geom.R0EXP, rz_geom.Z[:,i]*rz_geom.R0EXP, color="blue")
        for i in range(int(rz_geom.Ns)):
          plt.plot(rz_geom.R[i,:]*rz_geom.R0EXP,rz_geom.Z[i,:]*rz_geom.R0EXP, color="green")
        for i in range(numCoilSets):
          plt.plot(rz_geom.R[s_index_1_list[i]+rz_geom.Ns_plas,chi_index_1_list[i]]*rz_geom.R0EXP,rz_geom.Z[s_index_1_list[i]+rz_geom.Ns_plas,chi_index_1_list[i]]*rz_geom.R0EXP,'rs')
          plt.plot(rz_geom.R[s_index_2_list[i]+rz_geom.Ns_plas,chi_index_2_list[i]]*rz_geom.R0EXP,rz_geom.Z[s_index_2_list[i]+rz_geom.Ns_plas,chi_index_2_list[i]]*rz_geom.R0EXP,'rs')
        plt.plot(rz_geom.R[rz_geom.Ns_plas-1,:]*rz_geom.R0EXP,rz_geom.Z[rz_geom.Ns_plas-1, :]*rz_geom.R0EXP, color="red")
        plt.plot(rz_geom.R[coil_surf_index+rz_geom.Ns_plas,:]*rz_geom.R0EXP,rz_geom.Z[coil_surf_index+rz_geom.Ns_plas,:]*rz_geom.R0EXP, color="red", linewidth=2.0)
        plt.plot(R_coil,Z_coil,'x', markeredgewidth=2.0, markersize=5.0)
        Rcoil_lower=(R_coil[2]+R_coil[3])/2
        Zcoil_lower=(Z_coil[2]+Z_coil[3])/2
        plt.xlim([Rcoil_lower-0.2, Rcoil_lower+0.2])
        plt.ylim([Zcoil_lower-0.2, Zcoil_lower+0.2])
        plt.xlabel('R')
        plt.ylabel('Z')

      plt.show(block=False)
    
class mars_tab_contents():
    def __init__(self, mogui):
      mogui.marsframe.columnconfigure(0,minsize=100)
      mogui.marsframe.columnconfigure(5,minsize=220)
      mogui.marsframe.rowconfigure(0,minsize=30)
      mogui.marsframe.rowconfigure(2,minsize=50)
      mogui.marsframe.rowconfigure(4,minsize=50)

      self.INCFEED = tk.IntVar()
      self.INCFEED.set(4)
      self.INCFEED_list = [("Stability",0),("Vacuum response",4),("Plasma response",8)]

      mogui.coil_seperate_runs.set(1)
      mogui.seperate_runs_button = tk.Checkbutton(mogui.marsframe, variable=mogui.coil_seperate_runs, text="Run upper and lower\ncoils seperately", font=mogui.textfont1)
      mogui.seperate_runs_button.grid(row=2,column=5,rowspan=2,sticky=tk.N+tk.S+tk.W, padx=16)

      label1 = tk.Label(mogui.marsframe, text = "ROTE:", font=mogui.textfont1, justify=tk.LEFT)
      label1.grid(row=1,column=1, sticky=tk.S+tk.W, padx=16)
      rote_label = tk.Label(mogui.marsframe, textvariable = mogui.rote_short, font=mogui.textfont1, justify=tk.LEFT)
      rote_label.grid(row=2,column=1, sticky=tk.N+tk.W, padx=16)
  
      label2 = tk.Label(mogui.marsframe, text = "ETA:", font=mogui.textfont1,justify=tk.LEFT)
      label2.grid(row=3,column=1, sticky=tk.S+tk.W, padx=16)
      eta_label = tk.Label(mogui.marsframe, textvariable = mogui.eta_short, font=mogui.textfont1, justify=tk.LEFT)
      eta_label.grid(row=4,column=1, sticky=tk.N+tk.W, padx=16)

      label3 = tk.Label(mogui.marsframe, text = "FCCHI:", font=mogui.textfont1, justify=tk.LEFT)
      label3.grid(row=8,column=1, sticky=tk.S+tk.W, padx=16)
      fcchi_label = tk.Label(mogui.marsframe, textvariable=mogui.fcchi_string_short, font=mogui.textfont1, justify=tk.CENTER)
      fcchi_label.grid(row=9,column=1, columnspan=4, sticky=tk.N+tk.W, padx=16)
      label4 = tk.Label(mogui.marsframe, text = "FWCHI:", font=mogui.textfont1, justify=tk.LEFT)
      label4.grid(row=10,column=1, sticky=tk.S+tk.W, padx=16)
      fwchi_label = tk.Label(mogui.marsframe, textvariable=mogui.fwchi_string_short, font=mogui.textfont1, justify=tk.CENTER)
      fwchi_label.grid(row=11,column=1, columnspan=4, sticky=tk.N+tk.W, padx=16)
      label5 = tk.Label(mogui.marsframe, text = "IFEED:", font=mogui.textfont1, justify=tk.LEFT)
      label5.grid(row=5,column=2, sticky=tk.S+tk.W, padx=5)
      ifeed_label = tk.Label(mogui.marsframe, textvariable=mogui.ifeed_string, font=mogui.textfont1, justify=tk.LEFT)
      ifeed_label.grid(row=6,column=2, sticky=tk.N+tk.W, padx=5)
      label6 = tk.Label(mogui.marsframe, text = "ISENS:", font=mogui.textfont1, justify=tk.LEFT)
      label6.grid(row=5,column=3, sticky=tk.S+tk.W, padx=5)
      isens_label = tk.Label(mogui.marsframe, textvariable=mogui.isens_string, font=mogui.textfont1, justify=tk.LEFT)
      isens_label.grid(row=6,column=3, sticky=tk.N+tk.W, padx=5)
      label7 = tk.Label(mogui.marsframe, text = "FEEDI:", font=mogui.textfont1, justify=tk.LEFT)
      label7.grid(row=12,column=1, sticky=tk.S+tk.W, padx=16)
      feedi_label = tk.Label(mogui.marsframe, textvariable=mogui.feedi_shortened_wrapped, font=mogui.textfont1, justify=tk.CENTER)
      feedi_label.grid(row=13,column=1, rowspan=2, columnspan=4, sticky=tk.N+tk.W, padx=16)

      launchMarsButton = tk.Button(mogui.marsframe, text = 'Run Mars', command = lambda: self.launchMars(mogui), font=mogui.textfont1)
      launchMarsButton.grid(row=6,column=4,rowspan=2,sticky=tk.N+tk.S+tk.E+tk.W, padx=16)
      self.waitMessage = tk.StringVar()
      self.waitMessage.set("")
      waitLabel = tk.Label(mogui.marsframe, textvariable = self.waitMessage, font=mogui.textfont1, justify=tk.LEFT)
      waitLabel.grid(row=8,column=5, rowspan=2,sticky=tk.N+tk.S+tk.E+tk.W, padx=16)


      self.INCFEED_name = tk.StringVar()
      self.INCFEED_name.set("Vacuum response")
      INCFEED_label = tk.Label(mogui.marsframe, text="INCFEED", font=mogui.textfont1, justify=tk.LEFT)
      INCFEED_label.grid(row=3,column=4, sticky=tk.S+tk.E+tk.W, padx=16)
      INCFEED_dropdown = tk.OptionMenu(mogui.marsframe, self.INCFEED, *[self.INCFEED_list[x][1] for x in range(len(self.INCFEED_list))], command=lambda x: self.updateINCFEED_name(mogui))
      INCFEED_dropdown.grid(row=4,column=4, sticky=tk.N+tk.E, padx=16)
      INCFEED_name_label = tk.Label(mogui.marsframe, textvariable=self.INCFEED_name, font=mogui.textfont1, justify=tk.LEFT)
      INCFEED_name_label.grid(row=4,column=5, sticky=tk.N+tk.W, padx=16)

      self.InitialGuess = tk.DoubleVar()
      self.InitialGuess.set(0.5)
      InitialGuess_entry_label = tk.Label(mogui.marsframe, text = "Initial GR guess", font=mogui.textfont1, justify=tk.LEFT)
      InitialGuess_entry_label.grid(row=5,column=4, sticky=tk.N+tk.S+tk.W, padx=16)
      self.InitialGuessEntry = tk.Entry(mogui.marsframe, textvariable = self.InitialGuess, font=mogui.textfont1, width=8)
      self.InitialGuessEntry.grid(row=5,column=5, sticky=tk.N+tk.S+tk.W, padx=16)
      self.InitialGuessEntry.config(state=tk.DISABLED)

      launchStandardRunButton = tk.Button(mogui.marsframe, text = 'Launch Standard Run', command = lambda: self.launchStandardRun(mogui), font=mogui.textfont1)
      launchStandardRunButton.grid(row=6,column=5,rowspan=2,sticky=tk.N+tk.S+tk.E+tk.W, padx=16)

      # whether to compute Ideal or Resistive response
      self.ideal_response = tk.IntVar()
      self.ideal_response.set(0)
      ideal_response_button = tk.Checkbutton(mogui.marsframe, variable=self.ideal_response, text="Compute ideal\nresponse", font=mogui.textfont1)
      ideal_response_button.grid(row=5,column=1,rowspan=2,sticky=tk.N+tk.S+tk.W, padx=16)

    def updateINCFEED_name(self, mogui):
      for name, value in self.INCFEED_list:
        if self.INCFEED.get()==value:
          self.INCFEED_name.set(name)
      if self.INCFEED.get()==0:
        self.InitialGuessEntry.config(state=tk.NORMAL)
      else:
        self.InitialGuessEntry.config(state=tk.DISABLED)

    def sendMarsInput(self, mogui):
      if mogui.local_mode.get()==1: # running in local mode. 
          print(' --------------------------- ')
          print(' ')
          if self.INCFEED_name.get()=="Stability":
            print(' Copying stability MARS namelist to GPU005 via scp')
            os.system('cp StabilityMarsNamelist %s/RUN.IN'%(mogui.workingdir_local.get(),))
          else:
            print(' Copying response MARS namelist to GPU005 via scp')
            os.system('cp NamelistMars %s/RUN.IN'%(mogui.workingdir_local.get(),))
          print('  ... Done.')
          print(' ') 
          print(' --------------------------- ')
      else:
          print(' --------------------------- ')
          print(' ')
          # scp NamelistMars over to GPU005
          if self.INCFEED_name.get()=="Stability":
            print(' Copying stability MARS namelist to GPU005 via scp')
            p = pex.spawnu('scp StabilityMarsNamelist %s@gpu005:%s/RUN.IN'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
            p.expect("assword:")
            p.sendline('%s'%(mogui.pw_gpu005.get(),))
            if mogui.verbose_mode.get()==1:
              p.logfile = sys.stdout
            p.expect('$')
            p.readlines()
            p.terminate(force=True)
            p.close()
          else:
            print(' Copying response MARS namelist to GPU005 via scp')
            p = pex.spawnu('scp NamelistMars %s@gpu005:%s/RUN.IN'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get()), timeout=None)
            p.expect("assword:")
            p.sendline('%s'%(mogui.pw_gpu005.get(),))
            if mogui.verbose_mode.get()==1:
              p.logfile = sys.stdout
            p.expect('$')
            p.readlines()
            p.terminate(force=True)
            p.close()
          print('  ... Done.')
          print(' ') 
          print(' --------------------------- ')

    def runMars(self, mogui):

      start_time = dt.datetime.now()
      if mogui.local_mode.get()==1: # running in local mode. 
        print(' --------------------------- ')
        print(' ')
        print(' Running MARS locally')

        os.system('cd %s; /home/dryan/MarsQ/marsq_v1.8.x > log_mars'%(mogui.workingdir_local.get(),))
        os.system('cd %s; rm B123U2.OUT B123U.OUT B1UP.OUT B1US00.OUT B2U.OUT B3U.OUT BFEEDF.OUT BNORM01.OUT CFEED.OUT CURRSENS.OUT EFAF.OUT ENERGY_ANALYZE_INPUT.OUT ENERGY.OUT J2UWF.OUT EQ4T7.OUT FEEDI.OUT FLUX.OUT J3UMAX.OUT J3UWF.OUT JACOBIAN.OUT JFD.OUT JPS.OUT JRW.OUT METRICS.OUT PPVISC.OUT PROFDISP.OUT PROFNTV.OUT PROFOFFSET.OUT SHELLRESULT.OUT TMP_JB.OUT TORQUEJXB.OUT TORQUENTV.OUT TORQUEREY.OUT PROFNUSTAR.OUT TORQUENTV2.OUT RESULT.OUT B1US0.OUT RMZM_F.OUT Error_Messages'%(mogui.workingdir_local.get(),))



      else:
        print(' --------------------------- ')
        print(' ')
        print(' Running MARS on GPU005 via ssh')
        p = pex.spawnu('ssh %s@gpu005'%(mogui.uname_gpu005.get(),), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.sendline('cd %s'%(mogui.workingdir_gpu005.get(),))
        p.expect('$')
        p.sendline('module load icc; module load openmpi_icc; module load ifort/12.0.1.107')
        p.expect('$')
        #p.sendline('time /home/dryan/MarsQ/marsq_v1.5.x > log_mars') # M1,M2=-59,59
        #p.sendline('time /home/dryan/MarsQ/marsq_v1.4.x > log_mars')
        p.sendline('time /home/dryan/MarsQ/marsq_v1.3.x > log_mars') # M1,M2=-29,29
        p.expect('$')
        p.sendline('rm B123U2.OUT B123U.OUT B1UP.OUT B1US00.OUT B2U.OUT B3U.OUT BFEEDF.OUT BNORM01.OUT CFEED.OUT CURRSENS.OUT EFAF.OUT ENERGY_ANALYZE_INPUT.OUT ENERGY.OUT J2UWF.OUT EQ4T7.OUT FEEDI.OUT FLUX.OUT J3UMAX.OUT J3UWF.OUT JACOBIAN.OUT JFD.OUT JPS.OUT JRW.OUT METRICS.OUT PPVISC.OUT PROFDISP.OUT PROFNTV.OUT PROFOFFSET.OUT SHELLRESULT.OUT TMP_JB.OUT TORQUEJXB.OUT TORQUENTV.OUT TORQUEREY.OUT')
        p.expect('$')
        p.sendline('logout')
        p.expect('$')
        output = p.readlines()
        p.terminate(force=True)
        p.close()

      end_time = dt.datetime.now()
      delta_t = end_time - start_time
      if self.INCFEED_name.get()=="Plasma response":
        print('  ... MARS plasma response computation finished in around', delta_t.seconds, 'seconds.')
      if self.INCFEED_name.get()=="Vacuum response":
        print('  ... MARS vacuum computation finished in around', delta_t.seconds, 'seconds.')
      if self.INCFEED_name.get()=="Stability":
        print('  ... MARS stability computation finished in around', delta_t.seconds, 'seconds.')
      print(' ') 
      print(' --------------------------- ')


    def collectMarsOuput(self, mogui): 
      print(' --------------------------- ')
      print(' ')
      print(' Copying MARS output to local via scp')

      if mogui.local_mode.get()==0: # running in remote mode. 
        p = pex.spawnu('scp %s@gpu005:%s/log_mars %s/log_mars'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.readlines()
        p.terminate(force=True)
        p.close()


      if self.INCFEED_name.get()=="Stability":
        if mogui.local_mode.get()==0: # running in remote mode. 
          p = pex.spawnu('scp %s@gpu005:%s/RESULT.OUT %s/stability_result'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
        else:
          os.system('mv %s/RESULT.OUT %s/stability_result'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

        os.system('mv %s/log_mars %s/log_mars_stability'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        print('Stability Result')
        os.system('cat %s/stability_result'%(mogui.workingdir_local.get(),))

      if self.INCFEED_name.get()=="Plasma response" or self.INCFEED_name.get()=="Vacuum response":
        if mogui.local_mode.get()==0: # running in remote mode. 
          p = pex.spawnu('scp %s@gpu005:%s/BPLASMA.OUT %s/bplas'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
        else:
          os.system('mv %s/BPLASMA.OUT %s/bplas'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

      if self.INCFEED_name.get()=="Plasma response":
        if mogui.local_mode.get()==0: # running in remote mode. 
          p = pex.spawnu('scp %s@gpu005:%s/PROFEQ.OUT %s/profeq_resp'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
        else:
          os.system('mv %s/PROFEQ.OUT %s/profeq_resp'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

        if mogui.local_mode.get()==0: # running in remote mode. 
          p = pex.spawnu('scp %s@gpu005:%s/XPLASMA.OUT %s/xplas_resp'%(mogui.uname_gpu005.get(), mogui.workingdir_gpu005.get(), mogui.workingdir_local.get() ), timeout=None)
          p.expect("assword:")
          p.sendline('%s'%(mogui.pw_gpu005.get(),))
          if mogui.verbose_mode.get()==1:
            p.logfile = sys.stdout
          p.expect('$')
          p.readlines()
          p.terminate(force=True)
          p.close()
        else:
          os.system('mv %s/XPLASMA.OUT %s/xplas_resp'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

        if 'lower only' in mogui.coil_set_name.get():
          if self.ideal_response.get()==0:
            os.system('mv %s/bplas %s/bplas_resist_resp_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_resist_resp_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_resist_resp_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_resist'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          if self.ideal_response.get()==1:
            os.system('mv %s/bplas %s/bplas_ideal_resp_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_ideal_resp_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_ideal_resp_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_ideal'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'upper only' in mogui.coil_set_name.get():
          if self.ideal_response.get()==0:
            os.system('mv %s/bplas %s/bplas_resist_resp_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_resist_resp_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_resist_resp_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_resist'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          if self.ideal_response.get()==1:
            os.system('mv %s/bplas %s/bplas_ideal_resp_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_ideal_resp_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_ideal_resp_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_ideal'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'extra set' in mogui.coil_set_name.get():
          if self.ideal_response.get()==0:
            os.system('mv %s/bplas %s/bplas_resist_resp_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
            os.system('mv %s/xplas_resp %s/xplas_resist_resp_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
            os.system('mv %s/log_mars %s/log_mars_resist_resp_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
            os.system('mv %s/profeq_resp %s/profeq_resist'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          if self.ideal_response.get()==1:
            os.system('mv %s/bplas %s/bplas_ideal_resp_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
            os.system('mv %s/xplas_resp %s/xplas_ideal_resp_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
            os.system('mv %s/log_mars %s/log_mars_ideal_resp_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
            os.system('mv %s/profeq_resp %s/profeq_ideal'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'upper + lower' in mogui.coil_set_name.get():
          if self.ideal_response.get()==0:
            os.system('mv %s/bplas %s/bplas_resist_resp_both'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_resist_resp_both'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_resist_resp_both'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_resist'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          if self.ideal_response.get()==1:
            os.system('mv %s/bplas %s/bplas_ideal_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_ideal_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_ideal_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_ideal'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'full set' in mogui.coil_set_name.get():
          if self.ideal_response.get()==0:
            os.system('mv %s/bplas %s/bplas_resist_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_resist_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_resist_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_resist'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          if self.ideal_response.get()==1:
            os.system('mv %s/bplas %s/bplas_ideal_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/xplas_resp %s/xplas_ideal_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/log_mars %s/log_mars_ideal_resp_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
            os.system('mv %s/profeq_resp %s/profeq_ideal'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

      if self.INCFEED_name.get()=="Vacuum response":
        if 'lower only' in mogui.coil_set_name.get():
          os.system('mv %s/bplas %s/bplas_vac_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          os.system('mv %s/log_mars %s/log_mars_vac_lower'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'upper only' in mogui.coil_set_name.get():
          os.system('mv %s/bplas %s/bplas_vac_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          os.system('mv %s/log_mars %s/log_mars_vac_upper'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'extra set' in mogui.coil_set_name.get():
          os.system('mv %s/bplas %s/bplas_vac_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
          os.system('mv %s/log_mars %s/log_mars_vac_%s'%(mogui.workingdir_local.get(),mogui.workingdir_local.get(), mogui.coil_set_name.get().split()[-1]))
        if 'upper + lower' in mogui.coil_set_name.get():
          os.system('mv %s/bplas %s/bplas_vac_both'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          os.system('mv %s/log_mars %s/log_mars_vac_both'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
        if 'full set' in mogui.coil_set_name.get():
          os.system('mv %s/bplas %s/bplas_vac_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))
          os.system('mv %s/log_mars %s/log_mars_vac_fullset'%(mogui.workingdir_local.get(),mogui.workingdir_local.get()))

      print('  ... Done.')
      print(' ') 
      print(' --------------------------- ')

    def launchMars(self, mogui, silent=False):
      if mogui.local_mode.get()==0: # running in remote mode. 
        if mogui.uname_gpu005.get()=="" or mogui.pw_gpu005.get()=="":
          print('Missing username or password')
          return
      if silent==False:
        self.waitMessage.set("Waiting for MARS\nto complete ...")
        mogui.update()

      if self.INCFEED_name.get()=="Plasma response" or self.INCFEED_name.get()=="Stability":
        profiles_found = self.checkForProfiles(mogui)
        if profiles_found==False:
          print(' Missing PROF*.IN file/s on GPU005 or locally. Cannot compute plasma response or stability')
          if silent==False:
            self.waitMessage.set("Done")
            mogui.update()
            mogui.after(250)
            self.waitMessage.set("")
          return


      if (mogui.coil_seperate_runs.get()==1) and (self.INCFEED_name.get()=="Vacuum response" or self.INCFEED_name.get()=="Plasma response"):
        if 'upper + lower' in mogui.coil_set_name.get(): 
          mogui.marscoilgeom_content.applyPhaseShift.set(1)
          mogui.marscoilgeom_content.updatePhaseShiftEntry()
          mogui.marscoilgeom_content.noplot.set(0)
          mogui.marscoilgeom_content.updateCoilSetInfo(mogui,0)

          machine = mogui.coil_set_name.get().split()[0]        
          mogui.coil_set_name.set(machine+' upper only')
          mogui.marscoilgeom_content.updateCoilSetInfo(mogui,0)
          mogui.update()
          self.writeNamelistMars(mogui)
          self.sendMarsInput(mogui)
          self.runMars(mogui)
          self.collectMarsOuput(mogui)
          mogui.coil_set_name.set(machine+' lower only')
          mogui.marscoilgeom_content.updateCoilSetInfo(mogui,0)
          mogui.update()
          self.writeNamelistMars(mogui)
          self.sendMarsInput(mogui)
          self.runMars(mogui)
          self.collectMarsOuput(mogui)
          mogui.coil_set_name.set(machine+' upper + lower')
          mogui.marscoilgeom_content.updateCoilSetInfo(mogui,0)
        else: 
          machine = mogui.coil_set_name.get().split()[0]
          setlist = [' upper only', ' lower only', ' extra set A',' extra set B',' extra set C',' extra set D',' extra set E',' extra set F',' extra set G',' extra set H',' extra set I',' extra set J']
          numcoils = mogui.numCoilSets.get()
          for coilnum in range(numcoils):
            mogui.coil_set_name.set(machine+setlist[coilnum])
            mogui.marscoilgeom_content.updateCoilSetInfo(mogui,0)
            mogui.update()
            self.writeNamelistMars(mogui)
            self.sendMarsInput(mogui)
            self.runMars(mogui)
            self.collectMarsOuput(mogui)
          mogui.coil_set_name.set(machine+' full set')
          mogui.marscoilgeom_content.updateCoilSetInfo(mogui,0)

      else:
        if self.INCFEED_name.get()=="Vacuum response" or self.INCFEED_name.get()=="Plasma response":
          self.writeNamelistMars(mogui)
        if self.INCFEED_name.get()=="Stability":
          self.writeStabilityNamelist(mogui)
        self.sendMarsInput(mogui)
        self.runMars(mogui)
        self.collectMarsOuput(mogui)

      if silent==False:
        self.waitMessage.set("Done")
        mogui.update()
        mogui.after(250)
        self.waitMessage.set("")
        messagebox.showinfo("Ahoy!", "MARS run finished.")

    def writeStabilityNamelist(self, mogui):  
      f=open('StabilityMarsNamelist')
      lines=f.readlines()
      f.close()

      for index, line in enumerate(lines):
        if ' TALPHA1'in line:
            lines[index] = ' TALPHA1 = (%0.6e, 0.0),\n'%(self.InitialGuess.get())
        if ' RNTOR' in line:
            lines[index] = ' RNTOR = -' + str(mogui.NTOR.get()) + '.,\n'

      f=open('StabilityMarsNamelist','w')
      f.writelines(lines)
      f.close()

    def writeNamelistMars(self, mogui):
      f=open('NamelistMars')
      lines=f.readlines()
      f.close()

      for index, line in enumerate(lines):
        if ' INCFEED' in line:
            lines[index] = ' INCFEED = ' + str(self.INCFEED.get())+',\n'
        if ' NCOIL' in line:
            lines[index] = ' NCOIL = ' + str(mogui.numCoilSets.get())+',\n'
        if ' RNTOR' in line:
            lines[index] = ' RNTOR = -' + str(mogui.NTOR.get()) + '.,\n'
        if self.INCFEED_name.get()=="Vacuum response":
            if ' NPROFN' in line:
              lines[index] = ' NPROFN = 0,\n'
            if ' NPROFR' in line:
              lines[index] = ' NPROFR = 0,\n'
            if ' NPROFIE' in line:
              lines[index] = ' NPROFIE = 0,\n'
            if ' NPROFWE' in line:
              lines[index] = ' NPROFWE = 0,\n'
            if ' ETA ' in line:
              lines[index] = ' ETA = 0,\n'
            if ' ROTE ' in line:
              lines[index] = ' ROTE = 0,\n'
            if ' ROTWE0 ' in line:
              lines[index] = ' ROTWE0 = 0,\n'
        if self.INCFEED_name.get()=="Plasma response":
            if ' NPROFN' in line:
              lines[index] = ' NPROFN = 4,\n'
            if ' NPROFR' in line:
              lines[index] = ' NPROFR = 4,\n'
            if ' NPROFIE' in line:
              lines[index] = ' NPROFIE = 4,\n'
            if ' NPROFWE' in line:
              lines[index] = ' NPROFWE = 4,\n'
            if ' ETA ' in line:
              if self.ideal_response.get()==0:
                lines[index] = ' ETA = ' + str(mogui.eta.get()) + ',\n'
              if self.ideal_response.get()==1:
                lines[index] = ' ETA = 0,\n'
            if ' ROTE ' in line:
              lines[index] = ' ROTE = ' + str(mogui.rote.get()) + ',\n'
            if ' ROTWE0 ' in line:
              lines[index] = ' ROTWE0 = ' + str(mogui.rote.get()) + ',\n'
        if ' FWCHI' in line:
            lines[index] = ' FWCHI  = ' + mogui.fwchi_string.get()+'\n'
        if ' FCCHI' in line:
            lines[index] = ' FCCHI  = ' + mogui.fcchi_string.get()+'\n'
        if ' IFEED' in line:
            lines[index] = ' IFEED  = ' + mogui.ifeed_string.get()+'\n'
        if ' ISENS' in line:
            lines[index] = ' ISENS  = ' + mogui.isens_string.get()+'\n'
        if ' FEEDI' in line:
            lines[index] = ' FEEDI  = ' + mogui.feedi_string.get()+'\n'

      f=open('NamelistMars','w')
      f.writelines(lines)
      f.close()

    def checkForProfiles(self, mogui):
      if mogui.local_mode.get()==1: # running in local mode. 
        missing_profiles = False
        if os.path.isfile('%s/PROFTE.IN'%(mogui.workingdir_local.get(),))==False:
          missing_profiles = True
        if os.path.isfile('%s/PROFTI.IN'%(mogui.workingdir_local.get(),))==False:
          missing_profiles = True
        if os.path.isfile('%s/PROFDEN.IN'%(mogui.workingdir_local.get(),))==False:
          missing_profiles = True
        if os.path.isfile('%s/PROFROT.IN'%(mogui.workingdir_local.get(),))==False:
          missing_profiles = True
        if os.path.isfile('%s/PROFWE.IN'%(mogui.workingdir_local.get(),))==False:
          missing_profiles = True
        if missing_profiles==True:
          return False
        else:
          return True
      if mogui.local_mode.get()==0:
        p = pex.spawnu('ssh %s@gpu005'%(mogui.uname_gpu005.get(),), timeout=None)
        p.expect("assword:")
        p.sendline('%s'%(mogui.pw_gpu005.get(),))
        if mogui.verbose_mode.get()==1:
          p.logfile = sys.stdout
        p.expect('$')
        p.sendline('cd %s'%(mogui.workingdir_gpu005.get(),))
        p.expect('$')
        p.sendline('ls PROFTI.IN')
        p.expect('$')
        p.sendline('ls PROFTE.IN')
        p.expect('$')
        p.sendline('ls PROFDEN.IN')
        p.expect('$')
        p.sendline('ls PROFWE.IN')
        p.expect('$')
        p.sendline('ls PROFROT.IN')
        p.expect('$')
        p.sendline('logout')
        output = p.readlines()
        p.terminate(force=True)
        p.close()
        concat_output = "".join(output)
        if 'o such file' in concat_output:
          return False
        else:
          return True

    def launchStandardRun(self, mogui):
      if mogui.local_mode.get()==0: # running in remote mode. 
        if mogui.uname_gpu005.get()=="" or mogui.pw_gpu005.get()=="":
          print('Missing username or password')
          return
      self.waitMessage.set("Waiting for standard run\nto complete...")
      mogui.update()
      mogui.basic_content.calcQuantities(mogui, silent=True) # calc rote, eta

      mogui.chease_content.RunMarsBasicOption.set(1)
      mogui.chease_content.CoordSys_name.set("PEST")
      mogui.chease_content.updateCoordSys(mogui) # calc coil params and feedi
      mogui.chease_content.launchChease(mogui, silent=True)

      mogui.chease_content.CoordSys_name.set("Geometric")
      mogui.chease_content.updateCoordSys(mogui)
      mogui.chease_content.launchChease(mogui, silent=True)

      mogui.coil_seperate_runs.set(1)
      self.INCFEED.set(4)
      self.updateINCFEED_name(mogui)
      self.launchMars(mogui, silent=True)
      mogui.coil_seperate_runs.set(1)
      self.INCFEED.set(8)
      self.updateINCFEED_name(mogui)
      self.ideal_response.set(1) # ideal resp
      self.launchMars(mogui, silent=True)
      mogui.coil_seperate_runs.set(1)
      self.ideal_response.set(0) # resist resp
      self.launchMars(mogui, silent=True)
      self.writeStandardMoguiLogfile(mogui)

      if mogui.local_mode.get()==1: # running in local mode. 
        os.system('cd %s; rm OUT*MAR B1US0.OUT RUN.IN RMZM_F.OUT JPLASMA.OUT VPLASMA.OUT DPLASMA.OUT PPLASMA.OUT EPLASMA.OUT Error_Messages datain'%(mogui.workingdir_local.get(),))
      ##++ self.writeRemakeScripts(mogui) ## <-- to do

      self.waitMessage.set("Done")
      mogui.update()
      mogui.after(250)
      self.waitMessage.set("")
      messagebox.showinfo("Ahoy!", "MARS standard run finished.")

    def writeStandardMoguiLogfile(self, mogui):

       ## Add what to do if mogui standard run fails for any reason, so
       ## files wont be present.
       ## Logfile contains all user inputs, CHEASE/MARS namelist vars and equilibrium expeq file used
## add a copy of all PROF*.IN files if they are detected (usually not large)

      if os.path.isfile(mogui.workingdir_local.get()+'/log_chease'):
        mogui.logchease.set(mogui.workingdir_local.get()+'/log_chease')

      logchease=open(mogui.logchease.get())
      cheaselines=logchease.readlines()
      logchease.close()
      q95='Not found'
      gexp='Not found'
      ppf='Not found'
      for line in cheaselines:
        if 'GEXP' in line:
          gexp = line.split()[3]
        if 'Q AT 95% FLUX SURFACE' in line:
          q95= eval(line.split()[-1])
        if 'PRESSURE PEAKING FACTOR' in line:
          ppf = line.split()[-1]

      li = cheaselines[-14].split()[0]
      Ireal = float(cheaselines[-19].split(' ')[13])/1e6
      Ichease = float(cheaselines[-19].split(' ')[4])
      q0 = float(cheaselines[-13].split()[-2])
      qa = float(cheaselines[-12].split(' ')[-2])

      logfile = open(mogui.workingdir_local.get()+'/standard_run_log.txt','w+')

      logfile.write('Mogui Standard Run logfile.\n')
      logfile.write('Created     : '+dt.datetime.now().strftime('%d-%m-%Y %H:%M:%S')+'\n')
      logfile.write('Working dir : '+mogui.workingdir_local.get()+'\n\n')

      logfile.write('Input experimental values:\n')
      logfile.write('B0 (T)    : '+str(mogui.B0EXP.get())+'\n')
      logfile.write('R0 (m)    : '+str(mogui.R0EXP.get())+'\n')
      logfile.write('ntor      : '+str(mogui.NTOR.get())+'\n')
      logfile.write('vT0 krad/s: '+str(mogui.vt0.get())+'\n')
      logfile.write('ne0       : '+str(mogui.ne0.get())+'\n')
      logfile.write('Te0       : '+str(mogui.Te0.get())+'\n')
      logfile.write('Ti0       : '+str(mogui.Ti0.get())+'\n')
      logfile.write('asp       : '+str(mogui.aspect.get())+'\n')
      logfile.write('Coilset   : '+mogui.coil_set_name.get()+'\n')
      logfile.write('Upper coil currents:'+mogui.upperCurrentString.get()+'\n')
      logfile.write('Lower coil currents:'+mogui.lowerCurrentString.get()+'\n\n')
      logfile.write('Generic coil currents:'+mogui.genericCurrentString.get()+'\n\n')

      logfile.write('Equilibrium global values:\n')
      logfile.write('q0        : ' + str(q0)+'\n')
      logfile.write('q95       : ' + str(q95)+'\n')
      logfile.write('qa        : ' + str(qa)+'\n')
      logfile.write('betaN     : '+ str(gexp)+'\n')
      logfile.write('I (MA)    : '+ str(Ireal)+'\n')
      logfile.write('ppf       : '+ str(ppf)+'\n')
      logfile.write('li        : '+ str(li)+'\n\n')

      logfile.write('Intermediate MARS values:\n')
      logfile.write('ETA       : '+str(mogui.eta.get())+'\n')
      logfile.write('ROTE      : '+str(mogui.rote.get())+'\n')
      logfile.write('FWCHI     : '+mogui.fwchi_string.get()+'\n')
      logfile.write('FCCHI     : '+mogui.fcchi_string.get()+'\n')
      logfile.write('IFEED     : '+mogui.ifeed_string.get()+'\n')
      logfile.write('FEEDI     : '+mogui.feedi_string.get()+'\n\n')

      logfile.write('Input expeq file: '+mogui.chease_equilib_file.get()+'\n')
      logfile.write('Mod time: '+dt.datetime.fromtimestamp(os.path.getmtime(mogui.chease_equilib_file.get())).strftime('%d-%m-%Y %H:%M:%S')+'\n')
      expeq_file = open(mogui.chease_equilib_file.get(),'r')
      for line in expeq_file:
        logfile.write(line)
      expeq_file.close()
      
      logfile.close()

      print('log written to '+mogui.workingdir_local.get()+'/standard_run_log.txt')

class results_tab_contents():

    def __init__(self, mogui):
      self.results_nb = ttk.Notebook(mogui.resultsframe, name='results_nb')
      self.results_nb.pack(fill=tk.BOTH, expand=tk.Y, padx=2, pady=3)
      self.results_file_tab = ttk.Frame(self.results_nb)
      self.magnetics_tab = ttk.Frame(self.results_nb)
      self.displacement_tab = ttk.Frame(self.results_nb)
      self.pressure_tab = ttk.Frame(self.results_nb)
      self.current_tab = ttk.Frame(self.results_nb)
      self.results_save_tab = ttk.Frame(self.results_nb)

      self.add_results_file_tab_contents(mogui)
      self.add_magnetics_tab_contents(mogui)
      self.add_displacement_tab_contents(mogui)
      self.add_pressure_tab_contents(mogui)
      self.add_current_tab_contents(mogui)
      self.add_results_save_tab_contents(mogui)

      self.results_nb.add(self.results_file_tab, text='MARS Files')
      self.results_nb.add(self.magnetics_tab, text='Magnetics')
      self.results_nb.add(self.displacement_tab, text='Displacement')
      self.results_nb.add(self.pressure_tab, text='Pressure')
      self.results_nb.add(self.current_tab, text='Current')
      self.results_nb.add(self.results_save_tab, text='Save Processed Results')


      # set default values for filenames
      if os.path.isfile(mogui.workingdir_local.get()+'/rmzm_pest'):
        self.rmzm_pest.set(mogui.workingdir_local.get()+'/rmzm_pest')
        self.rmzm_pest_shortened.set('...'+(mogui.workingdir_local.get()+'/rmzm_pest')[-15:])
      else:
        self.rmzm_pest_shortened.set('No rmzm pest found')

      if os.path.isfile(mogui.workingdir_local.get()+'/rmzm_geom'):
        self.rmzm_geom.set(mogui.workingdir_local.get()+'/rmzm_geom')
        self.rmzm_geom_shortened.set('...'+(mogui.workingdir_local.get()+'/rmzm_geom')[-15:])
      else:
        self.rmzm_geom_shortened.set('No rmzm geom found')

      if os.path.isfile(mogui.workingdir_local.get()+'/bplas_vac_both'):
        self.bplas.set(mogui.workingdir_local.get()+'/bplas_vac_both')
        self.bplas_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_vac_both')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/bplas_resist_resp_both'):
        self.bplas.set(mogui.workingdir_local.get()+'/bplas_resist_resp_both')
        self.bplas_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_resist_resp_both')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/bplas_ideal_resp_both'):
        self.bplas.set(mogui.workingdir_local.get()+'/bplas_ideal_resp_both')
        self.bplas_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_ideal_resp_both')[-15:])
      else:
        self.bplas_shortened.set('No bplas found')

      if os.path.isfile(mogui.workingdir_local.get()+'/bplas_vac_upper'):
        self.bplas_u.set(mogui.workingdir_local.get()+'/bplas_vac_upper')
        self.bplas_u_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_vac_upper')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/bplas_resist_resp_upper'):
        self.bplas_u.set(mogui.workingdir_local.get()+'/bplas_resist_resp_upper')
        self.bplas_u_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_resist_resp_upper')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/bplas_ideal_resp_upper'):
        self.bplas_u.set(mogui.workingdir_local.get()+'/bplas_ideal_resp_upper')
        self.bplas_u_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_ideal_reKsp_upper')[-15:])
      else:
        self.bplas_l_shortened.set('No bplas upper found')

      if os.path.isfile(mogui.workingdir_local.get()+'/bplas_vac_lower'):
        self.bplas_l.set(mogui.workingdir_local.get()+'/bplas_vac_lower')
        self.bplas_l_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_vac_lower')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/bplas_resist_resp_lower'):
        self.bplas_l.set(mogui.workingdir_local.get()+'/bplas_resist_resp_lower')
        self.bplas_l_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_resist_resp_lower')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/bplas_ideal_resp_lower'):
        self.bplas_l.set(mogui.workingdir_local.get()+'/bplas_ideal_resp_lower')
        self.bplas_l_shortened.set('...'+(mogui.workingdir_local.get()+'/bplas_ideal_resp_lower')[-15:])
      else:
        self.bplas_l_shortened.set('No bplas lower found')

      if os.path.isfile(mogui.workingdir_local.get()+'/xplas_ideal_resp_both'):
        self.xplas.set(mogui.workingdir_local.get()+'/xplas_ideal_resp_both')
        self.xplas_shortened.set('...'+(mogui.workingdir_local.get()+'/xplas_ideal_resp_both')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/xplas_resist_resp_both'):
        self.xplas.set(mogui.workingdir_local.get()+'/xplas_resist_resp_both')
        self.xplas_shortened.set('...'+(mogui.workingdir_local.get()+'/xplas_resist_resp_both')[-15:])
      else:
        self.xplas_shortened.set('No xplas found')

      if os.path.isfile(mogui.workingdir_local.get()+'/xplas_ideal_resp_upper'):
        self.xplas_u.set(mogui.workingdir_local.get()+'/xplas_ideal_resp_upper')
        self.xplas_u_shortened.set('...'+(mogui.workingdir_local.get()+'/xplas_ideal_resp_upper')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/xplas_resist_resp_upper'):
        self.xplas_u.set(mogui.workingdir_local.get()+'/xplas_resist_resp_upper')
        self.xplas_u_shortened.set('...'+(mogui.workingdir_local.get()+'/xplas_resist_resp_upper')[-15:])
      else:
        self.xplas_u_shortened.set('No xplas upper found')

      if os.path.isfile(mogui.workingdir_local.get()+'/xplas_ideal_resp_lower'):
        self.xplas_l.set(mogui.workingdir_local.get()+'/xplas_ideal_resp_lower')
        self.xplas_l_shortened.set('...'+(mogui.workingdir_local.get()+'/xplas_ideal_resp_lower')[-15:])
      elif os.path.isfile(mogui.workingdir_local.get()+'/xplas_resist_resp_lower'):
        self.xplas_l.set(mogui.workingdir_local.get()+'/xplas_resist_resp_both')
        self.xplas_l_shortened.set('...'+(mogui.workingdir_local.get()+'/xplas_resist_resp_lower')[-15:])
      else:
        self.xplas_l_shortened.set('No xplas lower found')

      if os.path.isfile(mogui.workingdir_local.get()+'/pplas_ideal_resp_both'):
        self.pplas.set(mogui.workingdir_local.get()+'/pplas_ideal_resp_both')
        self.pplas_shortened.set('...'+(mogui.workingdir_local.get()+'/pplas_ideal_resp_both')[-15:])
      else:
        self.pplas_shortened.set('No pplas found')

      if os.path.isfile(mogui.workingdir_local.get()+'/jplas_ideal_resp_both'):
        self.jplas.set(mogui.workingdir_local.get()+'/jplas_ideal_resp_both')
        self.jplas_shortened.set('...'+(mogui.workingdir_local.get()+'/jplas_ideal_resp_both')[-15:])
      else:
        self.jplas_shortened.set('No jplas found')


    def add_results_file_tab_contents(self, mogui):
      self.results_file_tab.columnconfigure(0,minsize=20)
      self.results_file_tab.rowconfigure(0,minsize=30)
      self.rmzm_pest = tk.StringVar()
      self.rmzm_pest_shortened = tk.StringVar()
      getRmzmPestButton = tk.Button(self.results_file_tab, text='Select PEST File', command=lambda: self.getRmzmPest(mogui), font=mogui.textfont1)
      getRmzmPestButton.grid(row=0+1, column=0+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      rmzmpest_label = tk.Label(self.results_file_tab, textvariable=self.rmzm_pest_shortened, width=15, font=mogui.textfont1)
      rmzmpest_label.grid(row=1+1, column=0+1, padx=5)
      
      self.rmzm_geom = tk.StringVar()
      self.rmzm_geom_shortened = tk.StringVar()
      getRmzmGeomButton = tk.Button(self.results_file_tab, text='Select GEOM File', command=lambda: self.getRmzmGeom(mogui), font=mogui.textfont1)
      getRmzmGeomButton.grid(row=2+1, column=0+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      rmzmgeom_label = tk.Label(self.results_file_tab, textvariable=self.rmzm_geom_shortened, width=15, font=mogui.textfont1)
      rmzmgeom_label.grid(row=3+1, column=0+1, padx=5)

      getProfeqButton = tk.Button(self.results_file_tab, text='Select PROFEQ File', command=lambda: self.getProfeq(mogui), font=mogui.textfont1)
      getProfeqButton.grid(row=4+1, column=0+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      profeq_label = tk.Label(self.results_file_tab, textvariable=mogui.profeqfile_shortened, width=15, font=mogui.textfont1)
      profeq_label.grid(row=5+1, column=0+1, padx=5)

      self.bplas = tk.StringVar()
      self.bplas_shortened = tk.StringVar()
      self.getBplasGeomButton = tk.Button(self.results_file_tab, text='Select Two Coil\n BPLAS File', command=lambda: self.getBplas(mogui), font=mogui.textfont1)
      self.getBplasGeomButton.grid(row=0+1, column=1+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      bplas_label = tk.Label(self.results_file_tab, textvariable=self.bplas_shortened, width=15, font=mogui.textfont1)
      bplas_label.grid(row=1+1, column=1+1, padx=5)

      self.bplas_u = tk.StringVar()
      self.bplas_u_shortened = tk.StringVar()
      self.getBplasGeomButton_u = tk.Button(self.results_file_tab, text='Select Upper Coil\n BPLAS File', command=lambda: self.getBplas_u(mogui), font=mogui.textfont1)
      self.getBplasGeomButton_u.grid(row=2+1, column=1+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getBplasGeomButton_u.config(state=tk.DISABLED)
      bplas_u_label = tk.Label(self.results_file_tab, textvariable=self.bplas_u_shortened, width=15, font=mogui.textfont1)
      bplas_u_label.grid(row=3+1, column=1+1, padx=5)

      self.bplas_l = tk.StringVar()
      self.bplas_l_shortened = tk.StringVar()
      self.getBplasGeomButton_l = tk.Button(self.results_file_tab, text='Select Lower Coil\n BPLAS File', command=lambda: self.getBplas_l(mogui), font=mogui.textfont1)
      self.getBplasGeomButton_l.grid(row=4+1, column=1+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getBplasGeomButton_l.config(state=tk.DISABLED)
      bplas_l_label = tk.Label(self.results_file_tab, textvariable=self.bplas_l_shortened, width=15, font=mogui.textfont1)
      bplas_l_label.grid(row=5+1, column=1+1, padx=5)

      self.xplas = tk.StringVar()
      self.xplas_shortened = tk.StringVar()
      self.getXplasGeomButton = tk.Button(self.results_file_tab, text='Select Two Coil\n XPLAS File', command=lambda: self.getXplas(mogui), font=mogui.textfont1)
      self.getXplasGeomButton.grid(row=0+1, column=2+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      xplas_label = tk.Label(self.results_file_tab, textvariable=self.xplas_shortened, width=15, font=mogui.textfont1)
      xplas_label.grid(row=1+1, column=2+1, padx=5)

      self.xplas_u = tk.StringVar()
      self.xplas_u_shortened = tk.StringVar()
      self.getXplasGeomButton_u = tk.Button(self.results_file_tab, text='Select Upper Coil\n XPLAS File', command=lambda: self.getXplas_u(mogui), font=mogui.textfont1)
      self.getXplasGeomButton_u.grid(row=2+1, column=2+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getXplasGeomButton_u.config(state=tk.DISABLED)
      xplas_u_label = tk.Label(self.results_file_tab, textvariable=self.xplas_u_shortened, width=15, font=mogui.textfont1)
      xplas_u_label.grid(row=3+1, column=2+1, padx=5)

      self.xplas_l = tk.StringVar()
      self.xplas_l_shortened = tk.StringVar()
      self.getXplasGeomButton_l = tk.Button(self.results_file_tab, text='Select Lower Coil\n XPLAS File', command=lambda: self.getXplas_l(mogui), font=mogui.textfont1)
      self.getXplasGeomButton_l.grid(row=4+1, column=2+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getXplasGeomButton_l.config(state=tk.DISABLED)
      xplas_l_label = tk.Label(self.results_file_tab, textvariable=self.xplas_l_shortened, width=15, font=mogui.textfont1)
      xplas_l_label.grid(row=5+1, column=2+1, padx=5)


      self.pplas = tk.StringVar()
      self.pplas_shortened = tk.StringVar()
      self.getPplasGeomButton = tk.Button(self.results_file_tab, text='Select Two Coil\n PPLAS File', command=lambda: self.getPplas(mogui), font=mogui.textfont1)
      self.getPplasGeomButton.grid(row=0+1, column=3+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      pplas_label = tk.Label(self.results_file_tab, textvariable=self.pplas_shortened, width=15, font=mogui.textfont1)
      pplas_label.grid(row=1+1, column=3+1, padx=5)

      self.pplas_u = tk.StringVar()
      self.pplas_u_shortened = tk.StringVar()
      self.getPplasGeomButton_u = tk.Button(self.results_file_tab, text='Select Upper Coil\n PPLAS File', command=lambda: self.getPplas_u(mogui), font=mogui.textfont1)
      self.getPplasGeomButton_u.grid(row=2+1, column=3+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getPplasGeomButton_u.config(state=tk.DISABLED)
      pplas_u_label = tk.Label(self.results_file_tab, textvariable=self.pplas_u_shortened, width=15, font=mogui.textfont1)
      pplas_u_label.grid(row=3+1, column=3+1, padx=5)

      self.pplas_l = tk.StringVar()
      self.pplas_l_shortened = tk.StringVar()
      self.getPplasGeomButton_l = tk.Button(self.results_file_tab, text='Select Lower Coil\n PPLAS File', command=lambda: self.getPplas_l(mogui), font=mogui.textfont1)
      self.getPplasGeomButton_l.grid(row=4+1, column=3+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getPplasGeomButton_l.config(state=tk.DISABLED)
      pplas_l_label = tk.Label(self.results_file_tab, textvariable=self.pplas_l_shortened, width=15, font=mogui.textfont1)
      pplas_l_label.grid(row=5+1, column=3+1, padx=5)


      self.jplas = tk.StringVar()
      self.jplas_shortened = tk.StringVar()
      self.getJplasGeomButton = tk.Button(self.results_file_tab, text='Select Two Coil\n JPLAS File', command=lambda: self.getJplas(mogui), font=mogui.textfont1)
      self.getJplasGeomButton.grid(row=0+1, column=4+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      jplas_label = tk.Label(self.results_file_tab, textvariable=self.jplas_shortened, width=15, font=mogui.textfont1)
      jplas_label.grid(row=1+1, column=4+1, padx=5)

      self.jplas_u = tk.StringVar()
      self.jplas_u_shortened = tk.StringVar()
      self.getJplasGeomButton_u = tk.Button(self.results_file_tab, text='Select Upper Coil\n JPLAS File', command=lambda: self.getJplas_u(mogui), font=mogui.textfont1)
      self.getJplasGeomButton_u.grid(row=2+1, column=4+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getJplasGeomButton_u.config(state=tk.DISABLED)
      jplas_u_label = tk.Label(self.results_file_tab, textvariable=self.jplas_u_shortened, width=15, font=mogui.textfont1)
      jplas_u_label.grid(row=3+1, column=4+1, padx=5)

      self.jplas_l = tk.StringVar()
      self.jplas_l_shortened = tk.StringVar()
      self.getJplasGeomButton_l = tk.Button(self.results_file_tab, text='Select Lower Coil\n JPLAS File', command=lambda: self.getJplas_l(mogui), font=mogui.textfont1)
      self.getJplasGeomButton_l.grid(row=4+1, column=4+1, sticky=tk.N+tk.S+tk.E+tk.W, padx=5)
      self.getJplasGeomButton_l.config(state=tk.DISABLED)
      jplas_l_label = tk.Label(self.results_file_tab, textvariable=self.jplas_l_shortened, width=15, font=mogui.textfont1)
      jplas_l_label.grid(row=5+1, column=4+1, padx=5)

      self.seperateCoils = tk.IntVar() # 0 -> only 1 bplas file. 1 -> 2 bplas files for 2 coil sets
      self.seperateCoils.set(0)
      singleCoilOption = tk.Radiobutton(self.results_file_tab, text="single run for\n both coils", variable=self.seperateCoils, value=0, command=lambda: self.updateNumCoils(), font=mogui.textfont1)
      singleCoilOption.grid(row=6+1, column=0+1, padx=5)
      seperateCoilsOption = tk.Radiobutton(self.results_file_tab, text="seperate runs for\n 2 coil sets", variable=self.seperateCoils, value=1, command=lambda: self.updateNumCoils(), font=mogui.textfont1)
      seperateCoilsOption.grid(row=6+1, column=1+1, padx=5)
 
      self.deltaphi = tk.DoubleVar()
      self.deltaphi.set(0.0)
      deltaphi_entry_label = tk.Label(self.results_file_tab, text = "Deltaphi (deg)", font=mogui.textfont1)
      deltaphi_entry_label.grid(row=6+1, column=2+1, sticky=tk.E, padx=5)
      self.deltaphiEntry = tk.Entry(self.results_file_tab, textvariable = self.deltaphi, width=6, font=mogui.textfont1)
      self.deltaphiEntry.grid(row=6+1, column=3+1, sticky=tk.W, padx=5)
      self.deltaphiEntry.config(state=tk.DISABLED)

    def add_magnetics_tab_contents(self, mogui):
      self.magnetics_tab.columnconfigure(0,minsize=100)
      self.magnetics_tab.rowconfigure(0,minsize=30)

      self.plot_b1_spectrum = tk.IntVar()
      self.plot_b1_profiles = tk.IntVar()
      #self.plot_b1_RZ = tk.IntVar()
      self.plot_bn_spectrum = tk.IntVar()
      self.plot_bn_profiles = tk.IntVar()
      self.plot_bn_RZ = tk.IntVar()
      self.plot_b1_res_mars = tk.IntVar()
      self.plot_b1_res_ergos = tk.IntVar()
      self.min_mag_harmonic = tk.IntVar()
      self.min_mag_harmonic.set(1)
      self.max_mag_harmonic = tk.IntVar()
      self.max_mag_harmonic.set(10)
      self.plot_chir = tk.IntVar()

      self.b1_spectrum_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_b1_spectrum, text=u'Plot b\u00B9 spectrum\n(MARS units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.b1_spectrum_checkbutton.grid(row=1, column=1, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.b1_profiles_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_b1_profiles, text=u'Plot b\u00B9 profiles\n(MARS units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.b1_profiles_checkbutton.grid(row=2, column=1, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      #self.b1_RZ_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_b1_RZ, text=u'Plot b\u00B9(R,Z)\n(MARS units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      #self.b1_RZ_checkbutton.grid(row=3, column=1, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.bn_spectrum_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_bn_spectrum, text=u'Plot b\u207F spectrum\n(Real units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.bn_spectrum_checkbutton.grid(row=1, column=2, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.bn_profiles_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_bn_profiles, text=u'Plot b\u207F profiles\n(Real units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.bn_profiles_checkbutton.grid(row=2, column=2, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.bn_RZ_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_bn_RZ, text=u'Plot b\u207F(R,Z)\n(Real units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.bn_RZ_checkbutton.grid(row=3, column=2, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.b1_res_mars_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_b1_res_mars, text=u'Plot b\u00B9 resonant\n(MARS units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.b1_res_mars_checkbutton.grid(row=1, column=3, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.b1_res_ergos_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_b1_res_ergos, text=u'Plot b\u00B9 resonant\n(ERGOS units)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.b1_res_ergos_checkbutton.grid(row=2, column=3, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      min_mag_harmonic_label = tk.Label(self.magnetics_tab, text="Min. harmonic\nfor profile plot", justify=tk.LEFT, font=mogui.textfont1)
      min_mag_harmonic_label.grid(row=3,column=3,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.min_mag_harmonic_entry = tk.Entry(self.magnetics_tab, textvariable=self.min_mag_harmonic, width=6, justify=tk.CENTER, font=mogui.textfont1)
      self.min_mag_harmonic_entry.grid(row=4,column=3, padx=5)

      max_mag_harmonic_label = tk.Label(self.magnetics_tab, text="Max. harmonic\nfor profile plot", justify=tk.LEFT, font=mogui.textfont1)
      max_mag_harmonic_label.grid(row=3,column=4,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.max_mag_harmonic_entry = tk.Entry(self.magnetics_tab, textvariable=self.max_mag_harmonic, width=6, justify=tk.CENTER, font=mogui.textfont1)
      self.max_mag_harmonic_entry.grid(row=4,column=4, padx=5)

      self.chir_checkbutton = tk.Checkbutton(self.magnetics_tab, variable=self.plot_chir, text=u'Plot \u03C3 chirikov', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.chir_checkbutton.grid(row=1, column=4, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.calc_deltaphi_opt_button = tk.Button(self.magnetics_tab, text=u"Compute opt. \u0394\u03A6", command = lambda: self.calc_deltaphi_opt(mogui), font=mogui.textfont1)
      self.calc_deltaphi_opt_button.grid(row=2,column=4,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.plot_selected_button = tk.Button(self.magnetics_tab, text="Plot Selected Results", command = lambda: self.plot_selected_mag_results(mogui), font=mogui.textfont1)
      self.plot_selected_button.grid(row=4, column=1, columnspan=2, padx=5)

    def add_displacement_tab_contents(self, mogui):
      self.displacement_tab.columnconfigure(0,minsize=100)
      self.displacement_tab.rowconfigure(0,minsize=30)

      self.plot_xin_spectrum = tk.IntVar()
      self.plot_xin_profiles = tk.IntVar()
      self.plot_xin_surface = tk.IntVar()
      self.plot_xin_RZ = tk.IntVar()

      self.xin_spectrum_checkbutton = tk.Checkbutton(self.displacement_tab, variable=self.plot_xin_spectrum, text=u'Plot \u03be\u207f spectrum\n(mm)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.xin_spectrum_checkbutton.grid(row=1, column=1, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.xin_profiles_checkbutton = tk.Checkbutton(self.displacement_tab, variable=self.plot_xin_profiles, text=u'Plot \u03be\u207f profiles\n(mm)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.xin_profiles_checkbutton.grid(row=1, column=2, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.xin_surface_checkbutton = tk.Checkbutton(self.displacement_tab, variable=self.plot_xin_surface, text=u'Plot \u03be\u207f surface\n(mm)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.xin_surface_checkbutton.grid(row=2, column=2, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.xin_RZ_checkbutton = tk.Checkbutton(self.displacement_tab, variable=self.plot_xin_RZ, text=u'Plot \u03be\u207f(R,Z)\n(mm)', anchor = tk.W, justify=tk.LEFT, font=mogui.textfont1)
      self.xin_RZ_checkbutton.grid(row=2, column=1, sticky=tk.W+tk.N+tk.S+tk.E, padx=5)

      self.min_disp_harmonic = tk.IntVar()
      self.min_disp_harmonic.set(3)
      min_disp_harmonic_label = tk.Label(self.displacement_tab, text="Min. harmonic\nfor profile plot", justify=tk.LEFT, font=mogui.textfont1)
      min_disp_harmonic_label.grid(row=1,column=3,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.min_disp_harmonic_entry = tk.Entry(self.displacement_tab, textvariable=self.min_disp_harmonic, width=6, justify=tk.CENTER, font=mogui.textfont1)
      self.min_disp_harmonic_entry.grid(row=1,column=4, padx=5)

      self.max_disp_harmonic = tk.IntVar()
      self.max_disp_harmonic.set(13)
      max_disp_harmonic_label = tk.Label(self.displacement_tab, text="Max. harmonic\nfor profile plot", justify=tk.LEFT, font=mogui.textfont1)
      max_disp_harmonic_label.grid(row=2,column=3,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.max_disp_harmonic_entry = tk.Entry(self.displacement_tab, textvariable=self.max_disp_harmonic, width=6, justify=tk.CENTER, font=mogui.textfont1)
      self.max_disp_harmonic_entry.grid(row=2,column=4, padx=5)

      self.disp_psin_surface = tk.DoubleVar()
      self.disp_psin_surface.set(0.95)
      disp_psin_surface_label = tk.Label(self.displacement_tab, text=u"Flux Surface\nto Plot (\u03A8_N)", justify=tk.LEFT, font=mogui.textfont1)
      disp_psin_surface_label.grid(row=1,column=5,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.disp_psin_surface_entry = tk.Entry(self.displacement_tab, textvariable=self.disp_psin_surface, width=6, justify=tk.CENTER, font=mogui.textfont1)
      self.disp_psin_surface_entry.grid(row=1,column=6, padx=5)

      self.disp_rho_surface = tk.DoubleVar()
      self.disp_rho_surface.set(0.9025)
      disp_rho_surface_label = tk.Label(self.displacement_tab, text=u"Flux Surface\nto Plot (\u03c1)", justify=tk.LEFT, font=mogui.textfont1)
      disp_rho_surface_label.grid(row=2,column=5,sticky=tk.W+tk.N+tk.S+tk.E, padx=5)
      self.disp_rho_surface_entry = tk.Entry(self.displacement_tab, textvariable=self.disp_rho_surface, width=6, justify=tk.CENTER, font=mogui.textfont1)
      self.disp_rho_surface_entry.grid(row=2,column=6, padx=5)

      self.plot_selected_button = tk.Button(self.displacement_tab, text="Plot Selected Results", command = lambda: self.plot_selected_disp_results(mogui), font=mogui.textfont1)
      self.plot_selected_button.grid(row=3, column=1, columnspan=2, padx=5)


    def add_pressure_tab_contents(self, mogui):
      return

    def add_current_tab_contents(self, mogui):
      return

    def add_results_save_tab_contents(self, mogui):
      return


    def updateNumCoils(self):
      if self.seperateCoils.get()==0: # only 1 bplas file
        self.getBplasGeomButton.config(state=tk.NORMAL)
        self.getBplasGeomButton_u.config(state=tk.DISABLED)
        self.getBplasGeomButton_l.config(state=tk.DISABLED)
        self.getXplasGeomButton.config(state=tk.NORMAL)
        self.getXplasGeomButton_u.config(state=tk.DISABLED)
        self.getXplasGeomButton_l.config(state=tk.DISABLED)
        self.getPplasGeomButton.config(state=tk.NORMAL)
        self.getPplasGeomButton_u.config(state=tk.DISABLED)
        self.getPplasGeomButton_l.config(state=tk.DISABLED)
        self.getJplasGeomButton.config(state=tk.NORMAL)
        self.getJplasGeomButton_u.config(state=tk.DISABLED)
        self.getJplasGeomButton_l.config(state=tk.DISABLED)
        self.deltaphiEntry.config(state=tk.DISABLED)
      elif self.seperateCoils.get()==1: # two bplas files for upper and lower coil sets
        self.getBplasGeomButton.config(state=tk.DISABLED)
        self.getBplasGeomButton_u.config(state=tk.NORMAL)
        self.getBplasGeomButton_l.config(state=tk.NORMAL)
        self.getXplasGeomButton.config(state=tk.DISABLED)
        self.getXplasGeomButton_u.config(state=tk.NORMAL)
        self.getXplasGeomButton_l.config(state=tk.NORMAL)
        self.getPplasGeomButton.config(state=tk.DISABLED)
        self.getPplasGeomButton_u.config(state=tk.NORMAL)
        self.getPplasGeomButton_l.config(state=tk.NORMAL)
        self.getJplasGeomButton.config(state=tk.DISABLED)
        self.getJplasGeomButton_u.config(state=tk.NORMAL)
        self.getJplasGeomButton_l.config(state=tk.NORMAL)
        self.deltaphiEntry.config(state=tk.NORMAL)
      else:
        return



    def plot_selected_mag_results(self, mogui):
      if self.plot_b1_spectrum.get()==1:
        self.plotMagSpectrogram(mogui,0,0,0)
      if self.plot_b1_profiles.get()==1:
        self.plotBplasProfiles(mogui,0)
      #if self.plot_b1_RZ.get()==1:
      #  self.plotBplasRect(mogui)
      if self.plot_bn_spectrum.get()==1:
        self.plotMagSpectrogram(mogui,0,1,0)
      if self.plot_bn_profiles.get()==1:
        self.plotBplasProfiles(mogui,1)
      if self.plot_bn_RZ.get()==1:
        self.plotBplasRect(mogui)
      if self.plot_b1_res_mars.get()==1:
        self.plotMagSpectrogram(mogui,0,0,1)
      if self.plot_b1_res_ergos.get()==1:
        self.plotMagSpectrogram(mogui,1,0,0)
      if self.plot_chir.get()==1:
        print('plot chir, not fully implemented')
  


    def plot_selected_disp_results(self, mogui):
      print('not implemented')
    
    ### ---- get file paths ----- ###
    ### ------- start ----------- ###
    def getRmzmPest(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.rmzm_pest.set(selectinputfile)
        self.rmzm_pest_shortened.set('...'+selectinputfile[-15:])

    def getRmzmGeom(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.rmzm_geom.set(selectinputfile)
        self.rmzm_geom_shortened.set('...'+selectinputfile[-15:])

    def getBplas(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.bplas.set(selectinputfile)
        self.bplas_shortened.set('...'+selectinputfile[-15:])

    def getBplas_u(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.bplas_u.set(selectinputfile)
        self.bplas_u_shortened.set('...'+selectinputfile[-15:])

    def getBplas_l(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.bplas_l.set(selectinputfile)
        self.bplas_l_shortened.set('...'+selectinputfile[-15:])

    def getXplas(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.xplas.set(selectinputfile)
        self.xplas_shortened.set('...'+selectinputfile[-15:])

    def getXplas_u(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.xplas_u.set(selectinputfile)
        self.xplas_u_shortened.set('...'+selectinputfile[-15:])

    def getXplas_l(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.xplas_l.set(selectinputfile)
        self.xplas_l_shortened.set('...'+selectinputfile[-15:])

    def getPplas(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.pplas.set(selectinputfile)
        self.pplas_shortened.set('...'+selectinputfile[-15:])

    def getPplas_u(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.pplas_u.set(selectinputfile)
        self.pplas_u_shortened.set('...'+selectinputfile[-15:])

    def getPplas_l(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.pplas_l.set(selectinputfile)
        self.pplas_l_shortened.set('...'+selectinputfile[-15:])

    def getJplas(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.jplas.set(selectinputfile)
        self.jplas_shortened.set('...'+selectinputfile[-15:])

    def getJplas_u(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.jplas_u.set(selectinputfile)
        self.jplas_u_shortened.set('...'+selectinputfile[-15:])

    def getJplas_l(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        self.jplas_l.set(selectinputfile)
        self.jplas_l_shortened.set('...'+selectinputfile[-15:])

    def getProfeq(self, mogui):
      selectinputfile = filedialog.askopenfilename(initialdir=mogui.workingdir_local.get())
      if os.path.isfile(str(selectinputfile)):
        mogui.profeqfile.set(selectinputfile)
        mogui.profeqfile_shortened.set('...'+selectinputfile[-15:])
    ### ---- get file paths ----- ###
    ### --------- end ----------- ###


    def calc_deltaphi_opt(self, mogui):
      print('not implemented')

    def plotMagSpectrogram(self,mogui, benchmark, b1_or_bn, plot_rational):
      if self.seperateCoils.get()==0: # only 1 bplas file
        plotf.plotMagSpect_1coil(self.bplas.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), benchmark, b1_or_bn, plot_rational, mogui.B0EXP.get())
      elif self.seperateCoils.get()==1: # two bplas files for upper and lower coil sets
        plotf.plotMagSpect_2coil(self.bplas_u.get(), self.bplas_l.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), self.deltaphi.get(), benchmark, b1_or_bn, plot_rational, mogui.B0EXP.get())
    
    def plotBplasRect(self, mogui):
      print('Plotting rectangular components of \nperturbed field; Br,Bz,Bphi,|B|')
      if self.seperateCoils.get()==0: # only 1 bplas file
        plotf.plotBplasRZ_1coil(self.bplas.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), mogui.R0EXP.get(), mogui.B0EXP.get())
      elif self.seperateCoils.get()==1: # two bplas files for upper and lower coil sets
        plotf.plotBplasRZ_2coil(self.bplas_u.get(), self.bplas_l.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), mogui.R0EXP.get(), mogui.B0EXP.get(), self.deltaphi.get())

    def plotBplasProfiles(self, mogui, b1_or_bn):
      print('Plotting bplas profiles')
      if self.seperateCoils.get()==0: # only 1 bplas file
        plotf.plotBplasProfiles_1coil(self.bplas.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), self.min_mag_harmonic.get(), self.max_mag_harmonic.get(), b1_or_bn, mogui.B0EXP.get())
      elif self.seperateCoils.get()==1: # two bplas files for upper and lower coil sets
        plotf.plotBplasProfiles_2coil(self.bplas_u.get(), self.bplas_l.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), self.deltaphi.get(), self.min_mag_harmonic.get(), self.max_mag_harmonic.get(), b1_or_bn, mogui.B0EXP.get())


    def plotDispSpectrogram(self,mogui):
      if self.seperateCoils.get()==0: # only 1 xplas file
        plotf.plotDispSpect_1coil(self.xplas.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get())
      elif self.seperateCoils.get()==1: # two xplas files for upper and lower coil sets
        plotf.plotDispSpect_2coil(self.xplas_u.get(), self.xplas_l.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), self.deltaphi.get())

    def plotDispProfiles(self,mogui):
      if self.seperateCoils.get()==0: # only 1 xplas file
        plotf.plotDispProfiles_1coil(self.xplas.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get())
      elif self.seperateCoils.get()==1: # two xplas files for upper and lower coil sets
        plotf.plotDispProfiles_2coil(self.xplas_u.get(), self.xplas_l.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), self.deltaphi.get())

    def plotXplasRect(self, mogui):# incomplete
      print('Plotting rectangular components of displacement')
      if self.seperateCoils.get()==0: # only 1 xplas file
        plotf.plotXplasRZ_1coil(self.xplas.get(), self.rmzm_geom.get(), self.rmzm_pest.get(), mogui.profeqfile.get(), mogui.NTOR.get(), mogui.R0EXP.get())
      elif self.seperateCoils.get()==1: # two xplas files for upper and lower coil sets
        print('not implemented')
        return




